#!/bin/python -u
import numpy as np
import modules.common as cm
import os, sys, importlib
from pylal import Fr

##########################

#Description.

#Load a certain realisation of the data from H1 and L1, and output all relevant files:
#+An ASCII file with each of the time series.
#+The original input_parameters file.
#+The original bhb_pop numpy file.
#+The original data_sim numpy file.
#+A folder with all relevant plots.

##########################

#Input parameters.
inputfile=sys.argv[1] #Name of input file (in input_parameters). For example 'test2'.

#Derived inputs:
ip=importlib.import_module('input_parameters.%s' %inputfile)
odir=ip.com_dir+'%s/' %(inputfile) #Name of combined output directory.
ofile_sim='simdata.txt' #Name of output file containing simulated time series.
ofile_bhb='bhb_pop.txt' #Name of output file containing properties of bh binaries.
ofile_par='pars.txt' #Name of output file containing input parameters and snr.
bdir=ip.bhb_dir+'%s/' %(inputfile) #Directory of realisations of the bh binaries.
ddir=ip.sim_dir+'%s/' %(inputfile) #Directory of simulated data.
snrfile=ip.snr_dir+'%s.npy' %(inputfile) #Name of numpy file with S/N information.
ligo_psd_file=ip.ligo_psd_file #File with LIGO power spectral density (PSD).
nr=ip.nr #Number of realisations to consider (it can't be larger than the number of existing realisations).
srate=ip.srate #Sampling rate (in s).
dur=ip.dur #Duration of the observation of each data stream (in s).
dur_ext=ip.dur_ext #Duration of the extended time series, for the simulated waveforms; it should be at least as long as "dur" plus the typical duration of a chirp, to avoid the 'wrap around'.
oframes_Hdata=ip.com_dir+'%s/H-data.gwf' %(inputfile) #Output frames file with data.
oframes_Ldata=ip.com_dir+'%s/L-data.gwf' %(inputfile) #Output frames file with data.

##########################

#Main.

cm.check_odir(odir) #If output directory does not exist, create it. If it exists and already contains files, ask if they should be replaced. If so, remove existing files, otherwise exit.

#First load S/N file and identify the realisation that will be exported.
s=cm.AttrDict(np.load(snrfile)[()])
sim = [item for item in s.keys() if item[0:3]=='run'][0] 
print 'Selected realisation: ',sim
snrsim=s['%s'%sim] #S/N of that realisation.

print 'Creating ASCII file of input parameters and SNR...'
ifile=open('input_parameters/%s.py' %inputfile)
ofile=open(odir+ofile_par,'w')
for line in ifile:
	if '=' in line: #Write only the lines where a parameter is defined.
		ofile.write(line)
#Include some extra lines with useful information.
ofile.write('Selected realisation: %s \n' %sim)
ofile.write('Mean of S/N: %.3e \n' %np.mean(s['%s'%sim]))
ofile.write('Standard deviation of S/N: %.3e \n' %np.std(s['%s'%sim]))
ofile.close()

print 'Creating ASCII file of simulated data...'
#Load simulated data.
d=cm.AttrDict(np.load(ddir+sim+'.npy')[()])
#Save ASCII file
oarr=np.vstack((d.t,d.H1_signal,d.L1_signal,d.H1_data,d.H1_data)).T #Output array.
np.savetxt(odir+ofile_sim, oarr, delimiter='\t', newline='\n', header='Time/s \t H1_signal \t H1_data \t L1_signal \t L1_data')

print 'Creating ASCII file of bhb properties...'
#Load bhb properties.
b=cm.AttrDict(np.load(bdir+sim+'.npy')[()])
oarr=np.vstack((b.tc, b.z, b.m1, b.m2, b.ras, b.dec, b.dl, b.phi, b.pol, b.inc)).T #Output array.
np.savetxt(odir+ofile_bhb, oarr, delimiter='\t', newline='\n', header='T_coal/s \t Redshift \t M1/solar_mass \t M2/solar_mass \t RA/rad \t DEC/rad \t Lum_dist/mpc \t Phase/rad \t Polarisation/rad \t Inclination/rad')

print 'Creating frame files...' #I follow Rorys instructions.
#Dictionary for H1 data.
H1_dict = {}
H1_dict['name']  = 'H-data'
H1_dict['data']  = d.H1_data
H1_dict['start'] = ip.start_time
H1_dict['dx']    = 1/float(ip.srate)
H1_dict['type']  = 1  # For time series, type should be 1.
Fr.frputvect(oframes_Hdata, [H1_dict])
#Dictionary for L1 data.
L1_dict = {}
L1_dict['name']  = 'L-data'
L1_dict['data']  = d.L1_data
L1_dict['start'] = ip.start_time
L1_dict['dx']    = 1/float(ip.srate)
L1_dict['type']  = 1  # For time series, type should be 1.
Fr.frputvect(oframes_Ldata, [L1_dict])

