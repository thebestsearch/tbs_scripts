#############################################################################
Description for 'tbs_sim', by Pablo A. Rosado, 2016 (pabloarosado@gmail.com).
#############################################################################

The goal of tbs_sim is to generate a population of coalescing black hole binaries and simulated the data that the two LIGO detectors would collect during some time.

In this folder there are several scripts that have to be run in a particular order.
+++A simple improvement would be to create a script that does everything at once, but for the moment and our purposes, it's better this way.

The procedure is the following:

(1) In the folder 'tbs_sim/input_parameters/' there is a file called 'common_inputs.py', where all necessary paths and default parameters are defined.
You should edit the following two lines:

###
data_dir='/home/pablo.rosado/projects/tbs/tbs_data/' #Parent directory of input and output data.
plots_dir='/home/pablo.rosado/public_html/' #Parent directory for all output plots.
###

So the paths are the ones you need.
You have to make sure that 'ligo_psd_file' is the path to the file containing LIGO power spectral density, and that 'orf_lh_file' is the path to the overlap reduction function of the detector pair H-L.

The rest of parameters in 'common_inputs.py' can also be edited if you want: they will be the default parameters.

(2) Now you should create a new file in the 'tbs_sim/input_parameters/' folder, for example called 'test.py'.
This file should start with the following line:

###
from common_inputs import * #To import all default inputs.
###

If nothing else is added, the input parameters and paths will be the ones in 'common_inputs.py'.
If you want to change the value of a parameter, simply add a new line to redefine it. For example:

###
dur = 10. #Duration of the observation, in s.
###

(3) Now you can run all scripts one by one, as follows:

###
./bhb_pop.py test #To generate the 1000 populations (or whatever number of simulations you want) of BH binaries.
./data_sim test #To simulate the data for those populations.
./snr.py test #To calculate the S/N.
./plots.py test #To produce some plots.
./prepare_output.py test #To generate the output data (ASCII and frame files).
###

Where 'test' must be the name of the input parameters file (that we called 'test.py'), without the extension '.py'.

The output data will be stored in the folder 'com_dir' (as defined in 'common_inputs.py'), and the plots will be stored in 'plots_dir'.


