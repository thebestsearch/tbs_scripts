#!/bin/python -u
import numpy as np
import modules.common as cm
from modules.gaussian_noise import gaussian_noise
from modules.generateIMRPhenomPv2 import FD
from modules.time_estimate import time_estimate
import os, sys, importlib
import modules.detectors as det

########################

#Description.

#Import a realisation of the ensemble of bhbs, and calculate the optimal matched-filter S/N for the two LIGO detectors.

########################

#Input parameters.
inputfile=sys.argv[1] #Name of input file (in input_parameters). For example 'test2'.

#Derived inputs:
ip=importlib.import_module('input_parameters.%s' %inputfile)
sdir=ip.bhb_dir+'%s/' %(inputfile) #Directory of realisations of the sources.
ligo_psd_file=ip.ligo_psd_file #File with LIGO power spectral density (PSD).
nr=ip.nr #Number of realisations to consider (it can't be larger than the number of existing realisations).
srate=ip.srate #Sampling rate (in s).
dur=ip.dur #Duration of the observation of each data stream (in s).
dur_ext=ip.dur_ext #Duration of the extended time series, for the simulated waveforms; it should be at least as long as "dur" plus the typical duration of a chirp, to avoid the 'wrap around'.

########################

#Main.

Sh=np.loadtxt(ligo_psd_file) #LIGO PSD.
tbin=1./srate #Time resolution (in s).
t_ext=np.arange(0., dur_ext, tbin) #Extended array of time samples (in s).
tsel=(t_ext<dur) #Mask that selects only time samples between 0 and "dur".
t=t_ext[tsel] #Array of time samples (in s).
deltaF=1./dur_ext #Frequency resolution for the generation of binary waveforms.

#Fixed parameters.
S1=[0,0,0] #Spin of the first black hole.
S2=[0,0,0] #Spin of the second black hole.
fmin=ip.fmin #Minimum frequency for the waveform (in Hz).
fmax=ip.fmax #Maximum frequency.
fRef=ip.fRef #Reference frequency.
#Define other parameters for the waveforms (whose meaning I ignore).
lambda1=0.
lambda2=0.
waveFlags=None
nonGRparams=None
amplitude0=0
phase0=0

##################################

fvec=np.arange(0.,5000.,deltaF)
psd=np.hstack((np.inf,det.psd_interp(fvec[1:])))

########################################

te=time_estimate(nr-1) #Initialise this class to have an estimated computation time.

for ri in xrange(nr):
        te.display() #Shows the remaining computation time.
        te.increase() #Needed to calculate the remaining computation time.

	#Load ensemble of bhbs and generate signal.
	b=cm.AttrDict(np.load(sdir+'run%.3i.npy' %int(ri))[()]) #Class binary; all properties will be loaded as "b.z" or "b.m1", etc.
	nb=len(b.z) #Number of binaries in this realisation.
	for bi in xrange(nb):
		dl=b.dl[bi] #Luminosity distance (in mpc).
		m1=b.m1[bi]*(1.+b.z[bi]) #Redshifted mass of first black hole (in solar mass).
		m2=b.m2[bi]*(1.+b.z[bi]) #Redshifted mass of second black hole (in solar mass).
		phi=b.phi[bi] #Phase (in rad).
		inc=b.inc[bi] #Inclination (in rad).
		ras=b.ras[bi] #Right ascension (in rad).
		dec=b.dec[bi] #Declination (in rad).
		pol=b.pol[bi] #Polarisation (in rad).
		tc=b.tc[bi] #Coalescence time (in s).

		hf1=FD(fmin, fmax, deltaF, dl, m1, m2, S1, S2, fRef, inc, ras, dec, pol, phi, tc, 'H1')
		hf2=FD(fmin, fmax, deltaF, dl, m1, m2, S1, S2, fRef, inc, ras, dec, pol, phi, tc, 'L1')

		fvec_sel=fvec[0:len(hf1)]
		df=np.hstack((np.diff(fvec_sel),0.))
		psd_sel=psd[0:len(hf1)]

		snr1=np.sqrt(4.*np.real(np.sum(np.conj(hf1)*hf1*df/psd_sel))) #Optimal MF S/N for detector 1.
		snr2=np.sqrt(4.*np.real(np.sum(np.conj(hf2)*hf2*df/psd_sel))) #Optimal MF S/N for detector 2.
		print
		print 'Binary at dl=%.1f Mpc with redshifted mases %.1f and %.1f msun:' %(dl,m1,m2)
		print '    Optimal MF S/N at H1: %.3f' %snr1
		print '    Optimal MF S/N at L1: %.3f' %snr2
		raw_input('enter')
