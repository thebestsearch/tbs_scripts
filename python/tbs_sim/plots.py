#!/bin/python
import numpy as np
from scipy.interpolate import interp1d
import modules.common as cm
from modules.common import hub0, h0, grav, light, msun, mpc
#import inputs_fake_signal as ip
import modules.signal_model as sm
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as py
import os, sys, importlib
from scipy.signal import butter, filtfilt, iirdesign, zpk2tf, freqz
import matplotlib.mlab as mlab

###############################

#Description.

#Calculate the theoretical (average) S/N of the cross-correlation of two detectors (with the same PSD) for a given signal (made of a list of black holes).

###############################

#Input parameters.
inputfile=sys.argv[1] #Name of input file (in input_parameters). For example 'test2'.

#Derived inputs:
ip=importlib.import_module('input_parameters.%s' %inputfile)
ddir=ip.sim_dir+'%s/' %(inputfile) #Directory of realisations of fake data.
bdir=ip.bhb_dir+'%s/' %(inputfile) #Directory of the properties of bh binaries.
snrfile=ip.snr_dir+'%s.npy' %(inputfile) #Name of numpy file with S/N information.
oplotdir=ip.plots_dir+'%s/' %(inputfile) #Output directory for plots.
nr=ip.nr #Number of realisations to consider (it can't be larger than the number of existing realisations).

###############################

cm.check_odir(oplotdir) #If output directory does not exist, create it. If it exists and already contains files, ask if they should be replaced. If so, remove existing files, otherwise exit.

print 'Producing plots of the properties of the ensemble of BH binaries...'

#First load S/N file and identify the realisation that is closest to the average signal S/N.
s=cm.AttrDict(np.load(snrfile)[()])
sim = [item for item in s.keys() if item[0:3]=='run'][0]

#First make some plots of the bh binary realisations.
b=cm.AttrDict(np.load(bdir+sim+'.npy')[()])
#Plot distribution in mch-z plane.
py.plot(b.mch,b.z,'.')
py.xlabel('Chirp mass / solar mass')
py.ylabel('Redshift')
py.title('BHB population %s' %sim)
py.savefig(oplotdir+'z_vs_mch.png')

#Plot m2 vs m1 with z as color.
#py.figure()
#py.scatter(d.m1,d.m2,c=d.z)
#py.colorbar()
#py.savefig(oplotdir+'m2_m2_z_%.3i.png' %int(ri))

#Plot their distribution in the sky.
py.clf()
xbord,ybord=sm.borders(3600)
xvectot,yvectot=sm.coords(b.ras,b.dec)
left, right, top, bottom, c_fraction=0.02,0.98,0.93,0.04,0.145
fig = py.figure(frameon=False)
fig.subplots_adjust(left=left, right=right, top=top, bottom=bottom)
ax = fig.gca()
pointsize=29
py.plot(xbord,ybord,color='black',linewidth=1.15)
sct=py.scatter(xvectot,yvectot,c='black',s=pointsize,cmap=py.cm.jet_r,edgecolors='none',rasterized=True)
ax.set_xlim(-3.,3.)
ax.set_ylim(-1.5,1.5)
ax.axes.get_xaxis().set_visible(False)
ax.axes.get_yaxis().set_visible(False)
ax.set_frame_on(False)
ax.set_title('BHB population %s' %sim)
fig.savefig(oplotdir+'skymap.png')

print 'Producing plots of the (raw) simulated data...'

d=cm.AttrDict(np.load(ddir+sim+'.npy')[()])
py.figure()
py.plot(d.t,d.H1_data,color='red', label='H1 data')
py.plot(d.t,d.L1_data,color='blue', label='L1 data')
py.plot(d.t, d.H1_signal,color='green')
py.plot(d.t, d.L1_signal,color='yellow')
py.ylim(-4e-20,4e-20)
py.xlabel('t/s')
py.ylabel('strain of signal plus noise')
py.legend(loc='upper left')
py.savefig(oplotdir+'data')

py.clf()
py.plot(d.t,d.H1_signal,color='red', label='H1 signal')
py.plot(d.t,d.L1_signal,color='blue', label='L1 signal')
py.ylim(-1e-21,1e-21)
py.xlabel('t/s')
py.ylabel('strain of signal only')
py.legend(loc='upper left')
py.savefig(oplotdir+'signal')

print 'Producing plots of the processed simulated data...'

fs=ip.srate #Sampling rate.
NFFT=1*fs
fmin=ip.fmin #Minimum frequency.
fmax=ip.fmax #Maximum frequency.
time=d['t'] #Time array (in s).
dt=time[1]-time[0] #Time resolution (in s).
signal_H1=d.H1_signal #Strain data for H1 (signal only).
signal_L1=d.L1_signal #Strain data for L1 (signal only).
noise_H1=d.H1_data-d.H1_signal #Noise for H1.
noise_L1=d.L1_data-d.L1_signal #Noise for H1.
strain_H1=d.H1_data #Strain data for H1 (signal+noise).
strain_L1=d.L1_data #Same for L1.
tmid=0.5*(max(time)+min(time)) #Instant at the middle of the data series (for convenience I pick a binary in the middle of the simulated data).
bind=abs(tmid-b.tc).argmin() #Index of binary with coalescence time closest to tmid.
tevent=b.tc[bind]

#Obtain PSD of the two detectors.
#Pxx_H1,freqs=mlab.psd(strain_H1,Fs=fs,NFFT=NFFT)
#Pxx_L1,freqs=mlab.psd(strain_L1,Fs=fs,NFFT=NFFT)
Pxx_H1,freqs=mlab.psd(noise_H1,Fs=fs,NFFT=NFFT)
Pxx_L1,freqs=mlab.psd(noise_L1,Fs=fs,NFFT=NFFT)
psd_H1=interp1d(freqs,Pxx_H1)
psd_L1=interp1d(freqs,Pxx_L1)

Sxx_H1,freqs=mlab.psd(signal_H1,Fs=fs,NFFT=NFFT)
Sxx_L1,freqs=mlab.psd(signal_L1,Fs=fs,NFFT=NFFT)

#Plot ASDs.
py.clf()
py.loglog(freqs,np.sqrt(Pxx_H1),'r',label='H1 noise')
py.loglog(freqs,np.sqrt(Sxx_H1),'b',label='H1 signal')
py.loglog(freqs,np.sqrt(Pxx_L1),'brown',label='L1 noise')
py.loglog(freqs,np.sqrt(Sxx_L1),'g',label='L1 signal')
py.axis([fmin,fmax,1e-24,1e-21])
py.grid('on')
py.ylabel('ASD (strain/rtHz)')
py.xlabel('Freq (Hz)')
py.legend(loc='upper center')
py.title('Simulated data %s' %sim)
py.savefig(oplotdir+'data_asd.png')

#Whiten data.
strain_H1_whiten=sm.whiten(strain_H1,psd_H1,dt)
strain_L1_whiten=sm.whiten(strain_L1,psd_L1,dt)
#Band pass data.
bb, ab = butter(4, [20.*2./fs, 300.*2./fs], btype='band')
strain_H1_whitenbp=filtfilt(bb, ab, strain_H1_whiten)
strain_L1_whitenbp=filtfilt(bb, ab, strain_L1_whiten)

#Plot whitened time series with signal in the middle.
py.clf()
py.plot(time-tevent,strain_H1_whitenbp,'r',label='H1 strain')
py.plot(time-tevent,strain_L1_whitenbp,'g',label='L1 strain')
py.legend(loc='lower left')
py.title('Whitened simulated data %s around binary %i' %(sim,bind))
py.xlim(-0.3,0.3)
py.ylim(-8.,8.)
py.savefig(oplotdir+'data_whitened.png')

print 'Producing S/N histograms...'

py.clf()
histi_noise,binsi_noise=np.histogram(s.noise, normed=True, bins=100)
histi_signal,binsi_signal=np.histogram(s.signal, normed=True, bins=100)
py.bar(binsi_noise[:-1],histi_noise,width=(binsi_noise[1]-binsi_noise[0]),alpha=0.5, color='blue', label='%i noise' %len(s.noise))
py.bar(binsi_signal[:-1],histi_signal,width=(binsi_signal[1]-binsi_signal[0]),alpha=0.5, color='red', label='%i signal, %i noise' %(len(s.signal),len(s.signal)))
histi_signal,binsi_signal=np.histogram(s['%s'%sim], normed=True, bins=100)
py.bar(binsi_signal[:-1],histi_signal,width=(binsi_signal[1]-binsi_signal[0]),alpha=0.5, color='green', label='1 signal, %i noise' %len(s.noise))
py.legend(loc='upper right')
py.ylim(0,1)
#py.xlim(-5,12)
py.savefig(oplotdir+'snr_hist')
