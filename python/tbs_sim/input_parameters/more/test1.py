from common_inputs import data_dir, plots_dir, ligo_psd_file

#Define input parameters.

srate = 2048 #Sampling rate, in samples/s.
dur = 10. #Duration of the observation, in s.
dur_ext=30. #Duration of the time series of the simulated waveforms; it should be at least as long as "dur" plus the typical duration of a chirp, to avoid the 'wrap around'.
nb = 10 #Number of injected binaries.
nr = 1000 #Number of realisations.
noise_real=1000 #Number of realisations of the noise (for the calculation of the SNR normalisation).
m1_all=30. #For the moment, all binaries will have the same mass and redshift.
m2_all=30.
z_all=0.25

#Inputs for the waveforms.
fmin=10. #Minimum frequency of the waveform (in Hz).
fmax=srate/2. #Maximum frequency of the waveform (in Hz).
fRef=20. #Reference frequency at which spins are calculated (in Hz).

