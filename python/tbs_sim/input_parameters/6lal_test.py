from common_inputs import *

#Define input parameters.
srate = 2048 #Sampling rate, in samples/s.
dur = 26. #Duration of the observation, in s.
dur_ext=40. #Duration of the time series of the simulated waveforms; it should be at least as long as "dur" plus the typical duration of a chirp, to avoid the 'wrap around'.
nb = 10 #Number of injected binaries.
nr = 1000 #Number of realisations.
noise_real=1000 #Number of realisations of the noise (for the calculation of the SNR normalisation).
m1_all=20. #Mass of first bh (in solar mass). For the moment, all binaries will have the same mass.
m2_all=20. #Mass of second bh (in solar mass).
z_all=0.14 #Redshift. For the moment, all binaries will have the same redshift.
start_time=1164405617 #The time series starts at this fixed (arbitrary) GPS time (in s) [Calculated using: astropy.time.Time(datetime.datetime(2016,11,28,22,0,0),format='datetime').gps ].

#Inputs for the waveforms.
fmin=20. #Minimum frequency of the waveform (in Hz).
fmax=srate/2. #Maximum frequency of the waveform (in Hz).
fRef=20. #Reference frequency at which spins are calculated (in Hz).

