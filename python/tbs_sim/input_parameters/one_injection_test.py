from common_inputs import *

#Define input parameters.
srate = 2048 #Sampling rate, in samples/s.
dur = 10. #Duration of the observation, in s.
dur_ext=20. #Duration of the time series of the simulated waveforms; it should be at least as long as "dur" plus the typical duration of a chirp, to avoid the 'wrap around'.
nb = 1 #Number of injected binaries.
nr = 1 #Number of realisations.

#I inject the loudest signal from 6lal_test:
# T_coal/s Redshift M1/solar_mass M2/solar_mass RA/rad DEC/rad Lum_dist/mpc Phase/rad Polarisation/rad Inclination/rad
#1.880000000000000071e+01 1.400000000000000133e-01 2.000000000000000000e+01 2.000000000000000000e+01 3.451601199036324008e+00 1.266588927914273910e-01 6.823445574783847860e+02 7.007165062069350969e-02 1.873344099265950691e+00 5.731837190845757712e-01

m1=20. #Mass of first black hole (all the same), in solar mass.
m2=20. #Mass of second black hole (all the same), in solar mass.
z=0.14 #Redshift of all binaries.
tc=6. #Coalescence time.
ras=3.451601199036324008e+00 #Right ascension.
dec=1.266588927914273910e-01 #Declination.
inc=5.731837190845757712e-01 #Inclination.
pol=1.873344099265950691e+00 #Polarisation angle.
phi=7.007165062069350969e-02 #GW phase.

noise_real=1000 #Number of realisations of the noise (for the calculation of the SNR normalisation).
start_time=1164405617 #The time series starts at this fixed (arbitrary) GPS time (in s) [Calculated using: astropy.time.Time(datetime.datetime(2016,11,28,22,0,0),format='datetime').gps ].

#Inputs for the waveforms.
fmin=20. #Minimum frequency of the waveform (in Hz).
fmax=srate/2. #Maximum frequency of the waveform (in Hz).
fRef=20. #Reference frequency at which spins are calculated (in Hz).

