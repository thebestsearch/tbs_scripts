#!/bin/python -u
import numpy as np
import modules.common as cm
from modules.gaussian_noise import gaussian_noise
from modules.generateIMRPhenomPv2 import FD
from modules.time_estimate import time_estimate
import os, sys, importlib

########################

#Description.

#Import a realisation of the ensemble of bhbs, and produce some fake data streams for the two LIGO detectors.

########################

#Input parameters.
inputfile=sys.argv[1] #Name of input file (in input_parameters). For example 'test2'.

#Derived inputs:
ip=importlib.import_module('input_parameters.%s' %inputfile)
sdir=ip.bhb_dir+'%s/' %(inputfile) #Directory of realisations of the sources.
odir=ip.sim_dir+'%s/' %(inputfile) #Directory of simulated data.
ligo_psd_file=ip.ligo_psd_file #File with LIGO power spectral density (PSD).
nr=ip.nr #Number of realisations to consider (it can't be larger than the number of existing realisations).
srate=ip.srate #Sampling rate (in s).
dur=ip.dur #Duration of the observation of each data stream (in s).
dur_ext=ip.dur_ext #Duration of the extended time series, for the simulated waveforms; it should be at least as long as "dur" plus the typical duration of a chirp, to avoid the 'wrap around'.

########################

#Main.

#cm.check_odir(odir) #If output directory does not exist, create it. If it exists and already contains files, ask if they should be replaced. If so, remove existing files, otherwise exit.

Sh=np.loadtxt(ligo_psd_file) #LIGO PSD.
tbin=1./srate #Time resolution (in s).
t_ext=np.arange(0., dur_ext, tbin) #Extended array of time samples (in s).
tsel=(t_ext<dur) #Mask that selects only time samples between 0 and "dur".
t=t_ext[tsel] #Array of time samples (in s).
deltaF=1./dur_ext #Frequency resolution for the generation of binary waveforms.

#Fixed parameters.
S1=[0,0,0] #Spin of the first black hole.
S2=[0,0,0] #Spin of the second black hole.
fmin=ip.fmin #Minimum frequency for the waveform (in Hz).
fmax=ip.fmax #Maximum frequency.
fRef=ip.fRef #Reference frequency.
#Define other parameters for the waveforms (whose meaning I ignore).
lambda1=0.
lambda2=0.
waveFlags=None
nonGRparams=None
amplitude0=0
phase0=0

te=time_estimate(nr-1) #Initialise this class to have an estimated computation time.

for ri in xrange(nr):
        te.display() #Shows the remaining computation time.
        te.increase() #Needed to calculate the remaining computation time.

	ofile='run%.3i' %(int(ri))

	#Generate noise in each detector (no need to use the extended time series).
	n1=gaussian_noise(Sh, srate, dur, T=1, N=None)[0] #Noise for detector 1 (H1).
	n2=gaussian_noise(Sh, srate, dur, T=1, N=None)[0] #Noise for detector 2 (L1).

	#Generate signal in each detector.
	ht1=np.zeros_like(n1)
	ht2=np.zeros_like(n2)

	#Load ensemble of bhbs and generate signal.
	b=cm.AttrDict(np.load(sdir+'run%.3i.npy' %int(ri))[()]) #Class binary; all properties will be loaded as "b.z" or "b.m1", etc.
	nb=len(b.z) #Number of binaries in this realisation.
	for bi in xrange(nb):
		dl=b.dl[bi] #Luminosity distance (in mpc).
		m1=b.m1[bi]*(1.+b.z[bi]) #Redshifted mass of first black hole (in solar mass).
		m2=b.m2[bi]*(1.+b.z[bi]) #Redshifted mass of second black hole (in solar mass).
		phi=b.phi[bi] #Phase (in rad).
		inc=b.inc[bi] #Inclination (in rad).
		ras=b.ras[bi] #Right ascension (in rad).
		dec=b.dec[bi] #Declination (in rad).
		pol=b.pol[bi] #Polarisation (in rad).
		tc=b.tc[bi] #Coalescence time (in s).

		fd=FD(fmin, fmax, deltaF, dl, m1, m2, S1, S2, fRef, inc, ras, dec, pol, phi, tc, 'H1')
		print fd
		print np.shape(fd)
		#td=np.fft.irfft(fd,len(t_ext))#[tsel]*srate #I create an extended time domain waveform, but select only the first "dur" seconds of it.
		td=np.fft.irfft(fd)#[tsel]*srate #I create an extended time domain waveform, but select only the first "dur" seconds of it.
		fd_rec=np.fft.rfft(td)
		print fd_rec
		print np.shape(fd_rec)
		print np.vdot(fd,fd_rec)
		raw_input('enter')
	
		hf2=FD(fmin, fmax, deltaF, dl, m1, m2, S1, S2, fRef, inc, ras, dec, pol, phi, tc, 'L1')
		ht2+=np.fft.irfft(hf2,len(t_ext))[tsel]*srate

	d1=ht1+n1
	d2=ht2+n2
	continue#####################
	#Save data.
	dicti={'t':t, 'H1_data':d1, 'L1_data':d2, 'H1_signal':ht1, 'L1_signal':ht2}

	np.save(odir+ofile, dicti)

