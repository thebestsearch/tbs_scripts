#!/bin/python -u
import numpy as np
import modules.common as cm
import modules.signal_model as sm
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as py
import modules.detectors as det
from modules.gaussian_noise import gaussian_noise
from modules.time_estimate import time_estimate
import os, sys, importlib

###############################

#Description.

#Calculates the theoretical S/N of the cross-correlation of two detectors (with the same PSD) for a given signal (made of a list of black holes).
#It also calculates the numerical S/N from the data.

###############################

#Input parameters.
inputfile=sys.argv[1] #Name of input file (in input_parameters). For example 'test2'.

#Derived inputs:
ip=importlib.import_module('input_parameters.%s' %inputfile)
sdir=ip.bhb_dir+'%s/' %(inputfile) #Directory of realisations of the sources.
ddir=ip.sim_dir+'%s/' %(inputfile) #Directory of realisations of fake data.
odir=ip.snr_dir #Directory of output file.
ofile='%s' %(inputfile) #Name of output file.
nr=ip.nr #Number of realisations to consider (it can't be larger than the number of existing realisations).
noise_real=ip.noise_real #Number of realisations of the noise (for the calculation of the SNR normalisation).
dur=ip.dur #Duration of the observation of each data stream (in s).
srate=ip.srate #Sampling rate.

###############################

#Main.

#cm.check_odir(odir) #If output directory does not exist, create it. If it exists and already contains files, ask if they should be replaced. If so, remove existing files, otherwise exit.

#Define some quantities.
f_FFT=np.fft.fftfreq(int(dur*srate),1./srate) #Array of frequencies for the FFTs.
f_full=np.logspace(np.log10(det.ligoHL.fmin), np.log10(det.ligoHL.fmax), 100) #Array of frequencies for the integral in the theoretical calculation.
noise_snr=np.zeros(noise_real) #Array of SNR in the presence of noise only.
signal_snr=np.zeros(nr) #Calculated SNR in the presence of signals.
theor_snr=np.zeros(nr) #Theoretical SNR.

#Obtain the normalisation of the S/N from pure noise.
print 'Calculating S/N of %i noise realisations...' %int(noise_real)
norm=1.
te=time_estimate(noise_real) #Initialise this class to have an estimated computation time.

for i in xrange(noise_real):
        te.display() #Shows the remaining computation time.
        te.increase() #Needed to calculate the remaining computation time.

        n1=gaussian_noise(det.psd_d, ip.srate, dur, T=1, N=None)[0] #Noise for detector 1 (H1).
        n2=gaussian_noise(det.psd_d, ip.srate, dur, T=1, N=None)[0] #Noise for detector 2 (L1).
        Sh=sm.Sh_fun(n1,n2)
        noise_snr[i]=sm.snr_calc(det.ligoHL, f_FFT, Sh, norm)
print

#Normalise the S/N so that its std is 1.
print 'Calculating S/N of %i signal realisations...' %int(nr)
norm=1./np.std(noise_snr)
noise_snr*=norm
te=time_estimate(nr) #Initialise this class to have an estimated computation time.

for ri in xrange(nr):
        te.display() #Shows the remaining computation time.
        te.increase() #Needed to calculate the remaining computation time.

	b=cm.AttrDict(np.load(sdir+'run%.3i.npy' %int(ri))[()]) #Properties of binaries.
	d=cm.AttrDict(np.load(ddir+'run%.3i.npy' %int(ri))[()]) #Simulated data.

	s1=d.H1_data #Simulated data for H1.
	s2=d.L1_data #Simulated data for L1.
	Sh=sm.Sh_fun(s1,s2)
	signal_snr[ri]=sm.snr_calc(det.ligoHL,f_FFT,Sh,norm)

	theor_snr[ri]=sm.snr_theo(dur,b,det.ligoHL)

print
print 'Summary:'
print
print 'Noise S/N'
print 'mean: ',np.mean(noise_snr)
print 'std: ',np.std(noise_snr)
print
print 'Signal S/N'
print 'mean: ',np.mean(signal_snr)
print 'std: ',np.std(signal_snr)
print
print 'Theoretical S/N'
print 'mean: ',np.mean(theor_snr)
print 'std: ',np.std(theor_snr)
print

#Pick realisation whose S/N is closest to the mean, or to a given value.
print 'Press enter to pick the realisation with S/N closest to the mean of the existing realisations.'
deci=raw_input('To pick the realisation with S/N closest to a given value, enter that value ->')
if deci.isdigit():
	ind=abs(signal_snr-np.float(deci)).argmin()
else:
	ind=abs(signal_snr-np.mean(signal_snr)).argmin()

print 'The "most average" realisation is run%.3i.' %int(ind)
print
print 'Calculate S/N of that same signal for %i different realisations of the noise.' %noise_real
snr_samesignal=np.zeros(noise_real)
te=time_estimate(noise_real) #Initialise this class to have an estimated computation time.

for i in xrange(noise_real):
        te.display() #Shows the remaining computation time.
        te.increase() #Needed to calculate the remaining computation time.

        d=cm.AttrDict(np.load(ddir+'run%.3i.npy' %int(ind))[()]) #Simulated data.
        s1=d.H1_signal #Simulated signal for H1.
        s2=d.L1_signal #Simulated signal for L1.
        n1=gaussian_noise(det.psd_d, ip.srate, dur, T=1, N=None)[0] #Noise for detector 1 (H1).
        n2=gaussian_noise(det.psd_d, ip.srate, dur, T=1, N=None)[0] #Noise for detector 2 (L1).

	s1+=n1
	s2+=n2

        Sh=sm.Sh_fun(s1,s2)
        snr_samesignal[i]=sm.snr_calc(det.ligoHL,f_FFT,Sh,norm)

print
print 'Signal S/N'
print 'mean: ',np.mean(snr_samesignal)
print 'std: ',np.std(snr_samesignal)

#Save output data.
dicti={'noise':noise_snr, 'signal':signal_snr, 'run%.3i'%int(ind):snr_samesignal}
np.save(odir+ofile, dicti)

