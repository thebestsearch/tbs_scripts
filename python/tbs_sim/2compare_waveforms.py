import numpy as np
#from modules.generateIMRPhenomPv2 import FD
import modules.common as cm
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as py
import lalsimulation as lalsim
import lal

###############################

#Description.

#Plot injected waveform and waveform recovered by lal.

###############################

#Inputs.

lalfile='/home/pablo.rosado/public_html/files/lalinference/TBS_one_injection_test/lalinferencenest/IMRPhenomPv2pseudoFourPN/8s/lalinferencenest/IMRPhenomPv2pseudoFourPN/4s/1164405623.0-0/H1L1/summary_statistics.dat'
injfile='/home/pablo.rosado/projects/tbs/tbs_data/tbs_sim/combined/one_injection_test/bhb_pop.txt'
oplotdir='/home/pablo.rosado/public_html/2compare_waveforms/'

#Inputs from one_injection_test.py.
srate = 2048 #Sampling rate, in samples/s.
dur = 10. #Duration of the observation, in s.
dur_ext=20. #Duration of the time series of the simulated waveforms; it should be at least as long as "dur" plus the typical duration of a chirp, to avoid the 'wrap around'.

#Fixed parameters.
fmax=srate/2. #Maximum frequency of the waveform (in Hz).
fRef=20. #Reference frequency at which spins are calculated (in Hz).

#Define other parameters for the waveforms (whose meaning I ignore).
lambda1=0.
lambda2=0.
waveFlags=None
nonGRparams=None
amplitude0=0
phase0=0

###############################

#I will take FD and maybe modify it a bit, from the original in modules.generateIMRPhenomPv2.

def FD(fmin, fmax, deltaF, dist, m1, m2, S1, S2, fRef, iota, RA, DEC, psi, phi, tc, ifo):
# dist in mpc
# m1, m2 in solar masses
# S1,S2 are spin vectors, e.g., S1=[0,0.3,0.2] with sqrt(S1[0] + S1[1] + S1[2]) <= 1
# fRef: reference frequency at which spins are calculated
# iota: inclination
# RA, DEC
# psi: polarization angle
# phi: orbital phase at fRef
# tc: coalescence time at geocenter (a GPS time)
# ifo: either 'H1', 'L1' or 'V1'

# Returns the Fourier transform of the waveform strain in ifo including the timeshift from geocenter

        tgps = lal.LIGOTimeGPS(tc)
        gmst = lal.GreenwichMeanSiderealTime(tgps)

        #Distance and masses have to be in SI units.
        dist *= 1e6*lal.lal.PC_SI
        m1 *= lal.lal.MSUN_SI
        m2 *= lal.lal.MSUN_SI

        #Plus and cross polarisation waveforms in the frequency domain.
        hplus,hcross = lalsim.SimInspiralChooseFDWaveform(phi, deltaF, m1, m2, S1[0], S1[1], S1[2], S2[0], S2[1], S2[2], fmin, fmax, fRef, dist, iota, 0.0, 0.0, None, None, 0, 0, lalsim.IMRPhenomPv2)

        #Start the waveform at frequency fmin.
        h_p = hplus.data.data[(fmin/hplus.deltaF):]
        h_c = hcross.data.data[(fmin/hcross.deltaF):]

        #The coalescence (more precisely, the maximum of the waveform) occurs at 'tc' at the geocenter. Calculate time delay between geocenter and the chosen detector.
        if ifo == 'H1':
                diff = lal.LALDetectorIndexLHODIFF
        elif ifo == 'L1':
                diff = lal.LALDetectorIndexLLODIFF
        elif ifo == 'V1':
                diff = lal.LALDetectorIndexVIRGODIFF
        else:
                raise ValueError('detector not recognized: ' + ifo)
        timedelay = lal.TimeDelayFromEarthCenter(lal.CachedDetectors[diff].location, RA, DEC, tgps)
        timeshift = tc + timedelay

        #timeshift=0.0 #TEST.

        #Obtain antenna pattern factors for the detector for a source at location RA,DEC at the precise instant of coalescence (and assume these factors do not change significantly during the entire chirp).
        fplus, fcross = lal.ComputeDetAMResponse(lal.CachedDetectors[diff].response, RA, DEC, psi, gmst)

        #Calculate the observed strain at the detector, properly shifting the waveform from geocenter to detector frame.
        h = np.zeros_like(h_p, dtype=complex)
        pit = np.pi*timeshift
        if timeshift != 0.0:
                shift = complex(1.0, 0)
                dshift = complex(-2.*np.sin(pit*deltaF)*np.sin(pit*deltaF), -2.*np.sin(pit*deltaF)*np.cos(pit*deltaF))
                for i in xrange(0,h_p.size):
                        h[i] = shift*(fplus*h_p[i] + fcross*h_c[i])
                        shift += shift*dshift
        else:
                h = (fplus*h_p) + (fcross*h_c)

        return h

###############################

cm.check_odir(oplotdir) #If output directory does not exist, create it. If it exists and already contains files, ask if they should be replaced. If so, remove existing files, otherwise exit.

#Load injected parameters.
inj=np.loadtxt(injfile,skiprows=1) #Load injected parameters.
# T_coal/s 	 Redshift 	 M1/solar_mass 	 M2/solar_mass 	 RA/rad 	 DEC/rad 	 Lum_dist/mpc 	 Phase/rad 	 Polarisation/rad 	 Inclination/rad
z=inj[1]
m1=inj[2]
m2=inj[3]
ras=inj[4]
dec=inj[5]
dl=inj[6]
phi=inj[7]
pol=inj[8]
inc=inj[9]
tc=6.

#Create dictionary for main injected parameters.
dinj={}
dinj['z']=z
dinj['m1']=m1
dinj['m2']=m2
dinj['S1']=[0,0,0]
dinj['S2']=[0,0,0]

#Create dictionary for main recovered lal parameters and injected ones.
dlal={}
openlal=open(lalfile,'r')
for line in openlal:
	key=line.split()[0] #Quantity on the left-most column.
	item=line.split()[2] #Take maxL value.
	if key=='redshift':
		dlal['z']=float(item)
	elif key=='m1_source':
		dlal['m1']=float(item)
	elif key=='m2_source':
		dlal['m2']=float(item)
	elif key=='a1z':
		dlal['S1']=[0,0,float(item)]
	elif key=='a2z':
		dlal['S2']=[0,0,float(item)]

#The rest of parameters will be the same for both cases (identical to the injected ones).
dinj['ras']=ras
dinj['dec']=dec
dinj['dl']=dl
dinj['phi']=phi
dinj['pol']=pol
dinj['inc']=inc
dinj['tc']=tc

dlal['ras']=ras
dlal['dec']=dec
dlal['dl']=dl
dlal['phi']=phi
dlal['pol']=pol
dlal['inc']=inc
dlal['tc']=tc

#For convenience I transform the dictionaries into classes.
injb=cm.AttrDict(dinj)
lalb=cm.AttrDict(dlal)

#Define some quantities and functions.
tbin=1./srate #Time resolution (in s).
t_ext=np.arange(0., dur_ext, tbin) #Extended array of time samples (in s).
tsel=(t_ext<dur) #Mask that selects only time samples between 0 and "dur".
t=t_ext[tsel] #Array of time samples (in s).
deltaF=1./dur_ext #Frequency resolution for the generation of binary waveforms.
fmin=10. #Minimum frequency of the waveform (in Hz).

def TDwaveform(b):
	'''Generates waveforms of H1 and L1 for a binary described by "b".'''
        #Generate signal in each detector.
        ht1=np.zeros_like(t)
        ht2=np.zeros_like(t)

	dl=b.dl #Luminosity distance (in mpc).
	m1=b.m1*(1.+b.z) #Redshifted mass of first black hole (in solar mass).
	m2=b.m2*(1.+b.z) #Redshifted mass of second black hole (in solar mass).
	phi=b.phi #Phase (in rad).
	inc=b.inc #Inclination (in rad).
	ras=b.ras #Right ascension (in rad).
	dec=b.dec #Declination (in rad).
	pol=b.pol #Polarisation (in rad).
	tc=b.tc #Coalescence time (in s).
	S1=b.S1 #Spin of BH 1.
	S2=b.S2 #Spin of BH 2.

	hf1=FD(fmin, fmax, deltaF, dl, m1, m2, S1, S2, fRef, inc, ras, dec, pol, phi, tc, 'H1')
	ht1+=np.fft.irfft(hf1,len(t_ext))[tsel]*srate #I create an extended time domain waveform, but select only the first "dur" seconds of it.

	hf2=FD(fmin, fmax, deltaF, dl, m1, m2, S1, S2, fRef, inc, ras, dec, pol, phi, tc, 'L1')
	ht2+=np.fft.irfft(hf2,len(t_ext))[tsel]*srate

	return ht1, ht2

#Produce injected and recovered waveforms for H1 and L1.
inj_H,inj_L=TDwaveform(injb)
lal_H,lal_L=TDwaveform(lalb)

#Produce plots.

py.clf()
py.plot(t,inj_H,color='red', label='H1 injected signal')
py.plot(t,inj_L,color='blue', label='L1 injected signal')
py.ylim(-1e-21,1e-21)
py.xlabel('t/s')
py.ylabel('strain of injected signal only')
py.legend(loc='upper left')
py.savefig(oplotdir+'injected_signal')

py.clf()
py.plot(t,lal_H,color='red', label='H1 recovered signal')
py.plot(t,lal_L,color='blue', label='L1 recovered signal')
py.ylim(-1e-21,1e-21)
py.xlabel('t/s')
py.ylabel('strain of recovered signal only')
py.legend(loc='upper left')
py.savefig(oplotdir+'recovered_signal')

