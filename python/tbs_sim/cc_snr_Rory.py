import numpy as np
import matplotlib
matplotlib.use('Agg')
import pylab as py
from modules.time_estimate import time_estimate
from modules.gaussian_noise import gaussian_noise
import modules.detectors as det
import modules.signal_model as sm

#######################

#Description.

#Generate two time series (for H1 and L1) from the injections made by Rory in the frequency domain.

#Calculate the cross-correlation S/N.

#Do the same for the signal only (without noise).

#Watch out for abrupt time series that could affect the S/N.

#######################

#Inputs.

dir="/home/rory.smith/test/TBStest/inspinj_test/lalinferencenest/IMRPhenomPv2pseudoFourPN/4s/ROQdata/"
inj_dirs=np.arange(1,11)
H1n_name="data-dumpH1-freqData.dat"
H1d_name="data-dumpH1-freqDataWithInjection.dat"
L1n_name="data-dumpL1-freqData.dat"
L1d_name="data-dumpL1-freqDataWithInjection.dat"
oplotdir='/home/pablo.rosado/public_html/'

srate=2048.
dur_i=4. #Duration of time series of each binary.
dur=dur_i*len(inj_dirs) #Duration of the entire time series.
tbin=1./srate #Time resolution (in s).
noise_real=100 #Number of realisations of the noise, to calculate the normalisation of the S/N.

#######################

f_FFT=np.fft.fftfreq(int(dur*srate),1./srate) #Array of frequencies for the FFTs.
t_i=np.arange(0.,dur_i,tbin) #Time array for each binary.
t=np.arange(0.,dur,tbin) #Entire time array.
noise_snr=np.zeros(noise_real) #Array of SNR in the presence of noise only.
H1d_t=[]
L1d_t=[]

for bi in xrange(len(inj_dirs)):
	H1n_file=dir+str(inj_dirs[bi])+'/'+H1n_name
	H1d_file=dir+str(inj_dirs[bi])+'/'+H1n_name
	L1n_file=dir+str(inj_dirs[bi])+'/'+L1n_name
	L1d_file=dir+str(inj_dirs[bi])+'/'+L1n_name

	#if bi==0: #Load frequencies only once.
	#	f=np.loadtxt(H1n_file,usecols=(0,))
	#H1d=np.loadtxt(H1n_file,usecols=(1,))+1j*np.loadtxt(H1n_file,usecols=(2,))
       	#L1d=np.loadtxt(L1n_file,usecols=(1,))+1j*np.loadtxt(L1n_file,usecols=(2,))

	H1d_r=np.loadtxt(H1n_file,usecols=(1,))
	H1d_r[np.isnan(H1d_r)|(H1d_r==np.inf)|(H1d_r==-np.inf)]=1.
	H1d_r[H1d_r>1.]=1.
	L1d_r=np.loadtxt(L1n_file,usecols=(1,))
	L1d_r[np.isnan(L1d_r)|(L1d_r==np.inf)|(L1d_r==-np.inf)]=1.
	L1d_r[L1d_r>1.]=1.
	H1d_i=np.loadtxt(H1n_file,usecols=(2,))
	H1d_i[np.isnan(H1d_i)|(H1d_i==np.inf)|(H1d_i==-np.inf)]=1.
	H1d_i[H1d_i>1.]=1.
	L1d_i=np.loadtxt(L1n_file,usecols=(2,))
	L1d_i[np.isnan(L1d_i)|(L1d_i==np.inf)|(L1d_i==-np.inf)]=1.
	L1d_i[L1d_i>1.]=1.

	H1d=H1d_r+1j*H1d_i
	L1d=L1d_r+1j*L1d_i

	H1d_t.extend(np.fft.irfft(H1d,len(t_i))*srate)
	L1d_t.extend(np.fft.irfft(L1d,len(t_i))*srate)

	raw_input('na')
H1d_t=np.array(H1d_t)
L1d_t=np.array(L1d_t)

#Obtain the normalisation of the S/N from pure noise.
print 'Calculating S/N of %i noise realisations...' %int(noise_real)
norm=1.
te=time_estimate(noise_real) #Initialise this class to have an estimated computation time.

for i in xrange(noise_real):
        te.display() #Shows the remaining computation time.
        te.increase() #Needed to calculate the remaining computation time.

        n1=gaussian_noise(det.psd_d, srate, dur, T=1, N=None)[0] #Noise for detector 1 (H1).
        n2=gaussian_noise(det.psd_d, srate, dur, T=1, N=None)[0] #Noise for detector 2 (L1).
        Sh=sm.Sh_fun(n1,n2)
        noise_snr[i]=sm.snr_calc(det.ligoHL, f_FFT, Sh, norm)
print

#Normalise the S/N so that its std is 1.
print 'Calculating S/N of Rorys simulated data...'
norm=1./np.std(noise_snr)
noise_snr*=norm

Sh=sm.Sh_fun(H1d_t,L1d_t)
signal_snr=sm.snr_calc(det.ligoHL,f_FFT,Sh,norm)
print 'S/N: ',signal_snr
print

print 'Calculate S/N of that same signal for %i different realisations of the noise.' %noise_real
snr_samesignal=np.zeros(noise_real)
te=time_estimate(noise_real) #Initialise this class to have an estimated computation time.

for i in xrange(noise_real):
        te.display() #Shows the remaining computation time.
        te.increase() #Needed to calculate the remaining computation time.

	s1=H1d_t
	s2=L1d_t
        n1=gaussian_noise(det.psd_d, srate, dur, T=1, N=None)[0] #Noise for detector 1 (H1).
        n2=gaussian_noise(det.psd_d, srate, dur, T=1, N=None)[0] #Noise for detector 2 (L1).

        s1+=n1
        s2+=n2

        Sh=sm.Sh_fun(s1,s2)
        snr_samesignal[i]=sm.snr_calc(det.ligoHL,f_FFT,Sh,norm)

print
print 'Signal S/N'
print 'mean: ',np.mean(snr_samesignal)
print 'std: ',np.std(snr_samesignal)

