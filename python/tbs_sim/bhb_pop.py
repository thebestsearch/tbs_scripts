#!/bin/python -u
import numpy as np
#import inputs_fake_signal as ip
import modules.common as cm
from scipy import integrate
import modules.signal_model as sm
import os, sys, importlib

########################

#Description.

#Create different realisations of the ensemble of black hole binaries.
#It has to be run as './bhb_pop.py input_file_name' where input_file_name is the name of the file (without extension) in 'input_parameters/' with the inputs to be used.
#For example './bhb_pop.py test1'

########################

#Input parameters.
inputfile=sys.argv[1] #Name of input file (in input_parameters). For example 'test2'.

#Derived inputs:
ip=importlib.import_module('input_parameters.%s' %inputfile)
odir=ip.bhb_dir+'%s/' %(inputfile) #Output directory for this script.
nb=ip.nb #Number of injected binaries.
nr=ip.nr #Number of realisations.
dur=ip.dur #Duration of the observation (all binaries will coalesce within this interval of time, in s).
m1_all=ip.m1_all #Mass of first black hole (all the same), in solar mass.
m2_all=ip.m2_all #Mass of second black hole (all the same), in solar mass.
z_all=ip.z_all #Redshift of all binaries.

########################

#Main.

cm.check_odir(odir) #If output directory does not exist, create it. If it exists and already contains files, ask if they should be replaced. If so, remove existing files, otherwise exit.

print 'Creating %i realisations of the ensemble of BH binaries...' %nr

print
print 'For the moment I will distribute binaries in a very artificial way: one every second.' 
print

for ri in xrange(ip.nr):
	ofile='run%.3i' %int(ri)

	#To begin with something simple, I will just input the properties of the BHs manually.
	#tc=np.random.uniform(0., dur, nb) #Array of coalescence time.
	#tc=np.linspace(2.,20.,10)-0.2+3 #For this test only! I add 10 seconds of pure noise at the beginning and at the end, and make binaries coalesce at 0.2 seconds before each full second (after the 3 initial seconds).###############
	tc=np.linspace(2.,dur,nb)
	#m1=np.random.uniform(m1_min, m1_max, nb) #Array of masses of one black hole.
	#m2=np.random.uniform(m2_min, m2_max, nb) #Array of masses of the other black hole.
	m1=np.ones(nb)*m1_all#################Impose that all bh have the same mass.
	m2=np.ones(nb)*m2_all
	#z=np.random.uniform(z_min, z_max, nb) #Array of redshifts.
	z=np.ones(nb)*z_all #################Impose that all bh are at the same distance.
	mch=sm.mchirp(m1,m2) #Chirp mass (in Mpc).

        #Calculate comoving distance in Mpc.
        cmdist=np.zeros(nb)
        cmdist_const=cm.light/(cm.hub0*cm.h0)/cm.mpc
        for bi in xrange(nb):
                cmdist[bi]=integrate.quad(lambda zi: (cm.omm*(1.+zi)**3.+cm.omv)**(-0.5), 0, z[bi])[0]*cmdist_const
	dl=(1.+z)*cmdist #Luminosity distance (in Mpc).

        #Get all other quantities from random distributions.
        #ras=np.random.uniform(0., 2.*np.pi, nb) #Right ascension.
        #dec=np.arccos(np.random.uniform(-1., 1., nb))-np.pi/2. #Declination.
        #inc=np.arccos(np.random.uniform(-1., 1., nb)) #Inclination of the binary.
        #pol=np.random.uniform(0., np.pi, nb) #Polarization (at some reference frequency).
        #phi=np.random.uniform(0., 2.*np.pi, nb) #GW phase (at some reference frequency).

	ras=np.ones(nb)*ip.ras_all###########################All the same.
	dec=np.ones(nb)*ip.dec_all
	inc=np.ones(nb)*ip.inc_all
	pol=np.ones(nb)*ip.pol_all
	phi=np.ones(nb)*ip.phi_all
	
	#Create a dictionary with the properties of all binaries in each realisation.
	binary={'tc':tc, 'm1':m1, 'm2':m2, 'z':z, 'dl':dl, 'mch':mch, 'inc':inc, 'phi':phi, 'ras':ras, 'dec':dec, 'pol':pol}

	#Save one dictionary for each realisation.
	np.save(odir+ofile, binary)

