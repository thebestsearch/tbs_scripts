#!/bin/python -u
import numpy as np
#import inputs_fake_signal as ip
import modules.common as cm
from scipy import integrate
import modules.signal_model as sm
import os, sys, importlib

########################

#Description.

#Create one realisation of the ensemble of black hole binaries, with fixed parameters.
#It has to be run as './bhb_pop.py input_file_name' where input_file_name is the name of the file (without extension) in 'input_parameters/' with the inputs to be used.
#For example './bhb_pop.py test1'

########################

#Input parameters.
inputfile=sys.argv[1] #Name of input file (in input_parameters). For example 'test2'.
ofile='run000' #Name of output file.

#Derived inputs:
ip=importlib.import_module('input_parameters.%s' %inputfile)
odir=ip.bhb_dir+'%s/' %(inputfile) #Output directory for this script.
dur=ip.dur #Duration of the observation (all binaries will coalesce within this interval of time, in s).

#Properties of the injected binary.
m1=np.array([ip.m1]) #Mass of first black hole (all the same), in solar mass.
m2=np.array([ip.m2]) #Mass of second black hole (all the same), in solar mass.
z=np.array([ip.z]) #Redshift of all binaries.
tc=np.array([ip.tc]) #Coalescence time.
ras=np.array([ip.ras]) #Right ascension.
dec=np.array([ip.dec]) #Declination.
inc=np.array([ip.inc]) #Inclination.
pol=np.array([ip.pol]) #Polarisation angle.
phi=np.array([ip.phi]) #GW phase.

########################

#Main.

cm.check_odir(odir) #If output directory does not exist, create it. If it exists and already contains files, ask if they should be replaced. If so, remove existing files, otherwise exit.

print 'Creating one realisation of the ensemble of BH binaries...'

#Derive some quantities.
mch=sm.mchirp(m1,m2) #Chirp mass (in Mpc).

#Calculate comoving distance in Mpc.
cmdist_const=cm.light/(cm.hub0*cm.h0)/cm.mpc
cmdist=integrate.quad(lambda zi: (cm.omm*(1.+zi)**3.+cm.omv)**(-0.5), 0, z)[0]*cmdist_const
dl=(1.+z)*cmdist #Luminosity distance (in Mpc).

#Create a dictionary with the properties of all binaries in each realisation.
binary={'tc':tc, 'm1':m1, 'm2':m2, 'z':z, 'dl':dl, 'mch':mch, 'inc':inc, 'phi':phi, 'ras':ras, 'dec':dec, 'pol':pol}

#Save one dictionary.
np.save(odir+ofile, binary)

