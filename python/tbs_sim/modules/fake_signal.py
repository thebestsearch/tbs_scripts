import numpy as np

def fake_signal(Ns, fmin, fs):
        '''Generates an array of frequencies and an array of amplitudes of fake GW monochromatic sources.
        +Ns is the number of injected signals.
        +fmin is the minimum frequency.
        +fs is the sampling rate.'''
        fmax = fs*0.5 #Maximum frequency of injected signasl (Nyquist frequency).

        #f0 = fmin + (fmax-fmin)*np.random.random(Ns) #Array of Ns frequencies between fmin and fmax. Is not this the same as a uniform distribution???
        f0 = np.random.uniform(low = fmin, high = fmax, size = Ns) #Array of Ns frequencies between fmin and fmax.
        return f0

if __name__=='__main__': #When running this file, the following actions will be executed. Otherwise, if this file is imported, nothing else will happen.
	Ns = 300 #Number of injected sources.
	fs = 2048 #Sampling rate, in samples/s.
	fmin = 10 #Minimum frequency of the injected signals.
	fmax = fs/2. #Maximum frequency of the injected signals.
	f0 = fake_signal(Ns, fmin, fs)
	import pylab as py
	py.ion()
	py.plot(f0)
	raw_input('enter')
