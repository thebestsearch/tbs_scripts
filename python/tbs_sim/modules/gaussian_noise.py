#!/usr/bin/env python
import numpy as np
import scipy.interpolate as ip

###############

#Description.

#Matlab function "gaussian_noise" by Eric Thrane (eric.thrane@ligo.org) adapted to Python by Pablo A. Rosado (prosado@swin.edu.au).
#From Eric:
'''
function h = gaussian_noise(Sh, sampleRate, duration, T, N)
% function h = gaussian_noise(Sh, sampleRate, duration, T, N)
% E. Thrane: (borrows from code in readTimeSeries2) creates a NxT array of h(t)
% given a noise power spectrum Sh
%   T = the number of trials
%   sampleRate = sample rate
%   duration = signal duration
%   Sh = noise power spectral density
%   N is the number of data points
'''

###############

#Functions.

def gaussian_noise(Sh, sampleRate, duration, T=1, N=None):
	'''Generates Gaussian noise, where:
	+Sh is a noise power spectral density.
	+sampleRate is the sample rate, in samples/s.
	+duration is the signal duration, in s.
	+T is the number of trials.
	+N is the number of data poins.
	'''

	#Calculate N or use user-specified value.
	if not N: N=duration*sampleRate

	#Prepare FFT.
	numFreqs = N/2-1 if N%2 == 0 else (N-1)/2 #Number of frequency bins.
	deltaF = 1./duration #Size of f-bin, in Hz.
	flow = deltaF #Lowest frequency, in Hz.
	f = np.arange(flow, deltaF*(numFreqs+1), deltaF) #Array of frequency, in Hz.
	amp_values = Sh[:,1] #Values of PSD amplitude.
	f_transfer1 = Sh[:,0] #Frequency from PSD array.
	Pf1 = np.interp(f, f_transfer1, amp_values) #PSD linearly interpolated at the f-bins of interest.
	Pf1[((f<min(f_transfer1))&(f>max(f_transfer1)))]=1e90 #To avoid problems outside of the interpolated region, I make the PSD go to infinity on the borders.
	
	deltaT = 1./sampleRate #Time resolution.
	norm1_i = np.sqrt( N*1./(2.*deltaT) ) * np.sqrt( Pf1 )
	norm1 = np.tile(norm1_i, (T,1) ).T
	re1 = norm1*np.sqrt(0.5)*np.random.randn(numFreqs, T)
	im1 = norm1*np.sqrt(0.5)*np.random.randn(numFreqs, T)
	z1 = re1 + im1*1j
	
	htilde1 = z1 	#Frequency domain solution for htilde1 in terms of z1.???
	otilde1 = htilde1*1. #Convolve data with instrument transfer function.???
	#Set DC and Nyquist = 0, then add negative frequency parts in propert order.
	if N%2 == 0:
		otilde1 = np.vstack(( np.zeros(T) , otilde1 , np.zeros(T), np.flipud( np.conj(otilde1) ) )) #Note that most negative frequency is -f_Nyquist when N is even.
	else:
		otilde1 = np.vstack(( np.zeros(T) , otilde1 , np.flipud( np.conj(otilde1) ) )) #No Nyquist frequency when N is odd.

	#Fourier transform back to the time domain.
	o1_data = np.fft.ifft(otilde1.T)

	#Take real part (imaginary part = 0 to round-off).
	h = np.real(o1_data)

	return h

###############

#Test it!

if __name__=='__main__': #When running this file, the following actions will be executed. Otherwise, if this file is imported, nothing else will happen.
	Sh=np.loadtxt('../../../tbs_data/input/ZERO_DET_high_P_psd.txt')
	nr=100
	T=2
	#h=gaussian_noise(Sh, 2048,8,T)
	import pylab as py

	py.ion()
	for ploti in xrange(nr):
        	h=gaussian_noise(Sh, 2048,10,T)
		py.clf()
		py.plot(h[0])
		py.plot(h[1])
		raw_input('enter')
	#Compare to one example obtained with gaussian_noise.m.#########
	#o1_data_ml=np.loadtxt('../../../tbs_data/output/o1_data.txt') ###########
	#py.plot(np.real(o1_data_ml))##########
	#py.show()
	raw_input('enter')
