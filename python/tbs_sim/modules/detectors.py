import numpy as np
from scipy.interpolate import interp1d
import input_parameters.common_inputs as ip
import common as cm

#####################

#Description.

#Useful functions and quantities related to the detectors.

#####################

#Inputs.

ligo_psd_file=ip.ligo_psd_file #File with LIGO power spectral density (PSD).
orf_lh_file=ip.orf_lh_file #Overlap reduction function of the detector pair H-L.

#####################

#Main.

psd_d=np.loadtxt(ligo_psd_file) #LIGO PSD data.
psd_f,psd_a=psd_d[:,0],psd_d[:,1] #Frequency and amplitude of the PSD.
lpsd=interp1d(np.log(psd_f),np.log(psd_a),bounds_error=False,fill_value=np.inf) #Function that interpolates log(f) over log(psd).
def psd_interp(f):
        '''Interpolated PSD of LIGO, as a function of frequency "f" (in Hz).'''
        return np.exp(lpsd(np.log(f)))

orf_d=np.loadtxt(orf_lh_file) #ORF of LIGO-H and LIGO-L.
orf_f,orf_a=orf_d[:,0],orf_d[:,1] #Frequency and amplitude of the ORF.
def orf_interp(f):
        '''Interpolated overlap reduction function as a function of "f" (in Hz).'''
        return interp1d(orf_f,orf_a)(f) #Function that interpolates f over the ORF.

ligofmin=max(min(orf_f),min(psd_f)) #Minimum frequency that can be considered in the calculations.
ligofmax=min(max(orf_f),max(psd_f)) #Maximum frequency that can be considered in the calculations.
ligoHL=cm.AttrDict({'fmin':ligofmin, 'fmax':ligofmax, 'psd1':psd_interp, 'psd2':psd_interp, 'orf':orf_interp}) #Detector pair: this is a class that returns the interpolated PSD of the two LIGO detectors (assumed identical), the overlap reduction function of the pair, and the minimum and maximum frequency where those functions are defined.

