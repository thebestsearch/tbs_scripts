

'''
Paul Lasky
Handy tools
'''

import numpy as np
from scipy.interpolate import interp1d

def m12_to_mc(m1, m2):
    # convert m1 and m2 to chirp mass
    return (m1*m2)**(3./5.) / (m1 + m2)**(1./5.)

def m12_to_symratio(m1, m2):
    # convert m1 and m2 to symmetric mass ratio
    return m1 * m2 / (m1 + m2)**2


def h_tot(hp, Fp, hx, Fx):
    # calculate h_total from plus and cross polarizations and antenna pattern
    return Fp * hp + Fx * hx


import code
import sys

def keyboard(banner=None):
    ''' Function that mimics the matlab keyboard command '''
    # use exception trick to pick up the current frame
    try:
        raise None
    except:
        frame = sys.exc_info()[2].tb_frame.f_back
    print "# Use quit() to exit :) Happy debugging!"
    # evaluate commands in current namespace
    namespace = frame.f_globals.copy()
    namespace.update(frame.f_locals)
    try:
        code.interact(banner=banner, local=namespace)
    except SystemExit:
        return 


def fft(ht, Fs):
    '''
    performs an FFT while keeping track of the frequency bins
    assumes input time series is real
    
    ht = time series
    Fs = sampling frequency

    returns
    hf = single-sided FFT of ft normalised to units of strain / sqrt(Hz)
    f = frequencies associated with hf
    '''
    # add one zero padding if time series does not have even number of sampling times
    if np.mod(len(ht), 2) == 1:
        ht = np.append(ht, 0)
    LL = len(ht)
    # frequency range
    ff = Fs / 2 * np.linspace(0, 1, LL/2+1)

    # calculate FFT
    hf = np.fft.fft(ht)
    # only keep first half
    hf = hf[:LL/2+1]

    # normalise to units of strain / sqrt(Hz)
    hf = hf / Fs
    
    return hf, ff


def inner_product(aa, bb, freq, PSD):
    '''
    Calculate the inner product defined in the matched filter statistic
    '''
    # interpolate the PSD to the freq grid
    PSD_interp_func = interp1d(PSD[:, 0], PSD[:, 1], bounds_error=False, fill_value=np.inf)
    PSD_interp = PSD_interp_func(freq)

    # caluclate the inner product
    integrand = np.conj(aa) * bb / PSD_interp

    df = freq[1] - freq[0]
    integral = np.sum(integrand) * df

    product = 4. * np.real(integral)

    return product


def snr_exp(aa, freq, PSD):
    '''
    Calculates the expectation value for the optimal matched filter SNR
    '''
    return np.sqrt(inner_product(aa, aa, freq, PSD))




