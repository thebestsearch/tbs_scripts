#!/usr/bin/env python

#####################################

#Description.

#Something Bayesian.

#####################################

#Import necessary modules.

from __future__ import print_function, absolute_import
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as py
import numpy as np
from scipy.interpolate import interp1d
import pymultinest
import json
import tools_pdl.tools as tools
from scipy import signal

#####################################

#Define inputs.

#Input signal parameters (sine gaussian pulse).
freq_real = 200. #Frequency (in Hz).
#phase_real = np.pi #Phase (in rad).
amp_real = 1e-21 #Amplitude.
tau_real = 0.01 #Time of maximum of the pulse (in seconds).

#Multinest parameters.
f_min = 100.; f_max = 500.
#phase_min = 0.; phase_max = 2.*np.pi
amp_min = 1e-25; amp_max = 1e-20
tau_min = 0.001; tau_max = 0.1
#parameters = ["freq", "phase", "amplitude", "tau"]
parameters = ["freq", "amplitude", "tau"]

#Other parameters.
plotall = True #True to show plots.
odir = 'chains/' #Name of output directory.
chain_base = odir + 'freq_phase_' #Basename for all output files.
#true_params = [freq_real, phase_real, amp_real, tau_real]
true_params = [freq_real, amp_real, tau_real]
bins = 100 #Number of bins for posterior plots.
oplotdir='/home/pablo.rosado/public_html/pymultinest/sine_gaussian/' #Directory for output plots.

#####################################

#Define some functions and quantities.

#Create signal.
time = np.linspace(0, 0.5, 10000) #Array of times (in seconds).
Fs = 1./(time[1] - time[0]) #Sampling rate (number of samples per second).

def waveform(freq, tau):
        '''Sine-Gaussian signal of frequency "freq" (in Hz) and maximum at time "tau" (in seconds).'''
        ht = signal.gausspulse(time-tau, fc=freq)
        hf, ff = tools.fft(ht, Fs)
        return hf, ff

hf, ff = waveform(freq_real, tau_real)
hf = amp_real * hf #Normalise waveeform amplitude to the desired one.

#Load LIGO PSD, and let it be a function that can interpolate for different frequency arrays.
PSD = np.loadtxt('./ZERO_DET_high_P_psd.txt')
PSD_interp_func = interp1d(PSD[:, 0], PSD[:, 1], bounds_error=False, fill_value=np.inf)
PSD_interp = PSD_interp_func(ff)

#Other derived quantities.
n_params = len(parameters)

#####################################

#Plot signal.

if plotall:
        fig = py.figure()
        ax = py.gca()
        ax.loglog(ff, abs(hf))
        ax.loglog(ff, np.sqrt(PSD_interp))
        fig.savefig(oplotdir+'signal.png')
        #py.show(block = False)
        #raw_input('enter')

#####################################

#Prepare necessary inputs for multinest.

def prior(cube, ndim, nparams):
	'''Maps the cube "cube" from the interval 0:1 in all "ndim" dimensions onto a cube of the desire size in parameter space. To my understanding, along each dimension, for example the 0-th, cube[0] goes from 0 to 1, so we have to redefine cube[0] to go from Xmin to Xmax (being X the quantity in dimension 0-th).'''
	cube[0] = f_min + cube[0] * (f_max - f_min) #Uniform between f_min and f_max.
	cube[1] = amp_min + cube[1] * (amp_max - amp_min) #Uniform between amp_min and amp_max.
	cube[2] = tau_min + cube[2] * (tau_max - tau_min) #Uniform between tau_min and tau_max.
	return cube

def loglike(cube, ndim, nparams):
	'''Log likelihood function.'''
	freq = cube[0]
	amp = cube[1]
	tau = cube[2]
	hf_template, _ = waveform(freq, tau)
	hf_template = amp * hf_template
	lnL =  -0.5 * np.sum(abs(hf - hf_template)**2. / PSD_interp)
	return lnL

#####################################

#Run multinest.

progress = pymultinest.ProgressPlotter(n_params = n_params, outputfiles_basename = chain_base)
progress.start()
pymultinest.run(loglike, prior, n_params, importance_nested_sampling = False, resume = True, verbose = True, sampling_efficiency = 'model', n_live_points = 1000, outputfiles_basename = chain_base)
progress.stop()

a = pymultinest.Analyzer(n_params = n_params, outputfiles_basename = chain_base)
s = a.get_stats()

#Store parameters and derived statistics, and print result.
with open('%sparams.json' % a.outputfiles_basename, 'w') as f:
	json.dump(parameters, f, indent=2)
	with open('%sstats.json' % a.outputfiles_basename, mode='w') as f:
		json.dump(s, f, indent=2)
		print
		print ("-" * 30, 'ANALYSIS', "-" * 30)
		print ("Global Evidence:\n\t%.15e +- %.15e" % ( s['nested sampling global log-evidence'], s['nested sampling global log-evidence error'] ))

#####################################

#Plot resulting posteriors.

if plotall:
        post=np.loadtxt(chain_base+'post_equal_weights.dat')
        for parami in xrange(len(parameters)):
                histi, binsi = np.histogram(post[:,parami], bins = bins)
                fig = py.figure()
                ax = py.gca()
                ax.plot(binsi[:-1], histi)
                ax.vlines(true_params[parami], ax.get_ylim()[0], ax.get_ylim()[1])
                ax.set_title('Parameter: %s' %parameters[parami])
                fig.savefig(oplotdir+'post_%s.png' %parameters[parami])
                #py.show(block = False)
                #raw_input('enter')

#####################################

