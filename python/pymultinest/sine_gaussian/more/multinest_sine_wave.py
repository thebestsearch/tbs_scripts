

## make sure I can do Bayesian stuff
# fit a damped sinusoid!


from __future__ import print_function, absolute_import

import numpy as np
#EHT: 
#import matplotlib.pyplot as plt

#EHT: this yielded libptf77blas.so.3 error.
#EHT: fix = pip install scipy --user --upgrade
from scipy.interpolate import interp1d

import pymultinest
import json

#EHT: must run in the same directory as tools_pdl/
#EHT: THIS GIVES AN ERROR
import tools_pdl.tools as tools


## create time 
time = np.linspace(0, 0.5, 10000)
Fs = 1./(time[1] - time[0])

def waveform(freq, phase, tau):
    ht = np.exp(-time / tau) * np.sin(2. * np.pi * freq * time + phase)
    hf, ff = tools.fft(ht, Fs)

    return hf, ff


freq_real = 200.
amp_real = 1e-21
phase_real = np.pi
tau_real = 0.01


hf, ff = waveform(freq_real, phase_real, tau_real)
hf = amp_real * hf



#EHT: ASD = np.loadtxt('../spectroscopy/ZERO_DET_high_P.txt')
#EHT: PSD = ASD
PSD = np.loadtxt('./ZERO_DET_high_P_psd.txt')

# interpolate PSD to data waveform
PSD_interp_func = interp1d(PSD[:, 0], PSD[:, 1], bounds_error=False, fill_value=np.inf)
PSD_interp = PSD_interp_func(ff)


if False:
    plt.clf()
    plt.loglog(ff, abs(hf))
    plt.loglog(ff, np.sqrt(PSD_interp))


f_min = 100.; f_max = 500.;
phase_min = 0.; phase_max = 2.*np.pi
amp_min = 1e-25; amp_max = 1e-20
tau_min = 0.001; tau_max = 0.1

def prior(cube, ndim, nparams):
    ## the prior maps from the interval 0:1 to the relevant parameter space
    cube[0] = f_min + cube[0] * (f_max - f_min)              # uniform between f_min and f_max
    cube[1] = phase_min + cube[1] * phase_max                # uniform between phase_min and phase_max
    cube[2] = amp_min + cube[2] * amp_max
    cube[3] = tau_min + cube[3] * tau_max                    # damping time
    return cube


def loglike(cube, ndim, nparams):
    freq = cube[0]
    phase = cube[1]
    amp = cube[2]
    tau = cube[3]
    hf_template, _ = waveform(freq, phase, tau)
    hf_template = amp * hf_template
    
    lnL =  -0.5 * np.sum(abs(hf - hf_template)**2. / PSD_interp)

    return lnL


parameters = ["freq", "phase", "amplitude", "tau"]
n_params = len(parameters)

chain_base = 'chains/freq_phase_'


progress = pymultinest.ProgressPlotter(n_params = n_params,
                                       outputfiles_basename = chain_base)

progress.start()
pymultinest.run(loglike, prior, n_params,
                importance_nested_sampling = False, resume = True,
                verbose = True, sampling_efficiency = 'model',
                n_live_points = 1000,
                outputfiles_basename = chain_base)

progress.stop()



a = pymultinest.Analyzer(n_params = n_params, outputfiles_basename = chain_base)
s = a.get_stats()

with open('%sparams.json' % a.outputfiles_basename, 'w') as f:
    json.dump(parameters, f, indent=2)
    # store derived stats
    with open('%sstats.json' % a.outputfiles_basename, mode='w') as f:
        json.dump(s, f, indent=2)
        print()
        print("-" * 30, 'ANALYSIS', "-" * 30)
        print("Global Evidence:\n\t%.15e +- %.15e" % ( s['nested sampling global log-evidence'], s['nested sampling global log-evidence error'] ))
        

                                                

