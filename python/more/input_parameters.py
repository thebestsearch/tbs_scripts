###############

#Define input parameters.

ligo_psd_file='../../tbs_data/input/ZERO_DET_high_P_psd.txt' #File with LIGO power spectral density (PSD).
fs = 2048 #Sampling rate, in samples/s.
dt = 1./fs #Time resolution, in s.
dur = 10 #Duration of the observation, in s.

#Other parameters for demo_v2.
Ns = 10 #Number of injected sources.
fmin = 10 #Minimum frequency of the injected signals.
fmax = fs/2. #Maximum frequency of the injected signals.
phi = 0. #Fixed value of phase for all injected signals.
