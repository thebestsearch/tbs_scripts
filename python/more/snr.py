#!/bin/python -u
import numpy as np
from scipy.interpolate import interp1d
import modules.common as cm
from modules.common import hub0, h0, grav, light, msun, mpc
#import inputs_fake_signal as ip
import modules.signal_model as sm
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as py
import modules.detectors as det
from modules.gaussian_noise import gaussian_noise
import os, sys, importlib

###############################

#Description.

#Calculates the theoretical S/N of the cross-correlation of two detectors (with the same PSD) for a given signal (made of a list of black holes).
#It also calculates the numerical S/N from the data.

###############################

#Input parameters.
inputfile=sys.argv[1] #Name of input file (in input_parameters). For example 'test2'.
noise_real=1000 #Number of realisations of the noise (for the calculation of the SNR normalisation).

#Derived inputs:
ip=importlib.import_module('input_parameters.%s' %inputfile)
sdir='../../tbs_data/fake_signal/bhb/%s/' %(inputfile) #Directory of realisations of the sources.
ddir='../../tbs_data/fake_signal/simdata/%s/' %(inputfile) #Directory of realisations of fake data.
oplotdir='/home/pablo.rosado/public_html/%s/' %(inputfile) #Directory of output plots.
nr=ip.nr #Number of realisations to consider (it can't be larger than the number of existing realisations).
dur=ip.dur #Duration of the observation of each data stream (in s).
srate=ip.srate #Sampling rate.

###############################

#Main.

#Check if output directory exists.
if not os.path.isdir(oplotdir): os.makedirs(oplotdir)

#Define some quantities.
f_FFT=np.fft.fftfreq(int(dur*srate),1./ip.srate) #Array of frequencies for the FFTs.
f_full=np.logspace(np.log10(det.ligoHL.fmin), np.log10(det.ligoHL.fmax), 100) #Array of frequencies for the integral in the theoretical calculation.
noise_snr=np.zeros(noise_real) #Array of SNR in the presence of noise only.
signal_snr=np.zeros(ip.nr) #Calculated SNR in the presence of signals.
theor_snr=np.zeros(ip.nr) #Theoretical SNR.

#Obtain the normalisation of the S/N from pure noise.
print 'Calculating S/N of %i noise realisations...' %int(noise_real)
print
norm=1.
for i in xrange(noise_real):
        n1=gaussian_noise(det.psd_d, ip.srate, dur, T=1, N=None)[0] #Noise for detector 1 (H1).
        n2=gaussian_noise(det.psd_d, ip.srate, dur, T=1, N=None)[0] #Noise for detector 2 (L1).
        Sh=sm.Sh_fun(n1,n2)
        noise_snr[i]=sm.snr_calc(det.ligoHL, f_FFT, Sh, norm)

#Normalise the S/N so that its std is 1.
print 'Calculating S/N of %i signal realisations...' %int(ip.nr)
print
norm=1./np.std(noise_snr)
noise_snr*=norm
for ri in xrange(ip.nr):
	#print 'Realisation %.3i' %int(ri) 
	b=cm.AttrDict(np.load(sdir+'run%.3i.npy' %int(ri))[()]) #Properties of binaries.
	d=cm.AttrDict(np.load(ddir+'run%.3i.npy' %int(ri))[()]) #Simulated data.

	s1=d.H1_data #Simulated data for H1.
	s2=d.L1_data #Simulated data for L1.
	#s1=d.H1_signal #Simulated data for H1.
	#s2=d.L1_signal #Simulated data for L1.
	Sh=sm.Sh_fun(s1,s2)
	signal_snr[ri]=sm.snr_calc(det.ligoHL,f_FFT,Sh,norm)
	#print 'Calculated SNR: ', signal_snr[ri]

	#om=sm.omega_binaries(f_full,dur,b)
	#py.loglog(f_full,om)
	#py.ylabel('Omega(f)')
	#py.xlabel('f/Hz')
	#py.savefig(oplotdir+'omega.png')

	theor_snr[ri]=sm.snr_theo(dur,b,det.ligoHL)
	#print 'Theoretical SNR: ',theor_snr[ri]
	#print

print 'Summary:'
print
print 'Noise S/N'
print 'mean: ',np.mean(noise_snr)
print 'std: ',np.std(noise_snr)
print
print 'Signal S/N'
print 'mean: ',np.mean(signal_snr)
print 'std: ',np.std(signal_snr)
print
print 'Theoretical S/N'
print 'mean: ',np.mean(theor_snr)
print 'std: ',np.std(theor_snr)
print

histi_noise,binsi_noise=np.histogram(noise_snr, normed=True, bins=100)
histi_signal,binsi_signal=np.histogram(signal_snr, normed=True, bins=100)

py.bar(binsi_noise[:-1],histi_noise,width=(binsi_noise[1]-binsi_noise[0]),alpha=0.5, color='blue', label='1000 noise')
py.bar(binsi_signal[:-1],histi_signal,width=(binsi_signal[1]-binsi_signal[0]),alpha=0.5, color='red', label='100 signal, 100 noise')
py.legend(loc='upper left')
py.savefig(oplotdir+'snr_hist')

#Pick realisation whose S/N is closest to the mean.
ind=abs(signal_snr-np.mean(signal_snr)).argmin()
print 'The "most average" realisation is run%.3i.' %int(ind)
print
print 'Calculate S/N of that same signal for %i different realisations of the noise.' %noise_real
print
snr_samesignal=np.zeros(noise_real)
for i in xrange(noise_real):
        d=cm.AttrDict(np.load(ddir+'run%.3i.npy' %int(ind))[()]) #Simulated data.
        s1=d.H1_signal #Simulated signal for H1.
        s2=d.L1_signal #Simulated signal for L1.
        n1=gaussian_noise(det.psd_d, ip.srate, dur, T=1, N=None)[0] #Noise for detector 1 (H1).
        n2=gaussian_noise(det.psd_d, ip.srate, dur, T=1, N=None)[0] #Noise for detector 2 (L1).

	s1+=n1
	s2+=n2

        Sh=sm.Sh_fun(s1,s2)
        snr_samesignal[i]=sm.snr_calc(det.ligoHL,f_FFT,Sh,norm)
        #print 'Calculated SNR: ', snr_samesignal[i]	

print 'Signal S/N'
print 'mean: ',np.mean(snr_samesignal)
print 'std: ',np.std(snr_samesignal)

histi_signal,binsi_signal=np.histogram(snr_samesignal, normed=True, bins=100)

py.bar(binsi_signal[:-1],histi_signal,width=(binsi_signal[1]-binsi_signal[0]),alpha=0.5, color='green', label='1 signal, 1000 noise')
py.legend(loc='upper right')
py.savefig(oplotdir+'snr_samesignal_hist')

