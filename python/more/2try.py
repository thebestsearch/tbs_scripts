import numpy as np
#from modules.generateIMRPhenomPv2 import lalsim_IMRPhenomPv2
from modules.generateIMRPhenomPv2 import FD
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

tc=9.
srate=1024.
dur=10. #Duration of the time series.
s_dur=100. #Duration of the binary simulation (should be at least as long as "dur" plus a typical duration of a signal). I do this so that the chirp does not 'wrap around'.
deltaF=1./s_dur
deltaT=1./srate
fmin=10.
#fmax=200.
fmax=1./(2.*deltaT)
t=np.arange(0.,s_dur, 1./srate)

m1=30.
m2=30.
dl=400.
z=0.1

#def FD(fmin, fmax, deltaF, dist, m1, m2, S1, S2, fRef, iota, RA, DEC, psi, phi, tc, ifo):
signal1=FD(fmin, fmax, deltaF, dl, m1, m2, [0,0,0], [0,0,0], 20, 0.1, 1.1, 1.0, 0.1, 0.2, tc, 'H1')
signal2=FD(fmin, fmax, deltaF, dl, m1, m2, [0,0,0], [0,0,0], 20, 0.1, 1.1, 1.0, 0.1, 0.2, tc, 'L1')

ht1=np.fft.irfft(signal1,len(t))*srate
ht2=np.fft.irfft(signal2,len(t))*srate

tsel=(t<=dur) #I select only the first "dur" seconds of data.
plt.plot(t[tsel],ht1[tsel],color='blue')
plt.plot(t[tsel],ht2[tsel],color='red')
plt.xlim(4,10)
plt.savefig("/home/pablo.rosado/public_html/ht.png")

#hf1=np.fft.rfft(ht1)

plt.clf()
plt.plot(np.real(signal1),color='green')
#plt.plot(np.real(hf1),color='blue')
#plt.plot(np.imag(signal1),color='black')
#plt.plot(np.real(signal2),color='red')
plt.xlabel('Frequency bin')
plt.ylabel('Frequency domain strain')
plt.xlim(0,5000)
plt.savefig('/home/pablo.rosado/public_html/hf.png')
