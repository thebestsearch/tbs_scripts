#!/bin/python
import numpy as np
from scipy.interpolate import interp1d
import modules.common as cm
from modules.common import hub0, h0, grav, light, msun, mpc
#import inputs_fake_signal as ip
import modules.signal_model as sm
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as py
import os, sys, importlib

###############################

#Description.

#Calculate the theoretical (average) S/N of the cross-correlation of two detectors (with the same PSD) for a given signal (made of a list of black holes).

###############################

#Input parameters.
inputfile=sys.argv[1] #Name of input file (in input_parameters). For example 'test2'.

#Derived inputs:
ip=importlib.import_module('input_parameters.%s' %inputfile)
ddir='../../tbs_data/fake_signal/simdata/%s/' %(inputfile) #Directory of realisations of fake data.
oplotdir='/home/pablo.rosado/public_html/%s/plot_sim/' %(inputfile) #Output directory for plots.
nr=ip.nr #Number of realisations to consider (it can't be larger than the number of existing realisations).

###############################

#Get number of files.
listfiles=np.array([file for file in os.listdir(ddir) if file[-4:]=='.npy'])

#Check if output directory exists.
if not os.path.isdir(oplotdir): os.makedirs(oplotdir)

#Plot.

for ri in xrange(len(listfiles)):
	print 'Plotting run%.3i' %int(ri)
	d=cm.AttrDict(np.load(ddir+'run%.3i.npy' %int(ri))[()])
	py.figure(1)
	py.clf()
	py.plot(d.t,d.H1_data,color='red', label='H1 data')
	py.plot(d.t,d.L1_data,color='blue', label='L1 data')
	py.plot(d.t, d.H1_signal,color='green')
	py.plot(d.t, d.L1_signal,color='yellow')
	py.ylim(-4e-20,4e-20)
	py.xlabel('t/s')
	py.ylabel('strain of signal plus noise')
	py.legend(loc='upper left')
	#py.title('Cross-correlation S/N for this realisation: %.3e' %snr)
	py.savefig(oplotdir+'data_run%.3i' %int(ri))

	py.figure(2)
	py.clf()
	py.plot(d.t,d.H1_signal,color='red', label='H1 signal')
	py.plot(d.t,d.L1_signal,color='blue', label='L1 signal')
	py.ylim(-1e-21,1e-21)
	py.xlabel('t/s')
	py.ylabel('strain of signal only')
	py.legend(loc='upper left')
	#py.title('Cross-correlation S/N for this realisation: %.3e' %snr)
	py.savefig(oplotdir+'signal_run%.3i' %int(ri))

