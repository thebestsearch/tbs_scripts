#!/usr/bin/env python -u
import numpy as np
import pylab as py
import modules.signal as si
from modules.time_estimate import time_estimate
import os, sys

####################

#Description.

#Calculate the optimal S/N of each binary in a mbhb realisation. Plot the DP of this, assuming a Gaussian distribution with std=1 and mu=0 for H0, and a Gaussian with std=1 and mu=\sqrt{\sum{S/N^2}}.
#Calculate the A- and B- statistic DP, and the (weak signal) Allen&Romano1999 DP.

#All binaries are assumed monochromatic.
#All pulsars are identical (same cadence, white noise, and time span).

####################

#Inputs.

mbhb_file=sys.argv[1]
fap=0.001 #False alarm probability.
mf_mu0=0. #
mf_sigma0=1. #
mf_sigma1=1. #
#mbhb_file='/Users/prosado/work/projects/proj22_expected_properties_updated/data/input/mbhb/npy/gw_circ/MC_RES_BH163_MF107_PF111_TM102_REAL1008.out.npy'
#mbhb_file='/Users/prosado/work/projects/proj22_expected_properties_updated/data/input/mbhb/npy/gw_circ/MC_RES_BH162_MF103_PF112_TM101_REAL1001.out.npy'
npul=10 #Number of pulsars (all identical).
rms=100.*si.nanosec #White noise rms (in s).
cad=14.*24.*3600. #Cadence (in s).
tobs_min=1. #Minimum observing time (in yr).
tobs_max=20. #Maximum observing time (in yr).
tobs_nbins=100. #Number of bins.

####################

#Define some functions (although most of the functions are defined in modules/signal.py).

def snr_cc(tobs, strain, Sn, f):
	'''S/N of the cross-correlation for one binary, according to Eric.'''
	return 2.*strain**2.*np.sqrt(tobs)/(f**(1./2.)*Sn)

def snr_mf(tobs, strain, Sn):
        '''Optimal S/N of a matched filter search on a monochromatic binary.'''
        return 2.*np.sqrt(tobs*1./Sn)*strain

####################

#Main.

#Create array of observing time.
tobs_a=np.linspace(tobs_min*si.yr, tobs_max*si.yr, tobs_nbins)

#Load signal.
md=np.load(mbhb_file)[()] #Keys: ['reds', 'sky_phi_rad', 'incli', 'polariz', 'log10mch', 'log10fobs', 'init_phase', 'cmdist_mpc', 'sky_theta_rad']

#Create pulsar array.
f=10.**(md['log10fobs']) #Observed GW frequency of each binary.
h=si.strain(10.**(md['log10mch']), md['reds'], md['cmdist_mpc']*(1.+md['reds']), 10.**(md['log10fobs'])) #Strain amplitude of each binary.
S_n=si.Sn_white(rms, cad, f) #Noise of each psr at the observed frequency of each binary.

py.ion()

'''
#Scaling with time (should be the same).
snr_mf_a=np.zeros(len(tobs_a))
snr_cc_a=np.zeros(len(tobs_a))

for ti in xrange(len(tobs_a)):
	tobs=tobs_a[ti]
	snr_mf_i=snr_mf(tobs, h, S_n)
	comb_snr_mf=np.sqrt(np.sum(snr_mf_i**2.))
	snr_mf_a[ti]=comb_snr_mf

	snr_cc_i=snr_cc(tobs, h, S_n, f)
	comb_snr_cc=np.sqrt(np.sum(snr_cc_i**2.))
	snr_cc_a[ti]=comb_snr_cc

fig=py.figure(1)
ax=fig.gca()
#ax.plot(tobs_a, dp_a)
ax.loglog(tobs_a, snr_mf_a, label='MF')
ax.loglog(tobs_a, snr_cc_a, label='CC')
ax.legend(loc='lower right')
'''

#Scaling with number of binaries.
nbin=len(h)
snr_mf_a=np.zeros(nbin)
snr_cc_a=np.zeros(nbin)
tobs=15.*si.yr #Fix the observation time.

te=time_estimate(nbin)
for bi in xrange(1,nbin):
	te.display()
	te.increase()

	snr_mf_i=snr_mf(tobs, h[0:bi], S_n[0:bi])
	comb_snr_mf=np.sqrt(np.sum(snr_mf_i**2.))
	snr_mf_a[bi]=comb_snr_mf

	snr_cc_i=snr_cc(tobs, h[0:bi], S_n[0:bi], f[0:bi])
	comb_snr_cc=np.sqrt(np.sum(snr_cc_i**2.))
	snr_cc_a[bi]=comb_snr_cc
print
fig=py.figure(2)
ax=fig.gca()
ax.loglog(np.arange(nbin), snr_mf_a, label='MF')
ax.loglog(np.arange(nbin), snr_cc_a, label='CC')
ax.legend(loc='lower right')
raw_input('enter')



