import numpy as np
import pylab as py
import modules.signal as si

###################

#Description:

#I will take one realisation of the ensemble of BH binaries (circular, gw-driven binaries), calculate the cross-correlation S/N (and DP), and calculate the individual S/N of each binary (assuming that you can resolve each of them).
#The latter case would be the best case detection scenario, in which you can resolve every binary; it would be interesting to see how much better this S/N (or DP) is, with respect to an optimal cross-correlation search.

###################

#Inputs.

#File with data of the ensemble of MBHBs.
mbhb_file = '../../tbs_data/input/MBHB/run000000.npy'
#Properties of the assumed PTA.
cad = 2.*si.week #Cadence.
rms = 500.*si.nanosec #White rms noise.
numpul = 20 #Number of pulsars (assumed all identical).
fap=0.001 #False alarm probability.
tobs_min=1 #Minimum observing time.
tobs_max=100 #Maximum observing time.
tobs_bins=50 #Number of points in array of observing time.
tobsi=50. #Observing time at which power laws will be fitted.

###################

#Define some other quantities.

#Create signal data.
sd = si.get_signal_data(np.load(mbhb_file)[()])

###########I will consider only binaries with frequency above 1/20yr!!!
#sdall=si.get_signal_data(np.load(mbhb_file)[()])
#fsel=1./(15.*si.yr)
#selecti=(sdall['f']>fsel)
#dicti={'z':sdall.z[selecti], 'f':sdall.f[selecti], 'dl':sdall.dl[selecti], 'mch':sdall.mch[selecti]}
#sd=si.AttrDict(dicti)
##############

#Create pulsar array data (equal white noise, random sky positions).
pd=si.make_pulsar_data(numpul, rms, cad)

#Create vector of observation time (in years).
tobsvec=np.linspace(tobs_min, tobs_max, tobs_bins)

###################

#Calculate best SNR and cross-correlation SNRB.

SNR_best=np.zeros(len(tobsvec))
SNR_A=np.zeros(len(tobsvec))
SNR_B=np.zeros(len(tobsvec))

mu1_A=np.zeros(len(tobsvec))
sigma0_A=np.zeros(len(tobsvec))
sigma1_A=np.zeros(len(tobsvec))
DP_A=np.zeros(len(tobsvec))
mu1_B=np.zeros(len(tobsvec))
sigma0_B=np.zeros(len(tobsvec))
sigma1_B=np.zeros(len(tobsvec))
DP_B=np.zeros(len(tobsvec))
for ti in xrange(len(tobsvec)):
	print 'Observing time %.2f years.' %tobsvec[ti]
	tobs=tobsvec[ti]*si.yr

	SNR_best[ti]=si.SNR_best(sd, pd, tobs)

	SNR_A[ti]=si.SNR_A(sd, pd, tobs)

	SNR_B[ti]=si.SNR_B(sd, pd, tobs)

	sigma0_A[ti], sigma1_A[ti], mu1_A[ti], sigma0_B[ti], sigma1_B[ti], mu1_B[ti]=si.STAT(sd, pd, tobs)

	DP_A[ti]=si.DP(fap, sigma0_A[ti], sigma1_A[ti], mu1_A[ti])
	DP_B[ti]=si.DP(fap, sigma0_B[ti], sigma1_B[ti], mu1_B[ti])

tobsi_ind=abs(tobsvec-tobsi).argmin() #Index where tobsvec==tobsi.
pl_1=si.power_law(1./2., tobsvec, tobsvec[tobsi_ind], SNR_A[tobsi_ind])
pl_2=si.power_law(1./2., tobsvec, tobsvec[tobsi_ind], SNR_B[tobsi_ind])
pl_3=si.power_law(1./2., tobsvec, tobsvec[tobsi_ind], SNR_best[tobsi_ind])

py.ion()
py.figure(1)
#py.plot(tobsvec, DP_A)
py.semilogy(tobsvec, pl_1, color='black')
py.semilogy(tobsvec, pl_2, color='black')
py.semilogy(tobsvec, pl_3, color='black')
py.semilogy(tobsvec, SNR_best, color='red', label='Matched Filter SNR')
py.semilogy(tobsvec, SNR_A, color='blue', label='Cross-correlation A SNR')
py.semilogy(tobsvec, SNR_B, color='green', label='Cross-correlation B SNR')
py.xlabel('Observing time / yr')
py.ylabel('S/N')
py.title('%s pulsars, %.2fns rms, %.2f week cadence' %(numpul, rms*1./si.nanosec, cad*1./si.week))
py.legend(loc='lower right')

print 'CALCULATE DP FOR MF ASSUMING DIFFERENT NUMBER OF TEMPLATES.'
raw_input('enter')
py.figure(2)
py.plot(tobsvec, DP_A, label='DP A')
py.plot(tobsvec, DP_B, label='DP B')
py.xlabel('Observing time / yr')
py.ylabel('Detection Probability')
py.title('%s pulsars, %.2fns rms, %.2f week cadence' %(numpul, rms*1./si.nanosec, cad*1./si.week))
py.legend(loc='lower right')
raw_input('enter')

