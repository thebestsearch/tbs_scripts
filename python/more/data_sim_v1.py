import numpy as np
import inputs_fake_signal as ip
import modules.common as cm
from modules.gaussian_noise import gaussian_noise
import modules.signal_model as sm
from modules.time_estimate import time_estimate

########################

#Description.

#Import a realisation of the ensemble of bhbs, and produce some fake data streams.

########################

#Input parameters.
sdir='../../tbs_data/fake_signal/bhb/' #Directory of realisations of the sources.
odir='../../tbs_data/fake_signal/simdata/' #Directory of simulated data.
ligo_psd_file='../../tbs_data/input/ZERO_DET_high_P_psd.txt' #File with LIGO power spectral density (PSD).
nr=ip.nr #Number of realisations to consider (it can't be larger than the number of existing realisations).
n_p_r=1 #Number of data streams to produce per realisation of the ensemble of bhbs (not yet implemented!+++).
dur=ip.dur #Duration of the observation of each data stream (in s).
srate=ip.srate #Sampling rate (in s).

########################

#Main.

Sh=np.loadtxt(ligo_psd_file) #LIGO PSD.
tbin=1./srate #Time resolution (in s).
t=np.arange(0., dur, tbin)

te=time_estimate(nr-1)

for ri in xrange(nr):
	te.display() #Shows the remaining computation time.
        te.increase() #Needed to calculate the remaining computation time.

	ofile='run%.3i' %(int(ri))

	#Generate noise.
	noise=gaussian_noise(Sh, srate, dur, T=2, N=None)
	n1=noise[0]
	n2=noise[1]
	#n1=gaussian_noise(Sh, srate, dur, T=1, N=None)[0] #Noise for detector 1.
	#n2=gaussian_noise(Sh, srate, dur, T=1, N=None)[0] #Noise for detector 2.

	b=np.load(sdir+'run%.3i.npy' %int(ri))[()] #Load dictionary of properties of binaries in one realisation.
	allb,tmat=sm.combined_signal(b,t) #Create another dictionary with those properties converted into a matrix with dimensions (number of binaries x number of time samples).

	#To avoid numerical issues, once a binary merges, make the coalescence time infinity.
	merged=(tmat>=allb.tc) #Mask that becomes True if a binary has merged.
	allb.tc+=1e90*merged

	#Matrix of strain (only plus polarisation).
	hmat=sm.hplus(allb,tmat)

	#For the moment I simply add the strain of all binaries (without caring for sky positions, inclinations, etc.).
	h=np.sum(hmat, axis=0) #The total strain is the sum of all binaries.
	#Also, I assume that the signal is identical in both detectors (as if they were colocated).
	d1=h+n1
	d2=h+n2

	#Save data.
	dicti={'d1':d1, 'd2':d2, 'h':h, 't':t}

	np.save(odir+ofile, dicti)

