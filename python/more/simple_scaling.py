#!/usr/bin/env python -u
import numpy as np
import pylab as py
from modules.time_estimate import time_estimate

#####################

#Description.

#Compare the scaling with time of a cross-correlation with a matched-filter, in a very simplistic case.
#H0 (null hypothesis) assumes that there are no gws in the signal.
#H1 (gw hypothesis) assumes that there are gws in the signal. 

#We measure "nseg" segments of data; if the resolvability parameter is time, then "nseg" are different time samples (if the resolvability parameter were frequency, "nseg" would be different frequency bins).

#The likelihood function (LF) of the signal under H0 is LF(H0)=P(signal|H0).
#The probability of a segment "i" (e.g. a time sample) under H0 is p(segment_i|H0). The array of all these will be called "p_H0". Assume the noise follows a normal distribution.
#Finally, LF(H0)=p(segment_1|H0) x p(segment_2|H0) x ...

#The LF of the signal under H1 is LF=P(signal|H1)=p(segment_1|H1) x p(segment_2|H1) x ...
#I consider two different models for the distribution of the signal under H1.
#Model A: Assume that the distribution of the signal under H1 is normal.
#Model B: Assume a bimodal distribution.

#Assuming all segments are uncorrelated, the combined probability of all segments under H0 (which is the likelihood function LF(H0)) will be the product of all the p(segment_i|H0); the same applies to LF(H1). 
#Instead of calculating the ratio of the combined probabilities LF(H1)/LF(H0), for convenience I calculate Lambda=2 log(LF(H1)/LF(H0)), which is the log-likelihood ratio.
#Doing so, the log of the products become the sum of the logs.

#####################

#Inputs.
nseg=10000 #Number of segments. There will be one signal in each segment, which implies xi=1.
nreal=500 #Number of realisations of the same experiment.
noise_mu=0. #Mean of the (normal) distribution of the noise.
noise_sigma=1 #Standard deviation of the noise.
h0=0.5 #Amplitude of the gw (each segment will have one identical, or with sign switched).
gw_sign='alt' #Sign of the gw; either 'fix' (i.e. positive), or 'alt' (alternating).
gw_mu=h0 #Mean of the observed signal in the presence of a gw.
gw_sigma=noise_sigma #Std of the observed signal in the presence of a gw.
xi=1. #NOT YET IMPLEMENTED, IRRELEVANT!

#Plotting inputs.
negative_Lambda=1e-25 #Minimum value of log-likelihood ratio (negative values will be replaced by this).
Lambda_plot_min, Lambda_plot_max = 1e-1, 1e4
xscale='log' #Scale of x-axis (either 'log' or 'linear').
yscale='log' #Scale of y-axis.
#####################

#Define some functions.

def normalPDF(mu, sigma, s):
	'''Probability density function (PDF) of a normal distribution of mean "mu", standard deviation "sigma", evaluated at "s".'''
	return 1./np.sqrt(2.*np.pi*sigma**2.)*np.exp( -(s-mu)**2./(2.*sigma**2.) )

def normal_signs(mu, sigma, s):
	'''PDF of a distribution made of the combination of two normal distributions with means "mu" and "-mu", std "sigma", evaluated at "s".'''
	return 0.5*normalPDF(mu, sigma, s)+0.5*normalPDF(-mu, sigma, s)

def Lambda(signal,model):
	'''Log-likelihood ratio for model "A" or "B".'''
        p_H0=normalPDF(noise_mu, noise_sigma, signal) #Array of all p(segment_i|H0).
	if model=='A':
        	p_H1=normalPDF(gw_mu, gw_sigma, signal) #Array of all p(segment_i|H1) under model A.
        elif model=='B':
		p_H1=normal_signs(gw_mu, gw_sigma, signal) #Array of all p(segment_i|H1) under model B.
        return 2.*(np.cumsum(np.log(p_H1))-np.cumsum(np.log(p_H0))) #Log-likelihood ratio.

#####################

#Main.

#Define matrix of log-likelihood ratio assuming models A and B, and in the absence and presence of gws.
Lambda0_A=np.zeros((nreal, nseg))
Lambda0_B=np.zeros((nreal, nseg))
Lambda1_A=np.zeros((nreal, nseg))
Lambda1_B=np.zeros((nreal, nseg))

te=time_estimate(nreal) #A class that prints estimated computation time.

#Do different realisations of the same experiment.
for ri in xrange(nreal): 
	te.display() #Shows the remaining computation time.
	te.increase() #Needed to calculate the remaining computation time.

	#Generate signals in these segments.
	noise=np.random.normal(noise_mu, noise_sigma, nseg) #Detector noise at each segment.

	#Simulate signal in the absence of gws.
	signal0=noise #There is only noise.

	#Simulate signal in the presence of gws.
	if gw_sign=='fix':
		signal1=noise+h0 #Signal made of random noise and one gw in each segment. All signals are identical.
	elif gw_sign=='alt':
		signal1=noise+h0*np.sign(np.random.randn(nseg)) #The sign of the gw signal can be either + or -.

	#Calculate Lambda (under model A and B) in the absence of gws.
	Lambda0_A[ri,:]=Lambda(signal0,'A')
	Lambda0_B[ri,:]=Lambda(signal0,'B')

	#Calculate Lambda (under model A and B) in the presence of gws.
	Lambda1_A[ri,:]=Lambda(signal1,'A')
	Lambda1_B[ri,:]=Lambda(signal1,'B')

Lambda0_A_m=np.mean(Lambda0_A, axis=0) #Mean Lambda_A over all realisations.
Lambda0_B_m=np.mean(Lambda0_B, axis=0) #Mean Lambda_B.
Lambda1_A_m=np.mean(Lambda1_A, axis=0) #Mean Lambda_A over all realisations.
Lambda1_B_m=np.mean(Lambda1_B, axis=0) #Mean Lambda_B.

#####################

#Plot results.

print 'Plotting...'
print

#To avoid numerical issues, make all negative values of Lambda positive (but small).
Lambda0_A_plot=Lambda0_A.copy()
Lambda0_A_plot[Lambda0_A_plot<0]=negative_Lambda
Lambda0_B_plot=Lambda0_B.copy()
Lambda0_B_plot[Lambda0_B_plot<0]=negative_Lambda
Lambda1_A_plot=Lambda1_A.copy()
Lambda1_A_plot[Lambda1_A_plot<0]=negative_Lambda
Lambda1_B_plot=Lambda1_B.copy()
Lambda1_B_plot[Lambda1_B_plot<0]=negative_Lambda

py.ion()

#Plots of signal, noise, and models
fig=py.figure(1)
ax=fig.add_subplot(211)
bins=np.linspace(-10.,10.,100)
bins_m=(bins[1:]+bins[:-1])*0.5 #Arithmetic mean of the bins.
noise_model=normalPDF(noise_mu, noise_sigma, bins_m)
signal_model_A=normalPDF(gw_mu, gw_sigma, bins_m)
signal_model_B=normal_signs(gw_mu, gw_sigma, bins_m)
signal_hist=np.histogram(signal1, bins=bins, normed=True)[0]
noise_hist=np.histogram(noise, bins=bins, normed=True)[0]
ax.plot(bins_m, noise_model, color='orange', label='Noise model')
ax.plot(bins_m, signal_model_A, color='blue', label='Signal model A')
ax.plot(bins_m, signal_model_B, color='green', label='Signal model B')
ax.plot(bins_m, noise_hist, color='red', label='Real noise')
ax.plot(bins_m, signal_hist, color='black', label='Real signal')
ax.set_title('%s reals, h0=%.2f, sign %s., xi=%.2f' %(nreal,h0,gw_sign, xi))
ax.legend(loc='upper left',prop={'size':8})

ax=fig.add_subplot(212)
for ri in xrange(nreal):
	#Plot all different realisations in the background.
	ax.plot(Lambda1_A_plot[ri,:], color='blue', linewidth=0.1, alpha=0.9)
	ax.plot(Lambda1_B_plot[ri,:], color='green', linewidth=0.1, alpha=0.9)
	ax.plot(Lambda0_A_plot[ri,:], color='red', linewidth=0.1, alpha=0.9)
	ax.plot(Lambda0_B_plot[ri,:], color='grey', linewidth=0.1, alpha=0.9)
ax.plot(Lambda1_A_m, color='darkblue', linewidth=3, label='Model A, gws')
ax.plot(Lambda1_B_m, color='darkgreen', linewidth=3, label='Model B, gws')
ax.plot(Lambda0_A_m, color='darkred', linewidth=3, label='Model A, noise')
ax.plot(Lambda0_B_m, color='darkgrey', linewidth=3, label='Model B, noise')
#ax.set_title('%s reals, h0=%.2f, sign %s., xi=%.2f' %(nreal,h0,gw_sign, xi))
ax.set_xlabel('Data segment')
ax.set_ylabel('Lambda')
ax.set_ylim(Lambda_plot_min, Lambda_plot_max)
ax.legend(loc='upper left')
ax.set_xscale(xscale)
ax.set_yscale(yscale)
raw_input('enter')

