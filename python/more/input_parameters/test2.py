###############

#Define input parameters.

ligo_psd_file='../../tbs_data/input/ZERO_DET_high_P_psd.txt' #File with LIGO power spectral density (PSD).
srate = 2048 #Sampling rate, in samples/s.
dur = 10. #Duration of the observation, in s.
dur_ext=50. #Duration of the time series of the simulated waveforms; it should be at least as long as "dur" plus the typical duration of a chirp, to avoid the 'wrap around'.
nb = 10 #Number of injected binaries.
nr = 100 #Number of realisations.
m1_all=30. #For the moment, all binaries will have the same mass and redshift.
m2_all=30.
z_all=0.4

#Inputs for the waveforms.
fmin=10. #Minimum frequency of the waveform (in Hz).
fmax=srate/2. #Maximum frequency of the waveform (in Hz).
fRef=20. #Reference frequency at which spins are calculated (in Hz).

#def lalsim_IMRPhenomPv2(fmin, fmax, deltaF, dist, m1, m2, S1, S2, fRef, iota, RA, DEC, psi, phi, tc, ifo):
#lalsim_IMRPhenomPv2(10, 1024, 1./8., 100, m1,m2, [0,0,0], [0,0,0], 20, 0.1, 1.1, 1.0, 0.1, 0.2, 969379706, 'H1')
