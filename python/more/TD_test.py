import numpy as np
import lalsimulation as lalsim
import lal
from modules.generateIMRPhenomPv2 import timedomain_IMRPhenomPv2 as TD
#In [2]: lalsim.SimInspiralChooseTDWaveform?
#Docstring: SimInspiralChooseTDWaveform(REAL8 phiRef, REAL8 deltaT, REAL8 m1, REAL8 m2, REAL8 s1x, REAL8 s1y, REAL8 s1z, REAL8 s2x, REAL8 s2y, REAL8 s2z, REAL8 f_min, REAL8 f_ref, REAL8 r, REAL8 i, REAL8 lambda1, REAL8 lambda2, SimInspiralWaveformFlags waveFlags, SimInspiralTestGRParam nonGRparams, int amplitudeO, int phaseO, Approximant approximant) -> int
#Type:      builtin_function_or_method


phi, S1, S2, fmin, fmax, fRef, iota, lambda1, lambda2, waveFlags, nonGRparams, amplitude0, phase0=0.2, [0,0,0], [0,0,0], 20., 1024., 20., 0.1, 0., 0., None, None, 0, 0
deltaT=1./(2.*fmax)
#m1=30.*lal.lal.MSUN_SI
#m2=m1
#dist=100.*1e6*lal.lal.PC_SI
'''
hplus,hcross = lalsim.SimInspiralChooseTDWaveform(phi, deltaT, m1, m2, S1[0], S1[1], S1[2], S2[0], S2[1], S2[2], fmin, fRef, dist, iota, 0.0, 0.0, None, None, 0, 0, lalsim.IMRPhenomPv2)
'''
m1=30.
m2=m1
dist=100.
ifo='H1'
RA,DEC=1.1,1.0
psi=0.1
tc=919
#print lalsim_IMRPhenomPv2(20, 1024, 1./8., 100, 30, 30, [0,0,0], [0,0,0], 20, 0.1, 1.1, 1.0, 0.1, 0.2, 969379706, 'H1')
signal1=TD(fmin, fmax, deltaT, dist, m1, m2, S1, S2, fRef, iota, RA, DEC, psi, phi, tc, ifo)
print len(signal1)
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

#def lalsim_IMRPhenomPv2(fmin, fmax, deltaF, dist, m1, m2, S1, S2, fRef, iota, RA, DEC, psi, phi, tc, ifo):
#print lalsim_IMRPhenomPv2(20, 1024, 1./8., 100, 30, 30, [0,0,0], [0,0,0], 20, 0.1, 1.1, 1.0, 0.1, 0.2, 969379706, 'H1')
plt.plot(signal1,color='blue')
#plt.plot(signal3,color='green')
#plt.xlim(0.,100.)
plt.savefig("/home/pablo.rosado/public_html/TDwaveform.png")
