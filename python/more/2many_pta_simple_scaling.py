#!/usr/bin/env python -u
import numpy as np
import pylab as py
import modules.signal as si
from modules.time_estimate import time_estimate
import os, sys

####################

#Description.

#Calculate the ratio of matched filter S/N over cross-correlation S/N (using some definitions suggested by Eric). Do this for an increasing number of binaries, and for many realisations.
#Depending on how to pick binaries, the result changes a lot. In principle, I pick them randomly, but one can sort them in a way that maximizes the ratio.

#All binaries are assumed monochromatic.
#All pulsars are identical (same cadence, white noise, and time span).

####################

#Inputs.
liveplot=True #If True, plot each realisation on the flight.
mfsort=True #If True, sort binaries to maximize the MF/CC ratio.
fap=0.001 #False alarm probability.
mf_mu0=0. #
mf_sigma0=1. #
mf_sigma1=1. #
#mbhb_file=sys.argv[1]
mbhb_dir='/Users/prosado/work/projects/proj22_expected_properties_updated/data/input/mbhb/npy/gw_circ/'
#npul=10 #Number of pulsars (all identical).
rms=100.*si.nanosec #White noise rms (in s).
cad=14.*24.*3600. #Cadence (in s).
tobs_min=1. #Minimum observing time (in yr).
tobs_max=20. #Maximum observing time (in yr).
tobs_nbins=100. #Number of bins.

####################

#Define some functions (although most of the functions are defined in modules/signal.py).

def snr_cc(tobs, strain, Sn, f):
	'''S/N of the cross-correlation for one binary, according to Eric.'''
	return 2.*strain**2.*np.sqrt(tobs)/(f**(1./2.)*Sn)

def snr_mf(tobs, strain, Sn):
        '''Optimal S/N of a matched filter search on a monochromatic binary.'''
        return 2.*np.sqrt(tobs*1./Sn)*strain

####################

#Main.

#Create array of observing time.
#tobs_a=np.linspace(tobs_min*si.yr, tobs_max*si.yr, tobs_nbins)
tobs=20.*si.yr #Fix the observation time.

realisations=np.array([file for file in os.listdir(mbhb_dir) if file[0]=='M']) #Array of file names of realisations.
np.random.shuffle(realisations) #Sort them randomly.

nreal=len(realisations) #Number of realisations.
nbin_a=np.logspace(0,5,100) #Array of number of sources.
mf_a=np.zeros(len(nbin_a)) #Array with the mean of the MF S/N.
cc_a=np.zeros(len(nbin_a)) #Array with the mean of the CC S/N.
ratio_a=np.zeros(len(nbin_a))

py.ion()
fig=py.figure(1)
ax=fig.gca()
ax.loglog([], [], color='red', linewidth=3, label='S/N ratio (MF/CC)')
ax.loglog([], [], color='green', linewidth=3, label='Average ratio')
ax.set_xlabel('Number of binaries')
ax.set_ylabel('S/N ratio')
ax.legend(loc='upper right')
te=time_estimate(nreal)
for ri in xrange(nreal):
	te.display()
	te.increase()

	mbhb_file=realisations[ri]

	#Load signal.
	md=np.load(mbhb_dir+mbhb_file)[()] #Keys: ['reds', 'sky_phi_rad', 'incli', 'polariz', 'log10mch', 'log10fobs', 'init_phase', 'cmdist_mpc', 'sky_theta_rad']
	s=np.arange(len(md['reds']))
	np.random.shuffle(s)
	f=10.**(md['log10fobs'][s]) #Observed GW frequency of each binary.
	h=si.strain(10.**(md['log10mch'][s]), md['reds'][s], md['cmdist_mpc'][s]*(1.+md['reds'][s]), 10.**(md['log10fobs'][s])) #Strain amplitude of each binary.
	S_n=si.Sn_white(rms, cad, f) #Noise of each psr at the observed frequency of each binary.

	if mfsort:
		#Instead of sorting them randomly, now I sort them to have first the ones that maximize the ratio.
		#snrratio=np.sqrt(f*S_n)/h
		#sorti=snrratio.argsort()[::-1]
		sorti=h.argsort()[::-1]
		S_n=S_n[sorti]	
		f=f[sorti]
		h=h[sorti]

	#Scaling with number of binaries.
	nbin=len(h)
	#snr_mf_a=np.zeros(nbin)
	#snr_cc_a=np.zeros(nbin)

	snr_mf_a=snr_mf(tobs, h, S_n)
	comb_mf_a=np.sqrt(np.cumsum(snr_mf_a**2.))

	snr_cc_a=snr_cc(tobs, h, S_n, f)
	comb_cc_a=np.sqrt(np.cumsum(snr_cc_a**2.))

	#mf_a[ri,:]=np.interp(nbin_a,np.arange(nbin),snr_mf_a)	
	#cc_a[ri,:]=np.interp(nbin_a,np.arange(nbin),snr_cc_a)	
	#mf_a+=np.interp(nbin_a,np.arange(nbin),comb_mf_a)	
	#cc_a+=np.interp(nbin_a,np.arange(nbin),comb_cc_a)
	mf_a=np.interp(nbin_a,np.arange(nbin),comb_mf_a)	
	cc_a=np.interp(nbin_a,np.arange(nbin),comb_cc_a)
	ratio_i=mf_a*1./cc_a
	ratio_a+=ratio_i

	if liveplot:	
		#ax.loglog(np.arange(nbin), comb_mf_a, color='blue', linewidth=0.2)
		#ax.loglog(np.arange(nbin), comb_cc_a, color='green', linewidth=0.2)
		ax.loglog(np.arange(nbin), comb_mf_a*1./comb_cc_a, color='red', linewidth=0.2)

		if ri>0:
			#mfm.remove()
			#ccm.remove()
			rtm.remove()

		#mfm=ax.loglog(nbin_a, mf_a*1./(ri+1), color='blue', linewidth=3).pop(0)
		#ccm=ax.loglog(nbin_a, cc_a*1./(ri+1), color='green', linewidth=3).pop(0)
		rtm=ax.loglog(nbin_a, ratio_a*1./(ri+1), color='green', linewidth=3).pop(0)	

		ax.set_title('Realisation %i / %i' %(ri+1, nreal))
		py.pause(0.000001)
	#raw_input('enter')
print
if not liveplot:
	#ax.loglog(nbin_a, mf_a*1./nreal, color='blue', linewidth=3)
	#ax.loglog(nbin_a, cc_a*1./nreal, color='green', linewidth=3)
	ax.loglog(nbin_a, ratio_a*1./nreal, color='green', linewidth=3)
	ax.set_title('%i realisations' %(nreal))

raw_input('enter')



