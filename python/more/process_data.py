#!/bin/python
import numpy as np
from scipy import signal
from scipy.interpolate import interp1d
from scipy.signal import butter, filtfilt, iirdesign, zpk2tf, freqz
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import inputs_fake_signal as ip
import os, sys, importlib

###################################

#Description.

#Follow instructions in tutorial about GW150914 to recover signal from (in our case, simulated) data.
#https://losc.ligo.org/s/events/GW150914/GW150914_tutorial.html

###################################

#Input parameters.
inputfile=sys.argv[1] #Name of input file (in input_parameters). For example 'test2'.
sim='run073' #Name of simulated file to analyse.

#Derived inputs:
ip=importlib.import_module('input_parameters.%s' %inputfile)
sdir='../../tbs_data/fake_signal/bhb/%s/' %(inputfile) #Directory of realisations of the sources.
ddir='../../tbs_data/fake_signal/simdata/%s/' %(inputfile) #Directory of simulated data.
oplotdir='/home/pablo.rosado/public_html/%s/plot_sim/' %(inputfile) #Directory of output plots.

###################################

#Define some quantities and functions.

fs=ip.srate #Sampling rate.
NFFT=1*fs
fmin=ip.fmin #Minimum frequency.
fmax=ip.fmax #Maximum frequency.

def whiten(strain, interp_psd, dt):
	'''Whiten time series "strain", given a PSD "interp_psd", and a time resolution "dt".'''
	Nt=len(strain)
	freqs=rfftfreq(Nt,dt)
	hf=np.fft.rfft(strain)
	white_hf=hf/(np.sqrt(interp_psd(freqs)/dt/2.))
	white_ht=np.fft.irfft(white_hf,n=Nt)
	return white_ht

def rfftfreq(n,d=1.0):
	'''This function should be identical to np.fft.rfftfreq, but since it does not exist in the current version of numpy in LDAS, I define it myself.'''
	val = 1.0/(n*d)
	N = n//2 + 1
	results = np.arange(0, N, dtype=int)
	return results * val

###################################

#Main.

#Check if output directory exists.
if not os.path.isdir(oplotdir): os.makedirs(oplotdir)

#Load simulated data.
d=np.load(ddir+sim+'.npy')[()] #Simulated data.
time=d['t'] #Time array (in s).
dt=time[1]-time[0] #Time resolution (in s).
strain_H1=d['H1_data'] #Strain data for H1 (signal+noise).
strain_L1=d['L1_data'] #Same for L1.

#Load properties of simulated binaries.
b=np.load(sdir+sim+'.npy')[()]
tmid=0.5*(max(time)+min(time)) #Instant at the middle of the data series (for convenience I pick a binary in the middle of the simulated data).
bind=abs(tmid-b['tc']).argmin() #Index of binary with coalescence time closest to tmid.
tevent=b['tc'][bind]

#Obtain PSD of the two detectors.
Pxx_H1,freqs=mlab.psd(strain_H1,Fs=fs,NFFT=NFFT)
Pxx_L1,freqs=mlab.psd(strain_L1,Fs=fs,NFFT=NFFT)
psd_H1=interp1d(freqs,Pxx_H1)
psd_L1=interp1d(freqs,Pxx_L1)

#Whiten data.
strain_H1_whiten=whiten(strain_H1,psd_H1,dt)
strain_L1_whiten=whiten(strain_L1,psd_L1,dt)
#Band pass data.
bb, ab = butter(4, [20.*2./fs, 300.*2./fs], btype='band')
strain_H1_whitenbp = filtfilt(bb, ab, strain_H1_whiten)
strain_L1_whitenbp = filtfilt(bb, ab, strain_L1_whiten)
#strain_L1_shift = -np.roll(strain_L1_whitenbp,int(0.007*fs)) #Shift data (if the exact time shift is known).

###################################

#Plot ASDs.
plt.figure(1)
plt.loglog(freqs,np.sqrt(Pxx_H1),'r',label='H1 strain')
plt.loglog(freqs,np.sqrt(Pxx_L1),'b',label='L1 strain')
#plt.xlim(fmin,fmax)
#plt.ylim(1e-24,1e-29)
plt.axis([fmin,fmax,1e-24,1e-19])
plt.grid('on')
plt.ylabel('ASD (strain/rtHz)')
plt.xlabel('Freq (Hz)')
plt.legend(loc='upper center')
plt.title('Simulated data run000')
plt.savefig(oplotdir+'asd_run000.png')

#Plot whitened time series with signal in the middle.
plt.figure(2)
plt.plot(time-tevent,strain_H1_whitenbp,'r',label='H1 strain')
#plt.plot(time,strain_L1_shift,'g',label='L1 strain')
plt.plot(time-tevent,strain_L1_whitenbp,'g',label='L1 strain')
plt.legend(loc='lower left')
plt.title('Whitened simulated data %s around binary %i' %(sim,bind))
plt.xlim(-0.3,0.3)
plt.ylim(-8.,8.)
plt.savefig(oplotdir+'whitened_data_run000.png')

