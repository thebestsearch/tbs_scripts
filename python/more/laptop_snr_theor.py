import numpy as np
from scipy.interpolate import interp1d
import modules.common as cm
from modules.common import hub0, h0, grav, light, msun, mpc
import inputs_fake_signal as ip
import modules.signal_model as sm

###############################

#Description.

#Calculate the theoretical (average) S/N of the cross-correlation of two detectors (with the same PSD) for a given signal (made of a list of black holes).

###############################

#Inputs.
ligo_psd_file='../../tbs_data/input/ZERO_DET_high_P_psd.txt' #File with LIGO power spectral density (PSD).
orf_lh_file='../../tbs_data/input/gammaLHOLLO.txt' #Overlap reduction function of the detector pair H-L.
sdir='../../tbs_data/fake_signal/bhb/' #Directory of realisations of the sources.
ddir='../../tbs_data/fake_signal/simdata/' #Directory of realisations of fake data.
nr=ip.nr #Number of realisations to consider (it can't be larger than the number of existing realisations).
dur=ip.dur #Duration of the observation of each data stream (in s).

###############################

#Define some parameters and functions (eventually, these functions could be stored in another file to make the code cleaner).

psd_d=np.loadtxt(ligo_psd_file) #LIGO PSD data.
psd_f,psd_a=psd_d[:,0],psd_d[:,1] #Frequency and amplitude of the PSD.
lpsd=interp1d(np.log(psd_f),np.log(psd_a)) #Function that interpolates log(f) over log(psd).
def psd_interp(f):
	'''Interpolated PSD of LIGO, as a function of frequency "f" (in Hz).'''
	return np.exp(lpsd(np.log(f)))

orf_d=np.loadtxt(orf_lh_file) #ORF of LIGO-H and LIGO-L.
orf_f,orf_a=orf_d[:,0],orf_d[:,1] #Frequency and amplitude of the ORF.
def orf_interp(f):
	'''Interpolated overlap reduction function as a function of "f" (in Hz).'''
	return interp1d(orf_f,orf_a)(f) #Function that interpolates f over the ORF.

ligofmin=max(min(orf_f),min(psd_f)) #Minimum frequency that can be considered in the calculations.
ligofmax=min(max(orf_f),max(psd_f)) #Maximum frequency that can be considered in the calculations.
ligoHL=cm.AttrDict({'fmin':ligofmin, 'fmax':ligofmax, 'psd1':psd_interp, 'psd2':psd_interp, 'orf':orf_interp}) #Detector pair: this is a class that returns the interpolated PSD of the two LIGO detectors (assumed identical), the overlap reduction function of the pair, and the minimum and maximum frequency where those functions are defined.

#def omega_binaries(f,tobs,b):
#	'''GW density parameter (averaged over time "tobs", in s) for an isotropic background made of individual binaries, as a function of frequency "f" (in Hz). The background is made of individual binaries, and is described by class "b", that contains:
#	+"b.z" (redshift),
#	+"b.inc" (inclination, in rad),
#	+"b.mch" (chirp mass, in solar mass),
#	+"b.dl" (luminosity distance, in Mpc).
#	This formula is taken from Eq. (8) of Regimbau et al. 2014, but corrected for two typos.'''
#	sumall=np.sum(((1.+b.z)**(5./3.)*(b.mch*msun)**(5./3.)*1./(b.dl*mpc)**2.)*((1.+np.cos(b.inc)**2.)**2./4.+np.cos(b.inc)**2.))
#	return 5.*np.pi**(2./3.)*grav**(5./3.)*1./(18.*light**3.*(hub0*h0)**2.*tobs)*f**(2./3.)*sumall

def fobs_isco(m1,m2,z):
        '''GW observed frequency of the ISCO of a binary of masses "m1" and "m2" (in solar masses) and redshift "z".'''
        return light**3./(6.*np.sqrt(6.)*np.pi*grav*(m1+m2)*msun*(1.+z))

def omega_binaries(f,tobs,b):
        '''GW density parameter (averaged over time "tobs", in s) for an isotropic background made of individual binaries, as a function of frequency "f" (in Hz). The background is made of individual binaries, and is described by class "b", that contains:
        +"b.z" (redshift),
        +"b.inc" (inclination, in rad),
        +"b.mch" (chirp mass, in solar mass),
        +"b.dl" (luminosity distance, in Mpc).
        This formula is taken from Eq. (8) of Regimbau et al. 2014, but corrected for two typos.'''

        allb,fmat=sm.combined_signal_f(b, f)
        #To avoid contribution of binaries to omega beyond the merger, once a binary merges, make the chirp mass zero.
        merged=(fmat>=fobs_isco(allb.m1,allb.m2,allb.z)) #Mask that becomes True if a binary has merged.
        allb.mch[merged]=0.

        sumall=np.sum((f**(2./3.)*(1.+allb.z)**(5./3.)*(allb.mch*msun)**(5./3.)*1./(allb.dl*mpc)**2.)*((1.+np.cos(allb.inc)**2.)**2./4.+np.cos(allb.inc)**2.),axis=0)
        return 5.*np.pi**(2./3.)*grav**(5./3.)*1./(18.*light**3.*(hub0*h0)**2.*tobs)*sumall

def snr_cc(tobs, b, pair):
	'''Average S/N of the cross-correlation of a pair of detectors "pair", which is a class that contains:
	+"pair.orf" (non-normalized overlap reduction function),
	+"pair.psd1" (PSD of detector 1),
	+"pair.psd2" (PSD of detector 2),
	+"pair.fmin" (minimum frequency that can be considered in the calculations in Hz),
	+"pair.fmax" (maximum frequency that can be considered in the calculations, in Hz).
	The GW signal is described by density parameter "om" (which is assumed isotropic).'''
	psd1=pair.psd1 #PSD of detector 1.
	psd2=pair.psd2 #PSD of detector 2.
	fmin=pair.fmin
	fmax=pair.fmax
	fnbins=10000 #Number of elements for the integral in frequency.
	f=np.logspace(np.log10(fmin), np.log10(fmax), fnbins) #Array of frequencies for the integral.
	gamma=pair.orf(f) #ORF of the pair.
	om=omega_binaries(f,tobs,b)

	integ=np.trapz((gamma**2.*om**2.)*1./(f**6.*psd1(f)*psd2(f)),f)
	return 3.*(hub0*h0)**2.*1./(4.*np.pi**2.)*np.sqrt(2.*tobs*integ)

def snr_true(tobs, b, pair, d):
        '''Numerical S/N. WORK IN PROGRESS!!! Average S/N of the cross-correlation of a pair of detectors "pair", which is a class that contains:
        +"pair.orf" (non-normalized overlap reduction function),
        +"pair.psd1" (PSD of detector 1),
        +"pair.psd2" (PSD of detector 2),
        +"pair.fmin" (minimum frequency that can be considered in the calculations in Hz),
        +"pair.fmax" (maximum frequency that can be considered in the calculations, in Hz).
        The GW signal is described by density parameter "om" (which is assumed isotropic).'''
        psd1=pair.psd1 #PSD of detector 1.
        psd2=pair.psd2 #PSD of detector 2.
        fmin=pair.fmin
        fmax=pair.fmax
        fnbins=10000 #Number of elements for the integral in frequency.
        f=np.logspace(np.log10(fmin), np.log10(fmax), fnbins) #Array of frequencies for the integral.
        gamma=pair.orf(f) #ORF of the pair.
        om=omega_binaries(f,tobs,b)

        optfil=gamma*om/(f**3.*psd1(f)*psd2(f))

	x1=np.fft.fft(d.d1)
	x2=np.fft.fft(d.d2)
	fftfreq=np.fft.fftfreq(len(d.d1),1./ip.srate)

	x1_re=interp1d(fftfreq,np.real(x1))(f)
	x1_im=interp1d(fftfreq,np.imag(x1))(f)
	x2_re=interp1d(fftfreq,np.real(x2))(f)
	x2_im=interp1d(fftfreq,np.imag(x2))(f)
	
	integ=(x1_re-1j*x1_im)*optfil*(x2_re+1j*x2_im)
	integ_re=np.real(integ)
	#integ=np.conj(x1)*optfil*x2
	#integ_re=interp1d(fftfreq,np.real(integ))(f)
	#integ_im=inpter1d(fftfreq,np.imag(integ))(f)
	#Y=np.trapz(integ_re,f)+1j*np.trapz(integ_im,f)
	Y=np.trapz(integ_re,f)

        integ=np.trapz((gamma**2.*om**2.)*1./(f**6.*psd1(f)*psd2(f)),f)
        snrtheor=3.*(hub0*h0)**2.*1./(4.*np.pi**2.)*np.sqrt(2.*tobs*integ)

	return snrtheor, Y
	

###############################

#Calculate S/N of the realisations of fake signals and plot signals.
import pylab as py
py.ion()

#Yvec=np.zeros(nr)
#snrtheor=np.zeros(nr)
#f=np.logspace(np.log10(ligoHL.fmin), np.log10(ligoHL.fmax), 100) #Array of frequencies for the integral.
#for ri in xrange(nr):
#	b=cm.AttrDict(np.load(sdir+'run%.3i.npy' %int(ri))[()])
#	d=cm.AttrDict(np.load(ddir+'run%.3i.npy' %int(ri))[()])
#	snrtheor[ri],Yvec[ri]=snr_true(dur, b, ligoHL, d)
#	
#print np.mean(Yvec)*1./np.std(Yvec)
#print np.mean(snrtheor)

py.figure(1)

f=np.logspace(np.log10(ligoHL.fmin), np.log10(ligoHL.fmax), 100) #Array of frequencies for the integral.
for ri in xrange(nr):
	b=cm.AttrDict(np.load(sdir+'run%.3i.npy' %int(ri))[()])
	d=cm.AttrDict(np.load(ddir+'run%.3i.npy' %int(ri))[()])

	om=omega_binaries(f,dur,b)
	py.loglog(f,om)
	py.ylabel('Omega(f)')
	py.xlabel('f/Hz')
py.figure(2)
for ri in xrange(nr):
	b=cm.AttrDict(np.load(sdir+'run%.3i.npy' %int(ri))[()])
	d=cm.AttrDict(np.load(ddir+'run%.3i.npy' %int(ri))[()])

	snr=snr_cc(dur,b,ligoHL)
	py.clf()
	py.plot(d.t,d.d1,color='red')
	py.plot(d.t,d.d2,color='blue')
	py.plot(d.t, d.h,color='green')
	py.ylim(-4e-20,4e-20)
	py.xlabel('t/s')
	py.ylabel('strain+noise')
	py.title('Cross-correlation S/N for this realisation: %.3e' %snr)
	raw_input('enter to continue')


