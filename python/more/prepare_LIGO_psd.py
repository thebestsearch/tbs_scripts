import numpy as np
import pylab as py

########################

#Inputs.
ifile='./ZERO_DET_high_P_psd.txt'
ofile='./LIGO_psd.txt'

########################

#The LIGO psd has to be linearly spaced in frequency, and starting from 0.

d=np.loadtxt(ifile)

ld_f=np.log10(d[:,0])
ld_p=np.log10(d[:,1])

f=np.arange(0.,4096,1./50.)

fp=f[1:]

lfp=np.log10(fp)

psd=10.**(np.interp(lfp,ld_f,ld_p))

psd=np.hstack((np.array([0.]),psd))

oarr=np.vstack((f,psd)).T

np.savetxt(ofile,oarr)

py.ion()
py.loglog(fp,psd[1:])
raw_input('enter')

