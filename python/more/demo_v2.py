#!/usr/bin/env python
import numpy as np
from modules.gaussian_noise import gaussian_noise
from modules.fake_signal import fake_signal
import pylab as py

###############

#Description.

#Matlab function "demo_v2" by Eric Thrane (eric.thrane@ligo.org) adapted to Python by Pablo A. Rosado (prosado@swin.edu.au).
#From Eric:
'''
% function demo_v2
% eric.thrane@ligo.org
% demonstrate non-Gaussian search for sine waves
% for this version:
%  * 8 seconds of data with Gaussian noise + 300 sinusoids
%  * each sinusoid has phase phi=0
%  * we assume that the strain amplitude of each signal is exactly known
%  * we assume that the frequency falls exactly on an FFT frequency bin
%  * each signal has a signal-to-noise ratio of 0.5
'''
###############

#Input parameters.

import input_parameters as ip

###############

#Functions.

def fake_signal(Ns, fmin, fs):
	'''Generates an array of frequencies and an array of amplitudes of fake GW monochromatic sources.
	+Ns is the number of injected signals.
	+fmin is the minimum frequency.
	+fs is the sampling rate.'''
	fmax = fs*0.5 #Maximum frequency of injected signasl (Nyquist frequency).
	
	#f0 = fmin + (fmax-fmin)*np.random.random(Ns) #Array of Ns frequencies between fmin and fmax. Is not this the same as a uniform distribution???
	f0 = np.random.uniform(low = fmin, high = fmax, size = Ns) #Array of Ns frequencies between fmin and fmax.
	return f0

def demo_v2():
	''''''
	#Load PSD.
	Sh=np.loadtxt(ip.ligo_psd_file)
	
	#Generate random Gaussian noise.
	n = gaussian_noise(Sh, ip.fs, ip.dur)[0]

	#Generate signal, made of random frequencies.
	f0 = fake_signal(ip.Ns, ip.fmin, ip.fs)

	

	return n, f0

###############

#Action.

if __name__=='__main__': #When running this file, the following actions will be executed. Otherwise, if this file is imported, nothing else will happen.
	
	n, f0 = demo_v2()

	py.ion()
	py.figure(1)
	py.plot(f0)

	py.figure(2)
	py.plot(n)
	raw_input('enter')
