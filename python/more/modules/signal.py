import numpy as np
from scipy.special import erfc, erfcinv

####################

#Description.

#Create some functions that can be used to calculate optimal S/N.

####################

#Define constants.
hub0=3.2404407e-18
grav=6.673e-11
light=2.99792458e8
yr=365*24*3600.
myr=yr*1e6
gyr=yr*1e9
pc=3.086e16
kpc=pc*1e3
mpc=pc*1e6
gpc=pc*1e9
msun=1.9891e30
nanosec=1e-9
rsun=6.955e8
week=7.*24.*3600.

#Define cosmological parameters.
h0=0.678
omm=0.308
omv=0.692

#Define functions.

class AttrDict(dict):
        '''Class that converts dictionary keys into class attributes, from "http://stackoverflow.com/questions/4984647/accessing-dict-keys-like-an-attribute-in-python".'''
        def __init__(self, *args, **kwargs):
                super(AttrDict, self).__init__(*args, **kwargs)
                self.__dict__ = self

def mchirp(m1,m2):
        '''Physical chirp mass of a binary of masses m1 and m2'''
        return (m1*m2)**(3./5.)/(m1+m2)**(1./5.)

def Sn_white(rms, cad, freq):
	'''Detector strain noise; assume pulsar white noise.'''
	return 2.*rms**2.*cad*(12.*(np.pi**2.)*(freq**2.))

def opt_snr_mon(tobs, strain, Sn):
	'''Optimal S/N of a matched filter search on a monochromatic binary.'''
	return 2.*np.sqrt(tobs*1./Sn)*strain

def strain(mch, z, dl, f):
	'''Time domain GW strain amplitude of an inspiralling binary.'''
	return 4.*np.pi**(2./3.)*(grav*mch*msun*(1.+z))**(5./3.)/(light**4.*dl*mpc)*f**(2./3.)

def helldown_fun(rel):
        '''Hellings & Downs matrix (non-zero only at the elements that contribute to the cross correlation); The relative angle between the pulsars is "rel".'''
        return 3./4.*(1.-np.cos(rel))*np.log(0.5*(1.-np.cos(rel)))-(1.-np.cos(rel))*1./8.+0.5

def relangle(ras1, ras2, dec1, dec2):
        '''Relative angle (in rad) between two points with equatorial coordinates (ras1, dec1) and (ras2, dec2), in rad.'''
        return np.arccos( np.sin(dec2)*np.sin(dec1) + np.cos(dec2)*np.cos(dec1)*np.cos(ras2-ras1) )

def power_law(b, x, xi, yi):
	'''Creates a power law of the form y=a x^b, evaluated at vector x, that passes through point (xi, yi).'''
	a=yi*1./(xi**b)
	return a*x**b

def get_signal_data(d):
	'''Put data into a more convenient way.'''
	z = d['reds']
        mch = 10**(d['logchmass'])
        dl = d['cmdist']*(1.+z)
        f = 10**(d['logfobs'])
	sd=AttrDict({'z':z, 'f':f, 'dl':dl, 'mch':mch})
	return sd

def make_pulsar_data(numpul, rms, cad):
	phi=np.random.uniform(0., 2.*np.pi, numpul) #Phi coordinate (goes between 0 and 2 pi).
	theta=np.arccos(np.random.uniform(-1., 1., numpul)) #Theta coordiante (goes between 0 and pi).
	ras=phi #Right ascension.
	dec=(theta-np.pi/2.) #Declination.
	#Number of pulsar pairs.
	pairs=numpul*(numpul-1)/2
	#Get H&D coefficient for these pairs. First locate pulsars uniformly in the sky.
	hd=[] #H&D coefficients for each pulsar pair.
	for pi in xrange(numpul):
	        for pj in xrange(numpul):
	                if pi>pj:
        	                hd.append(helldown_fun(relangle(ras[pi], ras[pj], dec[pi], dec[pj])))
	#Check that \Gamma_{ij}^2<<1.
	hd=np.array(hd)
	hdterm=np.sum(hd**2.)
	pd = AttrDict({'cad':cad, 'numpul':numpul, 'rms':rms, 'ras':ras, 'dec':dec, 'hdterm':hdterm})
	return pd

def SNR_best(sd, pd, tobs):
	'''Optimal S/N of a matched filter search, where all pulsars are identical, and all binaries can be resolved.'''
        #Select binaries within the available frequency window.
        fmin=1./tobs #Minimum observable frequency.
        fmax=1./pd.cad #Maximum observable frequency.
        fbin=fmin #Size of the frequency bin.
        sel=(sd.f>fmin)&(sd.f<fmax)
        z=sd.z[sel]
        mch=sd.mch[sel]
        dl=sd.dl[sel]
        f=sd.f[sel]

        #Calculate optimal matched filter S/N assuming of each binary for one pulsar.
        h = strain(mch, z, dl, f)
        Sn = Sn_white(pd.rms, pd.cad, f)
        SNR_pi = opt_snr_mon(tobs, h, Sn)

        #Combined S/N for an array of pulsars would be SNR**2=SNR_1**2+SNR_2**2+...=N*SNR_pi**2, with N number of pulsars.
        SNR = np.sqrt( pd.numpul )*SNR_pi

        #The combined S/N for all binaries is obtained in a similar way.
        SNR_all = np.sqrt( np.sum(SNR**2.) )
	return SNR_all

def SNR_B(sd, pd, tobs):
	'''S/N B, as defined in RosadoEtAl2015.'''
        #Using Eq. (A19) of RosadoEtAl2015, if all pulsars have identical noise, and assuming \Gamma_{ij}^2<<1,  S/N_B^2=2 \sum_f (S_h^2/(P^2+2PS_h+S_h^2)) [\sum_{ij}\Gamma_{ij}^2]. I call [...]=hdterm.

        #Select binaries within the available frequency window.
        fmin=1./tobs #Minimum observable frequency.
        fmax=1./pd.cad #Maximum observable frequency.
        fbin=fmin #Size of the frequency bin.
        sel=(sd.f>fmin)&(sd.f<fmax)
        z=sd.z[sel]
        mch=sd.mch[sel]
        dl=sd.dl[sel]
        f=sd.f[sel]
	h = strain(mch, z, dl, f)
        fvec=np.arange(fmin, fmax, fbin)

        SNRBsq_fi=np.zeros(len(fvec))
        P_i=2.*pd.rms**2.*pd.cad
        for fi in xrange(len(fvec)):
                flow=fvec[fi]
                fsel=(f>=flow)&(f<flow+fbin)
                if len(f[fsel])>0:
                        fmean=np.mean(f[fsel])
                        hcsq=np.sum(h[fsel]**2.*fmean/fbin)
                        Sh=hcsq/(12.*np.pi**2.*fmean**3.)
                        SNRBsq_fi_elem=2.*Sh**2.*1./(P_i**2.+2.*P_i*Sh+Sh**2.)*pd.hdterm
                        SNRBsq_fi[fi]=SNRBsq_fi_elem
        SNRB=np.sqrt(np.sum(SNRBsq_fi))
	return SNRB

def SNR_A(sd, pd, tobs):
        '''S/N A, as defined in RosadoEtAl2015.'''

        #Select binaries within the available frequency window.
        fmin=1./tobs #Minimum observable frequency.
        fmax=1./pd.cad #Maximum observable frequency.
        fbin=fmin #Size of the frequency bin.
        sel=(sd.f>fmin)&(sd.f<fmax)
        z=sd.z[sel]
        mch=sd.mch[sel]
        dl=sd.dl[sel]
        f=sd.f[sel]
        h = strain(mch, z, dl, f)
        fvec=np.arange(fmin, fmax, fbin)

        SNRAsq_fi=np.zeros(len(fvec))
        P_i=2.*pd.rms**2.*pd.cad
        for fi in xrange(len(fvec)):
                flow=fvec[fi]
                fsel=(f>=flow)&(f<flow+fbin)
                if len(f[fsel])>0:
                        fmean=np.mean(f[fsel])
                        hcsq=np.sum(h[fsel]**2.*fmean/fbin)
                        Sh=hcsq/(12.*np.pi**2.*fmean**3.)
                        SNRAsq_fi_elem=2.*Sh**2.*1./(P_i**2.)*pd.hdterm
                        SNRAsq_fi[fi]=SNRAsq_fi_elem
        SNRA=np.sqrt(np.sum(SNRAsq_fi))
        return SNRA

def ASTAT(sd, pd, tobs):
	'''Detection Probability using the A-statistic.'''
        #Select binaries within the available frequency window.
        fmin=1./tobs #Minimum observable frequency.
        fmax=1./pd.cad #Maximum observable frequency.
        fbin=fmin #Size of the frequency bin.
        sel=(sd.f>fmin)&(sd.f<fmax)
        z=sd.z[sel]
        mch=sd.mch[sel]
        dl=sd.dl[sel]
        f=sd.f[sel]
        h = strain(mch, z, dl, f)
        fvec=np.arange(fmin, fmax, fbin)

        sigma0sq_fi=np.zeros(len(fvec))
	sigma1sq_fi=np.zeros(len(fvec))
	mu1_fi=np.zeros(len(fvec))
        P_i=2.*pd.rms**2.*pd.cad
        for fi in xrange(len(fvec)):
                flow=fvec[fi]
                fsel=(f>=flow)&(f<flow+fbin)
                if len(f[fsel])>0:
                        fmean=np.mean(f[fsel])
                        hcsq=np.sum(h[fsel]**2.*fmean/fbin)
                        Sh=hcsq/(12.*np.pi**2.*fmean**3.)
                        sigma0sq_fi_elem=2.*Sh**2.*1./(P_i**2.)*pd.hdterm
			#Again assume that \Gamma_{ij}^2<<1.
			sigma1sq_fi_elem=2.*Sh**2.*(P_i**2.+Sh**2.+2.*P_i*Sh)/(P_i**4.)*pd.hdterm
			mu1_fi_elem=sigma0sq_fi_elem

			sigma0sq_fi[fi]=sigma0sq_fi_elem
                        sigma1sq_fi[fi]=sigma1sq_fi_elem
			mu1_fi[fi]=mu1_fi_elem
        sigma0=np.sqrt(np.sum(sigma0sq_fi))
        sigma1=np.sqrt(np.sum(sigma1sq_fi))
        mu1=np.sum(mu1_fi)

        return sigma0, sigma1, mu1

def STAT(sd, pd, tobs):
	'''Detection Probability using the A- and B-statistics.'''
        #Select binaries within the available frequency window.
        fmin=1./tobs #Minimum observable frequency.
        fmax=1./pd.cad #Maximum observable frequency.
        fbin=fmin #Size of the frequency bin.
        sel=(sd.f>fmin)&(sd.f<fmax)
        z=sd.z[sel]
        mch=sd.mch[sel]
        dl=sd.dl[sel]
        f=sd.f[sel]
        h = strain(mch, z, dl, f)
        fvec=np.arange(fmin, fmax, fbin)

        Asigma0sq_fi=np.zeros(len(fvec))
	Asigma1sq_fi=np.zeros(len(fvec))
	Amu1_fi=np.zeros(len(fvec))
        Bsigma0sq_fi=np.zeros(len(fvec))
	Bsigma1sq_fi=np.zeros(len(fvec))
	Bmu1_fi=np.zeros(len(fvec))
        P_i=2.*pd.rms**2.*pd.cad
        for fi in xrange(len(fvec)):
                flow=fvec[fi]
                fsel=(f>=flow)&(f<flow+fbin)
                if len(f[fsel])>0:
                        fmean=np.mean(f[fsel])
                        hcsq=np.sum(h[fsel]**2.*fmean/fbin)
                        Sh=hcsq/(12.*np.pi**2.*fmean**3.)
                        Asigma0sq_fi_elem=2.*Sh**2.*1./(P_i**2.)*pd.hdterm
			#Again assume that \Gamma_{ij}^2<<1.
			Asigma1sq_fi_elem=2.*Sh**2.*(P_i**2.+Sh**2.+2.*P_i*Sh)/(P_i**4.)*pd.hdterm
			Amu1_fi_elem=Asigma0sq_fi_elem
                        Bsigma0sq_fi_elem=2.*Sh**2.*P_i**2.*1./((P_i**2.+Sh**2.+2.*P_i*Sh)**2.)*pd.hdterm
			Bsigma1sq_fi_elem=2.*Sh**2.*1./(P_i**2.+Sh**2.+2.*P_i*Sh)*pd.hdterm
			Bmu1_fi_elem=Bsigma1sq_fi_elem

			Asigma0sq_fi[fi]=Asigma0sq_fi_elem
                        Asigma1sq_fi[fi]=Asigma1sq_fi_elem
			Amu1_fi[fi]=Amu1_fi_elem
			Bsigma0sq_fi[fi]=Bsigma0sq_fi_elem
                        Bsigma1sq_fi[fi]=Bsigma1sq_fi_elem
			Bmu1_fi[fi]=Bmu1_fi_elem
        Asigma0=np.sqrt(np.sum(Asigma0sq_fi))
        Asigma1=np.sqrt(np.sum(Asigma1sq_fi))
        Amu1=np.sum(Amu1_fi)
        Bsigma0=np.sqrt(np.sum(Bsigma0sq_fi))
        Bsigma1=np.sqrt(np.sum(Bsigma1sq_fi))
        Bmu1=np.sum(Bmu1_fi)

        return Asigma0, Asigma1, Amu1, Bsigma0, Bsigma1, Bmu1


def DP(fap, sigma0, sigma1, mu1):
	''''''
	dp=0.5*erfc((np.sqrt(2)*sigma0*erfcinv(2.*fap)-mu1)*1./(np.sqrt(2.)*sigma1))
	return dp
