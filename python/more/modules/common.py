import numpy as np

#Define constants.
hub0=3.2404407e-18
grav=6.673e-11
light=2.99792458e8
day=24.*3600. #Solar day.
yr=365.*day
myr=yr*1e6
gyr=yr*1e9
pc=3.086e16
kpc=pc*1e3
mpc=pc*1e6
gpc=pc*1e9
msun=1.9891e30
nanosec=1e-9
rsun=6.955e8
week=7.*24.*3600.

#Define cosmological parameters.
h0=0.678
omm=0.308
omv=0.692

#Define some common functions and classes.

class AttrDict(dict):
        '''Class that converts dictionary keys into class attributes, from "http://stackoverflow.com/questions/4984647/accessing-dict-keys-like-an-attribute-in-python".'''
        def __init__(self, *args, **kwargs):
                super(AttrDict, self).__init__(*args, **kwargs)
                self.__dict__ = self

