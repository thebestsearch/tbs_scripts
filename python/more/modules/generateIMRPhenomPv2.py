import lalsimulation as lalsim
import lal
import numpy as np

###################################

#Description.

#Adapted from Rory Smith's original code, taken from:
#/home/rory.smith/projects/stochasticCBC/pythonWaveformExamples/generateIMRPhenomPv2.py

###################################

#Define functions.

def lalsim_IMRPhenomPv2(fmin, fmax, deltaF, dist, m1, m2, S1, S2, fRef, iota, RA, DEC, psi, phi, tc, ifo):
# dist in mpc
# m1, m2 in solar masses
# S1,S2 are spin vectors, e.g., S1=[0,0.3,0.2] with sqrt(S1[0] + S1[1] + S1[2]) <= 1
# fRef: reference frequency at which spins are calculated
# iota: inclination
# RA, DEC
# psi: polarization angle
# phi: orbital phase at fRef
# tc: coalescence time at geocenter (a GPS time)
# ifo: either 'H1', 'L1' or 'V1'

# Returns the Fourier transform of the waveform strain in ifo including the timeshift from geocenter

    tgps = lal.LIGOTimeGPS(tc)
    gmst = lal.GreenwichMeanSiderealTime(tgps)

    dist *= 1e6*lal.lal.PC_SI
    m1 *= lal.lal.MSUN_SI
    m2 *= lal.lal.MSUN_SI

    hplus,hcross = lalsim.SimInspiralChooseFDWaveform(phi, deltaF,\
                                                  m1, m2,\
                                                  S1[0], S1[1], S1[2],\
                                                  S2[0], S2[1], S2[2],\
                                                  fmin, fmax, fRef,\
                                                  dist, iota,\
                                                      0.0, 0.0, None, None,\
                                                      0, 0,\
                                                      lalsim.IMRPhenomPv2)

    h_p = hplus.data.data[(fmin/hplus.deltaF):] #grab the waveform from fmin
    h_c = hcross.data.data[(fmin/hcross.deltaF):]

    h = np.zeros_like(h_p, dtype=complex)

    if ifo == 'H1':
        diff = lal.LALDetectorIndexLHODIFF
    elif ifo == 'L1':
        diff = lal.LALDetectorIndexLLODIFF
    elif ifo == 'V1':
        diff = lal.LALDetectorIndexVIRGODIFF
    else:
        raise ValueError('detector not recognized: ' + ifo)

    timedelay = lal.TimeDelayFromEarthCenter(lal.CachedDetectors[diff].location, \
                                             RA, DEC, tgps)
    timeshift = tc + timedelay
    fplus, fcross = lal.ComputeDetAMResponse(lal.CachedDetectors[diff].response,\
                                         RA, DEC, psi, gmst)
    pit = np.pi*timeshift
    if timeshift != 0.0:
        shift = complex(1.0, 0)
        dshift = complex(-2.*np.sin(pit*deltaF)*np.sin(pit*deltaF), -2.*np.sin(pit*deltaF)*np.cos(pit*deltaF))
        for i in xrange(0,h_p.size):
            h[i] = shift*(fplus*h_p[i] + fcross*h_c[i])
            shift += shift*dshift
    else:
        h = (fplus*h_p) + (fcross*h_c)

    return h

def FD(fmin, fmax, deltaF, dist, m1, m2, S1, S2, fRef, iota, RA, DEC, psi, phi, tc, ifo):
# dist in mpc
# m1, m2 in solar masses
# S1,S2 are spin vectors, e.g., S1=[0,0.3,0.2] with sqrt(S1[0] + S1[1] + S1[2]) <= 1
# fRef: reference frequency at which spins are calculated
# iota: inclination
# RA, DEC
# psi: polarization angle
# phi: orbital phase at fRef
# tc: coalescence time at geocenter (a GPS time)
# ifo: either 'H1', 'L1' or 'V1'

# Returns the Fourier transform of the waveform strain in ifo including the timeshift from geocenter

	tgps = lal.LIGOTimeGPS(tc)
	gmst = lal.GreenwichMeanSiderealTime(tgps)

	#Distance and masses have to be in SI units.
	dist *= 1e6*lal.lal.PC_SI
	m1 *= lal.lal.MSUN_SI
	m2 *= lal.lal.MSUN_SI

	#Plus and cross polarisation waveforms in the frequency domain.
	hplus,hcross = lalsim.SimInspiralChooseFDWaveform(phi, deltaF, m1, m2, S1[0], S1[1], S1[2], S2[0], S2[1], S2[2], fmin, fmax, fRef, dist, iota, 0.0, 0.0, None, None, 0, 0, lalsim.IMRPhenomPv2)

	#Start the waveform at frequency fmin.
	h_p = hplus.data.data[(fmin/hplus.deltaF):]
	h_c = hcross.data.data[(fmin/hcross.deltaF):]

	#The coalescence (more precisely, the maximum of the waveform) occurs at 'tc' at the geocenter. Calculate time delay between geocenter and the chosen detector.
	if ifo == 'H1':
		diff = lal.LALDetectorIndexLHODIFF
	elif ifo == 'L1':
		diff = lal.LALDetectorIndexLLODIFF
	elif ifo == 'V1':
		diff = lal.LALDetectorIndexVIRGODIFF
	else:
		raise ValueError('detector not recognized: ' + ifo)
	timedelay = lal.TimeDelayFromEarthCenter(lal.CachedDetectors[diff].location, RA, DEC, tgps)
	timeshift = tc + timedelay

	#timeshift=0.0 #TEST.

	#Obtain antenna pattern factors for the detector for a source at location RA,DEC at the precise instant of coalescence (and assume these factors do not change significantly during the entire chirp).
	fplus, fcross = lal.ComputeDetAMResponse(lal.CachedDetectors[diff].response, RA, DEC, psi, gmst)

	#Calculate the observed strain at the detector, properly shifting the waveform from geocenter to detector frame.
	h = np.zeros_like(h_p, dtype=complex)
	pit = np.pi*timeshift
	if timeshift != 0.0:
		shift = complex(1.0, 0)
		dshift = complex(-2.*np.sin(pit*deltaF)*np.sin(pit*deltaF), -2.*np.sin(pit*deltaF)*np.cos(pit*deltaF))
		for i in xrange(0,h_p.size):
			h[i] = shift*(fplus*h_p[i] + fcross*h_c[i])
			shift += shift*dshift
	else:
		h = (fplus*h_p) + (fcross*h_c)

	return h

