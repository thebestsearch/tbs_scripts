import numpy as np
from common import msun, grav, light, mpc, hub0, h0 
import common as cm

##############################

#Description.

##############################

#Define functions.

def mchirp(m1,m2):
        '''Physical chirp mass of a binary of masses m1 and m2'''
        return (m1*m2)**(3./5.)/(m1+m2)**(1./5.)

def hamp_t(b, t):
        '''Observed GW amplitude (in the time domain, as a function of time "t", in s) of a binary described by "b", which is a class that contains:
	+b.mch (physical chirp mass, in solar mass),
	+b.z (redshift),
	+b.dl (luminosity distance, in Mpc),
	+b.tc (observed instant of coalescence, in s).'''
        return 5.**(1./4.)*(grav*b.mch*msun*(1.+b.z))**(5./4.)*1./(b.dl*mpc*light**(11./4.)*(b.tc-t)**(1./4.))

def phase_t(b, t):
        '''Observed GW phase (in the time domain, as a function of time "t", in s) of a binary described by "b", which is a class that contains:
	+b.mch (physical chirp mass, in solar mass),
	+b.z (redshift),
	+b.dl (luminosity distance, in Mpc),
	+b.tc (observed instant of coalescence, in s).'''
        return b.phic-2.*(light**3.*(b.tc-t)*1./(5.*grav*b.mch*msun*(1.+b.z)))**(5./8.)

def hplus(b, t):
        '''Observed plus GW polarisation (in the time domain, as a function of time "t", in s) of a binary described by "b", which is a class that contains:
	+b.mch (physical chirp mass, in solar mass),
	+b.z (redshift),
	+b.dl (luminosity distance, in Mpc),
	+b.tc (observed instant of coalescence, in s).'''
        afun=0.5*(1.+np.cos(b.inc)**2.)
        return hamp_t(b,t)*afun*np.cos(phase_t(b,t))

def hcross(b, t):
        '''Observed cross GW polarisation (in the time domain, as a function of time "t", in s) of a binary described by "b", which is a class that contains:
	+b.mch (physical chirp mass, in solar mass),
	+b.z (redshift),
	+b.dl (luminosity distance, in Mpc),
	+b.tc (observed instant of coalescence, in s).'''
        bfun=np.cos(b.inc)
        return hamp_t(b,t)*bfun*np.sin(phase_t(b,t))

def combined_signal(b, t):
	'''Takes dictionary "b", with the properties of each binary, and array "t" of time samples (in s), and outputs another dictionary, in which each element of "b" is now a matrix. This matrix contains all properties of "b" repeated as many times as time samples.'''
	nt,nb=len(t),len(b['z']) #Number of time samples and binaries.

        #Create a matrix in which each element is a time sample of the signal for each binary.
        tmat=np.tile(t, (nb,1)) #Matrix of time samples for each binary.
	c_dict={} #Combined dictionary: take each element in "b" and output the same element in a matrix of dimensions (nb x nt).
	for item in b.keys():
		key='%s' %item
		c_dict[key]=np.tile(b[key], (nt,1)).T
	return cm.AttrDict(c_dict), tmat
	
def combined_signal_f(b, f):
	'''Takes dictionary "b", with the properties of each binary, and array "f" of frequency samples (in Hz), and outputs another dictionary, in which each element of "b" is now a matrix. This matrix contains all properties of "b" repeated as many times as frequency samples.'''
	nf,nb=len(f),len(b['z']) #Number of time samples and binaries.

        #Create a matrix in which each element is a frequency sample of the signal for each binary.
        fmat=np.tile(f, (nb,1)) #Matrix of frequency samples for each binary.
	c_dict={} #Combined dictionary: take each element in "b" and output the same element in a matrix of dimensions (nb x nf).
	for item in b.keys():
		key='%s' %item
		c_dict[key]=np.tile(b[key], (nf,1)).T
	return cm.AttrDict(c_dict), fmat

def fobs_isco(m1,m2,z):
        '''GW observed frequency of the ISCO of a binary of masses "m1" and "m2" (in solar masses) and redshift "z".'''
        return light**3./(6.*np.sqrt(6.)*np.pi*grav*(m1+m2)*msun*(1.+z))

def omega_binaries(f,tobs,b):
        '''GW density parameter (averaged over time "tobs", in s) for an isotropic background made of individual binaries, as a function of frequency "f" (in Hz). The background is made of individual binaries, and is described by class "b", that contains:
        +"b.z" (redshift),
        +"b.inc" (inclination, in rad),
        +"b.mch" (chirp mass, in solar mass),
        +"b.dl" (luminosity distance, in Mpc).
        This formula is taken from Eq. (8) of Regimbau et al. 2014, but corrected for two typos.'''

        nb=len(b.z)
        sumall=np.zeros(len(f))
        for bi in xrange(nb):
                z, m1, m2, dl, mch, inc = b.z[bi], b.m1[bi], b.m2[bi], b.dl[bi], b.mch[bi], b.inc[bi]
                foisco=fobs_isco(m1,m2,z)
                sel=(f<foisco)
                sumall[sel]+=(f[sel]**(2./3.)*(1.+z)**(5./3.)*(mch*msun)**(5./3.)*1./(dl*mpc)**2.)*((1.+np.cos(inc)**2.)**2./4.+np.cos(inc)**2.)
        #To avoid contribution of binaries to omega beyond the merger, once a binary merges, make the chirp mass zero.

        return 5.*np.pi**(2./3.)*grav**(5./3.)*1./(18.*light**3.*(hub0*h0)**2.*tobs)*sumall

def snr_theo(tobs, b, pair):
        '''Average S/N of the cross-correlation of a pair of detectors "pair", which is a class that contains:
        +"pair.orf" (non-normalized overlap reduction function),
        +"pair.psd1" (PSD of detector 1),
        +"pair.psd2" (PSD of detector 2),
        +"pair.fmin" (minimum frequency that can be considered in the calculations in Hz),
        +"pair.fmax" (maximum frequency that can be considered in the calculations, in Hz).
        The GW signal is described by density parameter "om" (which is assumed isotropic).'''
        psd1=pair.psd1 #PSD of detector 1.
        psd2=pair.psd2 #PSD of detector 2.
        fmin=pair.fmin
        fmax=pair.fmax
        fnbins=10000 #Number of elements for the integral in frequency.
        f=np.logspace(np.log10(fmin), np.log10(fmax), fnbins) #Array of frequencies for the integral.
        gamma=pair.orf(f) #ORF of the pair.
        om=omega_binaries(f,tobs,b)

        integ=np.trapz((gamma**2.*om**2.)*1./(f**6.*psd1(f)*psd2(f)),f)
        return 3.*(hub0*h0)**2.*1./(4.*np.pi**2.)*np.sqrt(2.*tobs*integ)

def snr_calc(pair, f, Sh, snr_norm):
        '''Numerical S/N. WORK IN PROGRESS!!! Cross-correlation of a pair of detectors "pair", which is a class that contains:
        +"pair.orf" (non-normalized overlap reduction function),
        +"pair.psd1" (PSD of detector 1),
        +"pair.psd2" (PSD of detector 2),
        +"pair.fmin" (minimum frequency that can be considered in the calculations in Hz),
        +"pair.fmax" (maximum frequency that can be considered in the calculations, in Hz).
        The GW signal is described by density parameter "om" (which is assumed isotropic).'''
        fmin=pair.fmin
        fmax=pair.fmax
        fmask=(f>fmin)&(f<fmax)
        f=f[fmask]
        Sh=Sh[fmask]
        df=np.hstack((np.diff(f),0)) #Infinitesimal interval of frequency.
        orf=pair.orf(abs(f)) #Overlap reduction function (small gamma).
        #f[f==0.]=1e-90 #To avoid nans (it does not matter, since om will be zero at that frequency).
        psd1=pair.psd1(abs(f)) #PSD of detector 1.
        psd2=pair.psd2(abs(f)) #PSD of detector 2.
        om=abs(f)**(2./3.)*1e-9 #The exact value of the constant does not matter, since the S/N will be normalised.
        Q=orf*om/(abs(f)**3.*psd1*psd2) #Optimal filter.
        numerator=abs(np.sum(Q*orf*Sh*df)) #Since this comes from the square root of the definition of the S/N^2.
        denominator=np.sum(Q*Q*psd1*psd2*df)**(1./2.)
        snr=snr_norm*numerator*1./denominator

        return snr

def Sh_fun(s1,s2):
        '''.'''
        s1t=np.fft.fft(s1)
        s2t=np.fft.fft(s2)
        return np.real(np.conj(s1t)*s2t)

