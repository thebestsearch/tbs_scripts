import numpy as np
import pylab as py
import modules.signal as si

###################

#Description:

#I will take one realisation of the ensemble of BH binaries (circular, gw-driven binaries), calculate the cross-correlation S/N (and DP), and calculate the individual S/N of each binary (assuming that you can resolve each of them).
#The latter case would be the best case detection scenario, in which you can resolve every binary; it would be interesting to see how much better this S/N (or DP) is, with respect to an optimal cross-correlation search.

###################

#Inputs.

#File with data of the ensemble of MBHBs.
mbhb_file = '../../tbs_data/input/MBHB/run000000.npy'
#Properties of the assumed PTA.
cad = 2.*si.week #Cadence.
rms = 100.*si.nanosec #White rms noise.
numpul = 25 #Number of pulsars (assumed all identical).
tobs = 20.*si.yr #Observing time.

tobsvec=np.arange(1.,30.,1)
SNR_best=np.zeros(len(tobsvec))
SNR_cc=np.zeros(len(tobsvec))
for ti in xrange(len(tobsvec)):
	print 'Observing time %.2f years.' %tobsvec[ti]
	print
	tobs=tobsvec[ti]*si.yr
	###################


	#Load data.

	d = np.load(mbhb_file)[()]
	z = d['reds']
	mch = 10**(d['logchmass'])
	dl = d['cmdist']*(1.+z)
	f = 10**(d['logfobs'])
	#Select binaries within the available frequency window.
	fmin=1./tobs #Minimum observable frequency.
	fmax=1./cad #Maximum observable frequency.
	fbin=fmin #Size of the frequency bin.
	sel=(f>fmin)&(f<fmax)
	z=z[sel]
	mch=mch[sel]
	dl=dl[sel]
	f=f[sel]

	#Calculate optimal matched filter S/N assuming of each binary for one pulsar.
	h = si.strain(mch, z, dl, f)
	Sn = si.Sn_white(rms, cad, f)
	SNR_pi = si.opt_snr_mon(tobs, h, Sn)

	#Combined S/N for an array of pulsars would be SNR**2=SNR_1**2+SNR_2**2+...=N*SNR_pi**2, with N number of pulsars.
	SNR = np.sqrt( numpul )*SNR_pi

	#The combined S/N for all binaries is obtained in a similar way.
	SNR_all = np.sqrt( np.sum(SNR**2.) )

	SNR_best[ti]=SNR_all

	#Number of pulsar pairs.
	pairs=numpul*(numpul-1)/2
	#Get H&D coefficient for these pairs. First locate pulsars uniformly in the sky.
	phi=np.random.uniform(0., 2.*np.pi, numpul) #Phi coordinate (goes between 0 and 2 pi).
	theta=np.arccos(np.random.uniform(-1., 1., numpul)) #Theta coordiante (goes between 0 and pi).
	ras=phi #Right ascension.
	dec=(theta-np.pi/2.) #Declination.
	hd=[] #H&D coefficients for each pulsar pair.
	for pi in xrange(numpul):
		for pj in xrange(numpul):
			if pi>pj:
				hd.append(si.helldown_fun(si.relangle(ras[pi], ras[pj], dec[pi], dec[pj])))
	#Using Eq. (A19) of RosadoEtAl2015, if all pulsars have identical noise, and assuming \Gamma_{ij}^2<<1,  S/N_B^2=2 \sum_f (S_h^2/(P^2+2PS_h+S_h^2)) [\sum_{ij}\Gamma_{ij}^2]. I call [...]=hdterm.  
	hdterm=np.sum(np.array(hd)**2.)

	fvec=np.arange(fmin, fmax, fbin)

	SNRBsq_fi=np.zeros(len(fvec))
	P_i=2.*rms**2.*cad
	for fi in xrange(len(fvec)):
		flow=fvec[fi]
		fsel=(f>=flow)&(f<flow+fbin)
		if len(f[fsel])>0:
			fmean=np.mean(f[fsel])
			hcsq=np.sum(h[fsel]**2.*fmean/fbin)
			Sh=hcsq/(12.*np.pi**2.*fmean**3.)
			SNRBsq_fi_elem=2.*Sh**2.*1./(P_i**2.+2.*P_i*Sh+Sh**2.)*hdterm
			SNRBsq_fi[fi]=SNRBsq_fi_elem
	SNRB=np.sqrt(np.sum(SNRBsq_fi))

	SNR_cc[ti]=SNRB

py.plot(tobsvec, SNR_best, color='red')
py.plot(tobsvec, SNR_cc, color='green')
py.show()

