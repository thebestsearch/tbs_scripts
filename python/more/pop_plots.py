import numpy as np
import modules.common as cm
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as py

########################

#Description.

#Plot properties of a realisation of the simulated population of bhbs.

########################

#Inputs.
idir='/home/pablo.rosado/projects/tbs/tbs_data/fake_signal/bhb/' #Folder of realisations.
oplotdir='/home/pablo.rosado/public_html/pop_plots/' #Folder for output plots.
ri='000' #Run (realisation) to consider.

########################

#Define some functions.

def coords(ras,dec):
        '''Returns the x,y hammer coordinates, given the RAS, DEC coordinates in rad.'''
	ras-=np.pi #The following transformations are for RAS defined between -pi and pi.
        xvec=(2.*np.sqrt(2.)*np.cos(dec)*np.sin(ras*1./2.))/np.sqrt(1.+np.cos(dec)*np.cos(ras*1./2.))
        yvec=(np.sqrt(2.)*np.sin(dec))/np.sqrt(1.+np.cos(dec)*np.cos(ras*1./2.))
        return xvec,yvec

def borders(numpoints):
        '''Returns the borders of a hammer projected map'''
        decvec=np.linspace(0.,360.,numpoints)
        xbord=2.*np.sqrt(2.)*np.cos(decvec)
        ybord=np.sqrt(2.)*np.sin(decvec)
        return xbord,ybord

########################

#Main.

#Load population.
d=cm.AttrDict(np.load(idir+'run'+ri+'.npy')[()])

print d.keys()

#Plot distribution in mch-z plane.
py.figure()
py.plot(d.mch,d.z,'.')
py.xlabel('Chirp mass / solar mass')
py.ylabel('Redshift')
py.title('BHB population %.3i' %int(ri))
py.savefig(oplotdir+'z_vs_mch_%.3i.png' %int(ri))

#Plot m2 vs m1 with z as color.
#py.figure()
#py.scatter(d.m1,d.m2,c=d.z)
#py.colorbar()
#py.savefig(oplotdir+'m2_m2_z_%.3i.png' %int(ri))

#Plot their distribution in the sky.
xbord,ybord=borders(3600)
xvectot,yvectot=coords(d.ras,d.dec)
left, right, top, bottom, c_fraction=0.02,0.98,0.93,0.04,0.145
fig = py.figure(frameon=False)
fig.subplots_adjust(left=left, right=right, top=top, bottom=bottom)
ax = fig.gca()
pointsize=29
py.plot(xbord,ybord,color='black',linewidth=1.15)
sct=py.scatter(xvectot,yvectot,c='black',s=pointsize,cmap=py.cm.jet_r,edgecolors='none',rasterized=True)
ax.set_xlim(-3.,3.)
ax.set_ylim(-1.5,1.5)
ax.axes.get_xaxis().set_visible(False)
ax.axes.get_yaxis().set_visible(False)
ax.set_frame_on(False)
ax.set_title('BHB population %.3i' %int(ri))
fig.savefig(oplotdir+'skymap_%.3i.png' %int(ri))
