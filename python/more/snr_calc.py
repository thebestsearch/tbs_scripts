import numpy as np
from scipy.interpolate import interp1d
import modules.common as cm
from modules.common import hub0, h0, grav, light, msun, mpc
import inputs_fake_signal as ip
import modules.signal_model as sm
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as py
from modules.gaussian_noise import gaussian_noise
from modules.detectors import ligoHL

###############################

#Description.

#Calculate the 'true' (measured) S/N of the cross-correlation of two detectors (with the same PSD) for a given signal (made of a list of black holes).

###############################

#Inputs.
sdir='../../tbs_data/fake_signal/bhb/' #Directory of realisations of the sources.
ddir='../../tbs_data/fake_signal/simdata/' #Directory of realisations of fake data.
nr=ip.nr #Number of realisations to consider (it can't be larger than the number of existing realisations).
dur=ip.dur #Duration of the observation of each data stream (in s).

###############################

#Obtain the normalisation of the S/N from pure noise.
ligo_psd=np.loadtxt(ligo_psd_file) #LIGO PSD.
fbins=int(dur*ip.srate)
f=np.fft.fftfreq(fbins,1./ip.srate)
noise_snr=np.zeros(200)
norm=1./3500000.
for i in xrange(200):
	n1=gaussian_noise(ligo_psd, ip.srate, dur, T=1, N=None)[0] #Noise for detector 1 (H1).	
	n2=gaussian_noise(ligo_psd, ip.srate, dur, T=1, N=None)[0] #Noise for detector 2 (L1).	
	Sh=sm.Sh_fun(n1,n2)
	noise_snr[i]=snr_calc(ligoHL, f, Sh, norm)

signal_snr=np.zeros(ip.nr)
for ri in xrange(ip.nr):
	d=cm.AttrDict(np.load(ddir+'run%.3i.npy' %int(ri))[()])
	s1=d.H1_data
	s2=d.L1_data
	Sh=Sh_fun(s1,s2)
	signal_snr[ri]=snr_true(ligoHL,f,Sh,norm)
	print 'SNR of realisation %.3i: %.3f' %(int(ri),signal_snr[ri])
print
print 'Summary:'
print 'Noise S/N'
print 'mean: ',np.mean(noise_snr)
print 'std: ',np.std(noise_snr)
print
print 'Signal S/N'
print 'mean: ',np.mean(signal_snr)
print 'std: ',np.std(signal_snr)
