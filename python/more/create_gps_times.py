import numpy as np

#Create the list of GPS times of coalescences of the injected binaries. They happen 3 seconds after pure noise, and each binary happens every 2 seconds. The offset in time is the initial GPS time.

ofile='./gps.txt'

gps_ini=1164405617
#tc=np.linspace(1.,10, 10)-0.2+3+gps_ini
tc=np.linspace(2.,20.,10)-0.2+3+gps_ini

np.savetxt(ofile,tc, fmt='%.3f')
