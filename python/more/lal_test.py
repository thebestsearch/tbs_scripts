import numpy as np
import lalsimulation as lalsim
#import matplotlib
#matplotlib.use('Agg')
#import matplotlib.pyplot as plt
#def lalsim_IMRPhenomPv2(fmin, fmax, deltaF, dist, m1, m2, S1, S2, fRef, iota, RA, DEC, psi, phi, tc, ifo):
#print lalsim_IMRPhenomPv2(20, 1024, 1./8., 100, 30, 30, [0,0,0], [0,0,0], 20, 0.1, 1.1, 1.0, 0.1, 0.2, 969379706, 'H1')
#SimInspiralChooseFDWaveform(REAL8 phiRef, REAL8 deltaF, REAL8 m1, REAL8 m2, REAL8 S1x, REAL8 S1y, REAL8 S1z, REAL8 S2x, REAL8 S2y, REAL8 S2z, REAL8 f_min, REAL8 f_max, REAL8 f_ref, REAL8 r, REAL8 i, REAL8 lambda1, REAL8 lambda2, SimInspiralWaveformFlags waveFlags, SimInspiralTestGRParam nonGRparams, int amplitudeO, int phaseO, Approximant approximant)
phi, deltaF, m1, m2, S1, S2, fmin, fmax, fRef, dist, iota, lambda1, lambda2, waveFlags, nonGRparams, amplitude0, phase0=0.2, 1./8., 30., 30., [0,0,0], [0,0,0], 20., 1024., 20., 100., 0.1, 0., 0., None, None, 0, 0

approximant=lalsim.IMRPhenomPv2
#approximant=lalsim.IMRPhenomA

hplus,hcross=lalsim.SimInspiralChooseFDWaveform(phi, deltaF,m1, m2, S1[0], S1[1], S1[2], S2[0], S2[1], S2[2], fmin, fmax, fRef, dist, iota, lambda1, lambda2, waveFlags, nonGRparams, amplitude0, phase0, approximant)

h_p,h_c=np.real(hplus.data.data),np.real(hcross.data.data)

for i in xrange(100):
	newhplus,newhcross=lalsim.SimInspiralChooseFDWaveform(phi, deltaF,m1, m2, S1[0], S1[1], S1[2], S2[0], S2[1], S2[2], fmin, fmax, fRef, dist, iota, lambda1, lambda2, waveFlags, nonGRparams, amplitude0, phase0, approximant)
	newh_p,newh_c=np.real(newhplus.data.data), np.real(newhcross.data.data)
	
	print 'h plus consistent?',np.array_equal(h_p,newh_p)
	#print 'h cross consistent?',np.array_equal(h_c,newh_c)
	#print
	
	#plt.clf()
	#plt.plot(h_p,color='red')
	#plt.plot(newh_p,color='blue')
	#plt.xlim(160.,560)
	#plt.savefig("/home/pablo.rosado/public_html/hplus.png")
	#raw_input('enter')
