function simple_scaling
% function simple_scaling
% eric.thrane@monash.edu
% investigate the scaling properties of a non-Gaussian search with a simple
% toy model

% number of segments (there is a signal in each one, i.e., xi=1)
N = 100000;

% many realisations
T = 100;

% initialise
Lambda_v1_avg=0;  Lambda_v2_avg=0;  Lambda_X_avg=0;  Lambda_Y_avg=0;

% simulate signal
%h0 = 2;
%h0 = 1;
h0 = 0.5;
%h0 = 0.25;
fprintf('h0 = %1.2e\n', h0);

% loop over realisations
for ii=1:T

  % simulate noise
  n1 = randn(N,1);
  n2 = randn(N,1);
  sigma = 1;

  % add signal + noise
  s1 = n1 + h0;
  %s2 = n2 + h0;

  % make a histogram
  x = [-8 : 0.1 : 8];
  if ii==1
    N1_v1 = hist(s1, x);
  end

  % calculate log likelihood for the signal hypothesis
  %LS = -cumsum((s1-h0).^2 / (2*sigma.^2) );
  tmp = exp(-(s1-h0).^2 / (2*sigma.^2));
  tmp = tmp/sqrt(2*pi*sigma^2);
  tmp = log(tmp);
  LS = cumsum(tmp);

  % calculate log likelihood for the noise hypothesis
  %LN = -cumsum((s1).^2 / (2*sigma.^2) );
  tmp = exp(-s1.^2 / (2*sigma.^2) );
  tmp = tmp/sqrt(2*pi*sigma^2);
  tmp = log(tmp);
  LN = cumsum(tmp);

  % log likelihood ratio
  Lambda_v1 = 2*(LS-LN);

  % noise model for actual noise
  tmp = exp(-n2.^2 / (2*sigma.^2) );
  tmp = tmp/sqrt(2*pi*sigma^2);
  tmp = log(tmp);
  LNN = cumsum(tmp);

  % analyse pure nosie with model 1
  tmp = exp(-(n2-h0).^2 / (2*sigma.^2) );
  tmp = tmp/sqrt(2*pi*sigma^2);
  tmp = log(tmp);
  LX = cumsum(tmp);
  Lambda_X = 2*(LX-LNN);

  % analyse pure nosie with model 2
  tmp = 0.5*exp(-(n2-h0).^2/(2*sigma.^2)) + 0.5*exp(-(n2+h0).^2/(2*sigma.^2));
  tmp = tmp/sqrt(2*pi*sigma^2);
  tmp = log(tmp);
  LY = cumsum(tmp);
  Lambda_Y = 2*(LY-LNN);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % Now repeat allowing for h to change signs
  h = h0*sign(randn(N,1));
  s1_v2 = n1 + h;
  %s2 = n2 + h;

  % make a histogram
  if ii==1
    N1_v2 = hist(s1_v2, x);
  end

  % calculate log likelihood for the noise hypothesis
  tmp = exp(-s1_v2.^2 / (2*sigma.^2) );
  tmp = tmp/sqrt(2*pi*sigma^2);
  tmp = log(tmp);
  LN_v2 = cumsum(tmp);

  % calculate log likelihood for the signal hypothesis
  % marginalise over sign uncertainty
  tmp = 0.5*exp(-(s1_v2-h0).^2/(2*sigma.^2)) + 0.5*exp(-(s1_v2+h0).^2/(2*sigma.^2));
  tmp = tmp/sqrt(2*pi*sigma^2);
  tmp = log(tmp);
  LS = cumsum(tmp);

  % log likelihood ratio
  Lambda_v2 = 2*(LS-LN_v2);

  % book-keeping
  Lambda_v1_avg = Lambda_v1_avg + Lambda_v1;
  Lambda_v2_avg = Lambda_v2_avg + Lambda_v2;
  Lambda_X_avg = Lambda_X_avg + Lambda_X;
  Lambda_Y_avg = Lambda_Y_avg + Lambda_Y;
end

% normalise
Lambda_v1_avg=Lambda_v1_avg/T;  Lambda_v2_avg=Lambda_v2_avg/T;
Lambda_X_avg=Lambda_X_avg/T;  Lambda_Y_avg=Lambda_Y_avg/T;

% what is the ratio of Lambda_v1 to Lambda_v2
R = Lambda_v1_avg(end)/Lambda_v2_avg(end);
fprintf('R = %1.2e\n', R);

% detection threshold
th = 2*log(1000);

% plot the likelihood ratio
figure;
subplot(2,1,1);
loglog([1:N], Lambda_v1_avg, 'b');
hold on;
loglog([1:N], Lambda_v2_avg, 'r--');
loglog([1 N], [th th], 'Color', [0 0.8 0]);
axis([1 N 1 2*log(1e9)]);
xlabel('N');
ylabel('\Lambda');
legend('Known sign', 'Unknown sign', 'Location', 'SouthEast');
title(['h = ' num2str(h0)]);
pretty
grid on;
%
subplot(2,1,2);
loglog([1:N], -Lambda_X_avg, 'k');
hold on;
loglog([1:N], -Lambda_Y_avg, 'Color', [0.5 0 0.5]);
loglog([1 N], [th th], 'Color', [0 0.8 0]);
axis([1 N 1 2*log(1e9)]);
xlabel('N');
ylabel('-\Lambda');
legend('Known sign', 'Unknown sign', 'Location', 'SouthEast');
title(['h = ' num2str(h0)]);
pretty
grid on;
print('-dpng', 'img/Lambda1');

% semilog version
figure;
semilogx([1:N], Lambda_v1_avg, 'b');
hold on;
semilogx([1:N], Lambda_v2_avg, 'r--');
semilogx([1 N], [th th], 'Color', [0 0.8 0]);
axis([1 N 0 2*log(1e9)]);
xlabel('N');
ylabel('\Lambda');
legend('Known sign', 'Unknown sign', 'Location', 'SouthEast');
title(['h = ' num2str(h0)]);
pretty
grid on;
print('-dpng', 'img/Lambda1_semilog');

% plot histogram of s
figure;
plot(x, N1_v1);
hold on;
plot(x, N1_v2);
xlabel('s');
legend('Known sign', 'Unknown sign');
title(['h = ' num2str(h0)]);
pretty;
print('-dpng', 'img/histo');

% state N2/N1 at Lambda = th
N1th = interp1(Lambda_v1_avg, [1:N], th);
N2th = interp1(Lambda_v2_avg, [1:N], th);
fprintf('(N1th, N2th) = (%1.1e, %1.1e)\n', N1th, N2th);

% plot (hard-coded) Nth as a function of h0
hvals = [0.25 0.5 1 2];
N1thvals = [2.1e+02 5.7e+01 1.4e+01 3.3e+00];
N2thvals = [7.0e+03 5.0e+02 3.7e+01 5.3e+00];
figure;
subplot(2, 1, 1);
loglog(hvals, N1thvals, 'b');
hold on;
loglog(hvals, N2thvals, 'r');
loglog(hvals, N1thvals.^2, 'b--');
grid on;
xlabel('h_0');
ylabel('N_{th}');
pretty;
subplot(2, 1, 2);
loglog(hvals, N1thvals./N2thvals, 'k');
grid on;
xlabel('h_0');
ylabel('N_{th} ratio');
pretty;
print('-dpng', 'img/Nth');

return

