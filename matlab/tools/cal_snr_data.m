function rho = cal_snr_data(u, h, fs, Sh)
% function rho = cal_snr_data(u, h, fs, Sh)
% eric.thrane@ligo.org
% h = strain time series (Nx1 array)
% u = template
% fs = sampling frequency
% Sh = noise power spectrum (Nbins x 2 array)
% rho = matched filter SNR

% Fourier transform
[hf f] = fft_eht(h, fs);
[uf f] = fft_eht(u, fs);

% normalise
hf = FT_norm(hf, fs);
uf = FT_norm(uf, fs);

% calculate
rho = inner_product(uf, hf, f, Sh)  / sqrt(inner_product(uf, uf, f, Sh));

return

