function rho = cal_snr(h, fs, Sh)
% function rho = cal_snr(h, fs, Sh)
% eric.thrane@ligo.org
% h = strain time series (Nx1 array)
% fs = sampling frequency
% Sh = noise power spectrum (Nbins x 2 array)
% rho = **expectation value** of matched filter SNR

% Fourier transform
[hf f] = fft_eht(h, fs);

% normalise
hf = FT_norm(hf, fs);

% calculate
rho = sqrt(inner_product(hf, hf, f, Sh));

return

