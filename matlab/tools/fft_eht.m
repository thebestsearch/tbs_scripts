function [hf f] = fft_eht(h, Fs)
% function [hf f] = fft_eht(h, Fs)
% Eric Thrane: performs an fft while keeping track of the frequency bins
% It is assumed that the input time series h is real.
%
%   h = (LxT) array where L is the number of samples and T is the number of
%     trials
%   Fs = sampling frequency
%   hf = single-sided FFT of h
%   f = frequencies associated with hf
%
% Update on 5 jan 2016: in order to get units of strain/rHz, use the quantity
% hf*sqrt(2/L/Fs). The normalization is chosen so that fft_eht creates an fft
% with the same normalization as matlab.

% if the time series does not have an even number of sampling times, add one 
% zero padding so that it is
if mod(size(h,1), 2)==1
  h = [zeros(1, size(h,2)) ; h];
end

% the number of sampling points
L = size(h,1);

% the frequency range
f = Fs/2*linspace(0, 1, L/2+1)';

% calculate FFT
hf = fft(h, [], 1);

% keep only positive frequencies
hf = hf(1:L/2+1, :);

return
