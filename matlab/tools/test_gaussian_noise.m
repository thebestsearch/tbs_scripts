function test_gaussian_noise
% function test_gaussian_noise

% load noise file
Sh = load('/home/ethrane/noisecurves/ZERO_DET_high_P_psd.txt');

% sample frequency
fs = 2048;

% duration
dur = 10;

% nsamples
nsamples = dur*fs;

% simulate Gaussian noise
hn = gaussian_noise(Sh, fs, dur, 1, nsamples);

% calculate power spectrum
[hnf f] = fft_eht(hn, fs);
Pf = psd_eht(hnf, fs);

% diagnostic plot
figure;
loglog(f, sqrt(Pf), 'r');
hold on;
loglog(Sh(:,1), sqrt(Sh(:,2)), 'b', 'linewidth', 2);
axis([10 1000 1e-26 1e-20]);
pretty;
xlabel('f (Hz)');
ylabel('strain (rHz)');
pretty;
print('-dpng', 'test_gaussian_noise');

return
