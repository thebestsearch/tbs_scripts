function y = hpfilter_eht(x, n, fknee, fr)
% function y = hpfilter_eht(x, n, fknee, fr)
% applies a (causal) IIR filter
%   x = input data
%   n = order
%   fknee = knee frequency
%   fr (re)sample rate
%   y = ouput data
% Eric Thrane

% butterworth: FIR not IIR!
%[b,a] = butter(2, fknee/(fr/2), 'high');
%y = filtfilt(b, a, x);

% matlab demo with IIR highpass in 2015
%hpFilt = designfilt('highpassiir','FilterOrder',8, ...
%         'PassbandFrequency',75e3,'PassbandRipple',0.2, ...
%         'SampleRate',200e3);
%y = filter(hpFilt, x);

% my IIR filter
hpFilt = designfilt('highpassiir', 'FilterOrder', n, ...
         'PassbandFrequency', fknee, 'PassbandRipple',0.2, ...
         'SampleRate', fr);
y = filter(hpFilt, x);

return
