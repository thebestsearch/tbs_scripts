function normed_hf = FT_norm(xf, fs)
% function normed_hf = FT_norm(xf, fs)
% Paul Lasky
% paul.lasky@ligo.org
% The output of fft_eht is in units of strain,
% this converts that output to units of strain / sqrt(Hz)
%
% xf is a Fourier transform created by fft_eht
% fs is sampling frequency
% normed_hf is now ready as input for inner_product.m

% calculate the length of the time series used to create xf
length_xf = size(xf, 1);
if mod(length_xf, 2)
  L = (2*(length_xf-1));
else
  L = (2*(length_xf-2));
end

% factor of sqrt(6) included to correct normalisation
% that is, pure noise realisation return an S/N distribution with zero mean and unit variance
% this seems to be a function of the observation time

%normed_hf = sqrt(6 / (L * fs)) * xf;
normed_hf = xf / fs;

return

