clear all;

% set these parameters
f0=240;              % initial frequency in Hz
fdot = -0.2;           % df/dt in Hz/s
dur = 700;            % injection duration
h0=5e-22;            % peak amplitude
iota = 0*(pi/180);  % iota in radians
psi = 0*(pi/180);   % psi in radians
%---------------------

% calculate phase evolution
fs=16384;
t=[0:1/fs:dur];
Phit = 2*pi*(f0*t + 0.5*fdot*t.^2);

% calcaulate hp and hx
[hp, hx] = polarizer(Phit, iota, psi, h0);

% apply 1s ramps
hp(1:fs) = t(1:fs).*hp(1:fs);
hx(1:fs) = t(1:fs).*hx(1:fs);
hp(end-fs+1:end) = fliplr(t(1:fs)).*hp(end-fs+1:end);
hx(end-fs+1:end) = fliplr(t(1:fs)).*hx(end-fs+1:end);

% save waveform
out = [t; hp; hx;];

fname = sprintf('pol_%i_%i_%i.dat',(180/pi)*iota,(180/pi)*psi,dur);

fid = fopen(fname,'w+');
fprintf(fid,'%.12f\t%.8e\t%.8e\n',out);
fclose(fid);

% make a diagnostic plot
figure;
semilogx(t,hp,'b');
hold on;
semilogx(t,hx,'r');
print('-djpeg','polarizer.jpg');

