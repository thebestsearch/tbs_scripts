function product = inner_product(aa, bb, freq, PSD)
% function product = inner_product(aa, bb, freq, PSD)
% Paul Lasky
% modified by eric.thrane@ligo.org
% calculates the Owen-Sathyaprakash inner product; see Eq. 2.4 from:
%   http://arxiv.org/pdf/gr-qc/9808076v1.pdf
%----------------------------------------
% aa = single-sided FFT of the data NORMALISED using FT_norm
% bb = single-sided FFT the template NORMALISED using FT_norm
% freq = array of frequencies
% PSD = single-sided noise power spectrum
%----------------------------------------
% Note: the expectation value of matched filter signal-to-noise ratio rho for
% a template hh is given by innerproduct(hh, hh, freq, PSD).
%   S/N = rho = <h,h> / rms(n,h) = <h,h> / sqrt(h,h) = sqrt(h,h)
% This is consistent with Callister et al. Eq. C2:
%   https://dcc.ligo.org/LIGO-P1600059-v12

% interpolate the PSD to the freq grid
PSD_interp = interp1(PSD(:, 1), PSD(:, 2), freq);

% NANs are introduced outside the PSD grid.  Change NANs to Infs
% (an Inf in the PSD contributes zero to the SNR -- see below)
PSD_interp(isnan(PSD_interp)) = Inf;

% calculate the the inner product
integrand = conj(aa) .* bb ./ PSD_interp;

% calculate frequency spacing
df = freq(2) - freq(1);

% carry out sum
integral = sum(integrand) * df;

% Eq. 2.4 in Owen-Sathyaprakash
product = 4 * real(integral);

return

