function run_demo_v4
% function run_demo_v4

% trials
T = 20;

% nphi values
nphi = [1 3 9];

% loop over both
for ii=1:length(nphi)
  for jj=1:T
    tmp(ii,jj) = demo_v4(nphi(ii));
  end
end

% save a mat file
save('img/run_demo_v4.mat');

return
