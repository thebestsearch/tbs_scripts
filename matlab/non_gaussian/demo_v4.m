function Lambda = demo_v4(Nphi)
% function Lambda = demo_v4()
% eric.thrane@ligo.org
% demonstrate non-Gaussian search for sine waves
% for this version:
%  * 8 seconds of data with Gaussian noise + 300 sinusoids
%  * we assume that the strain amplitude of each signal is exactly known
%  * each signal has a signal-to-noise ratio of 0.5
% changes for v3: 
%  # allow for truly random frequencies
% changes for v4
%  * each sinusoid has random phase phi

% noise psd
tic;
Sh = load('../../../tbs_data/input/ZERO_DET_high_P_psd.txt');

% sample rate
fs = 2048;
dt = 1/fs;

% duration
dur = 8;
fprintf('duration = %1.1e\n', dur);

% generate random Gaussian noise
n = gaussian_noise(Sh, fs, dur);

% number of sources
Ns = 300;
fprintf('number of sources = %i\n', Ns);

% generate random frequencies for signals
fmin = 10;
fmax = fs/2;
f0 = fmin + (fmax-fmin)*rand(Ns,1);
% define fcut
[nf f] = fft_eht(n, fs);
nf = FT_norm(nf, fs);
fcut = f>=fmin & f<fmax;
fc = f(fcut);
df = fc(2)-fc(1);

% generate fixed amplitudes for now
f_Sh = Sh(:,1);
hrms_Sh = sqrt(Sh(:,2));
rhovals = 0.5;
% sanity check test
%rhovals = 1e-5;
%fprintf('TEST: pure noise\n');
h0 = rhovals .* interp1(f_Sh, hrms_Sh, f0);

% random phase
fprintf('random phase\n');
phi = 2*pi*rand(size(h0));

% add sinusoids to the data
t = [1/fs : 1/fs : dur]';
h = zeros(size(t));
for ii=1:Ns
  h = h + h0(ii)*sin(2*pi*f0(ii)*t);
end

% windowing off
fprintf('no windows\n');

% add signal to noise
s = h + n;

% Fourier transform
[hf f] = fft_eht(h, fs);
[sf f] = fft_eht(s, fs);

% normalise
hf = FT_norm(hf, fs);
sf = FT_norm(sf, fs);

% epectation value optimal snr (if we only knew the properties of each source)
rho_th = cal_snr(h, fs, Sh);
% actual value of optimal snr (if we only knew the properties of each source)
rho_opt = cal_snr_data(h, s, fs, Sh);
% print to screen
fprintf('if we only knew the properties of each source...\n');
fprintf('  theoretical snr = %1.2e\n', rho_th);
fprintf('     measured snr = %1.2e\n', rho_opt);

% do not include fmax in order to avoid unphysically large SNR
nf = nf(fcut);
hf = hf(fcut);
sf = sf(fcut);
Nfft = length(fc);

% speedy interpolation for noise
% factor of two ensures that std(real(sf)./sqrt(var)) = 1 for pure noise
var = 2*interp1(f_Sh, Sh(:,2), fc);

% define xi
xi = [0 : 0.001 : 1-0.001];

% create a new frequency array to take into accounts two-bin signals
% no observable bias
ff = fc(1) : df/2 : fc(end);
Nf = length(ff);

% construct arrays using to sum over search frequencies and FFT frequencies
% simultaneously (FFT + resolvability parameter)
t_array = repmat(t, 1, Nf);
sf_array = repmat(sf, 1, Nf);
var_array = repmat(var, 1, Nf);

% define regulator
reg = -0.5*sum(abs(sf_array).^2 ./ var_array, 1);
reg = mean(reg);

% calculate background probability summing over FFT
B = exp(-0.5*sum(abs(sf_array).^2 ./ var_array, 1) - reg);

% prepare to loop over phi
try
  dphi = 2*pi/NPhi;
catch
  dphi = pi/3;
end
phivals = [dphi : dphi : 2*pi];
Nphi = length(phivals);

  % template
  Nt = length(t);
  f_array = repmat(ff, Nt, 1);
  hu = rhovals*interp1(f_Sh, hrms_Sh, ff);
  hu_array = repmat(hu, Nt, 1);
  S = zeros(size(B));
  % marginalise over phi
  for kk=1:Nphi
    I = ones(size(f_array));
    u = hu_array.*sin(2*pi*f_array.*t_array + phi(kk)*I);
    [uf f] = fft_eht(u, fs);
    uf = FT_norm(uf, fs);  
    uf = uf(fcut, :);
    % calculate signal probability
    S = S + exp(-0.5*sum(abs(sf_array-uf).^2 ./ var_array, 1) - reg);
  end
  S = S/Nphi;

  % create arrays of S and B with new xi dimension
  Nxi = length(xi);
  S_array = repmat(S, Nxi, 1);
  B_array = repmat(B, Nxi, 1);
  xi_array = repmat(xi', 1, Nf);

  % calculate log probability by summing over search directions (resolvability
  % parameter
  LP = sum(log(xi_array.*S_array + (1-xi_array).*B_array), 2);

% calculate maximum likelihood point
[~, idx] = max(LP);
xi_hat = xi(idx);

% true value: not well-defined anymore with overlapping frequency bins
% it can be book-ended with a systematic error
xi0 = Ns/Nf;

% regularise posterior
reg2 = max(LP);
P = exp(LP-reg2);
P = P/sum(P);

% calculate the 95% CL interval
[P_sort, idx] = sort(P, 'descend');
P_cum = cumsum(P_sort);
pidx = min(find(P_cum>0.95));
xi_excluded = xi(idx(pidx:end));
ll = max(xi_excluded(xi_excluded<xi_hat));
ul = min(xi_excluded(xi_excluded>=xi_hat));
if isempty(ll)
  ll=0;
end

% detection statistic
Lambda = max(P)/P(1);

% print confidence interval
fprintf('max likelihood estimator xi = %1.1e\n', xi_hat);
fprintf('true likelihood estimator xi = %1.1e\n', xi0);
fprintf('95%% confidence interval = (%1.1e, %1.1e)\n', ll, ul);
fprintf('Lambda = %1.1e\n', Lambda);

if 1==0
% plot duty cycle
figure;
semilogy(xi, P, 'b');
hold on;
semilogy(xi_hat, interp1(xi, P, xi_hat), 'ko');
semilogy(ll*[1 1], [1e-10 1], 'g');
semilogy(ul*[1 1], [1e-10 1], 'g');
semilogy(xi0*[1 1], [1e-10 1], 'r');
axis([0 0.1 1e-10 1]);
grid on;
xlabel('\xi');
ylabel('p(\xi)');
pretty;
print('-dpng', '../../../tbs_plots/img/demo4/xi_v4');

% plot duty cycle loglog
figure;
loglog(xi, P, 'b');
hold on;
semilogy(xi_hat, interp1(xi, P, xi_hat), 'ko');
semilogy(ll*[1 1], [1e-10 1], 'g');
semilogy(ul*[1 1], [1e-10 1], 'g');
semilogy(xi0*[1 1], [1e-10 1], 'r');
axis([1e-3 1 1e-10 1]);
grid on;
xlabel('\xi');
ylabel('p(\xi)');
pretty;
print('-dpng', '../../../tbs_plots/img/demo4/xi_log_v4');

% spectra plot
figure;
loglog(fc, abs(sf));
hold on;
loglog(fc, abs(hf), 'r');
axis([fmin fmax 1e-25 1e-21]);
xlabel('f (Hz)');
ylabel('h (Hz^{-1/2})');
grid on;
pretty;
print('-dpng', '../../../tbs_plots/img/demo4/spectum_v4');
end

toc;
return

