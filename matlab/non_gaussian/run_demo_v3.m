function run_demo_v3
% function run_demo_v3
% eric.thrane@ligo.org
% measure Lambda as a function of res with decent statistics

% number of trials
N = 10;

% resolutions
res = [0.25 0.5 1 2 4];

% loop over resolutions
for ii=1:length(res)
  Lambda = [];
  % loop over trials
    for jj=1:N
      tmp = demo_v3(res(ii));
      Lambda = [Lambda tmp];
    end
    % calculate mean and std
    LambdaBar(ii) = mean(Lambda);
    LambdaSig(ii) = std(Lambda);
end

% save the results
save('img/run_demo_v3.mat');

% guess that the typical error is multiplicative
fac = LambdaBar/LambdaSig;

% overcounting factor
ofac = exp(res);

% plot the results
figure;
semilogy(res, LambdaBar./ofac, 'b');
%hold on;
%semilogy(res, LambdaBar*(1+fac), 'r');
%semilogy(res, LambdaBar*(1-fac), 'r');
grid on;
xlabel('resolution (FFT bins)');
ylabel('\Lambda');
pretty;
print('-dpng', 'img/run_demo_v3');

return
