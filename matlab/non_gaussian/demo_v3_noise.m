function [LP LP3] = demo_v3_noise
% function [LP LP3] = demo_v3_noise
% eric.thrane@ligo.org
% demonstrate non-Gaussian search for sine waves
% for this version:
%  * 8 seconds of data with Gaussian noise + 300 sinusoids
%  * pure noise
%  * we assume that the strain amplitude of each signal is exactly known
% changes for v3: 
%  # allow for truly random frequencies

tic;
% noise psd
Sh = load('ZERO_DET_high_P_psd.txt');

% sample rate
fs = 2048;
dt = 1/fs;

% duration
dur = 8;
fprintf('duration = %1.1e\n', dur);

% generate random Gaussian noise
n = gaussian_noise(Sh, fs, dur);

% number of sources
Ns = 300;
fprintf('number of sources = %i\n', Ns);

% generate random frequencies for signals
fmin = 10;
fmax = fs/2;
f0 = fmin + (fmax-fmin)*rand(Ns,1);
% define fcut
[nf f] = fft_eht(n, fs);
nf = FT_norm(nf, fs);
fcut = f>=fmin & f<fmax;
ff = f(fcut);
df = ff(2)-ff(1);

% generate fixed amplitudes for now
f_Sh = Sh(:,1);
hrms_Sh = sqrt(Sh(:,2));
rhovals = 0.5;
% sanity check test
%rhovals = 1e-5;
%fprintf('TEST: pure noise\n');
h0 = rhovals .* interp1(f_Sh, hrms_Sh, f0);

% fixed phase for now
fprintf('for the time being: phase=0\n');
phi = 0;

% add sinusoids to the data
t = [1/fs : 1/fs : dur]';
h = zeros(size(t));
for ii=1:Ns
  h = h + h0(ii)*sin(2*pi*f0(ii)*t);
end

% window everything?
%w = hann(length(h));
% for now, use rectangular window. hann window yields std(snr)=0.85
w = ones(size(h));
h = w.*h;
n = w.*n;

% add NO signal to noise
s = 0 + n;

% Fourier transform
[hf f] = fft_eht(h, fs);
[sf f] = fft_eht(s, fs);

% normalise
hf = FT_norm(hf, fs);
sf = FT_norm(sf, fs);

% epectation value optimal snr (if we only knew the properties of each source)
rho_th = cal_snr(h, fs, Sh);
% actual value of optimal snr (if we only knew the properties of each source)
rho_opt = cal_snr_data(h, s, fs, Sh);
% print to screen
fprintf('if we only knew the properties of each source...\n');
fprintf('  theoretical snr = %1.2e\n', rho_th);
fprintf('     measured snr = %1.2e\n', rho_opt);

% do not include fmax in order to avoid unphysically large SNR
nf = nf(fcut);
hf = hf(fcut);
sf = sf(fcut);
Nfft = length(ff);

% speedy interpolation for noise
% factor of two ensures that std(real(sf)./sqrt(var)) = 1 for pure noise
var = 2*interp1(f_Sh, Sh(:,2), ff);

% define xi
xi = [0 : 0.001 : 1-0.001];

% initialise log probability
LP=0; LP3=0;

% define regulator
reg = -0.5*sum(abs(sf).^2 ./ var);

% calculate background probability
B = exp(-0.5*sum(abs(sf).^2 ./ var) - reg);

% create a new frequency array to take into accounts two-bin signals
ff2 = ff(1) : df : ff(end);
% offset by df/2
ff3 = ff(1) + df/2 : df : ff(end);
Nf = length(ff2);


% loop over GW emission frequencies
for jj=1:Nf
  % template
  hu = rhovals*interp1(f_Sh, hrms_Sh, ff2(jj));
  u = hu*sin(2*pi*ff2(jj)*t);
  u = w.*u;
  [uf f] = fft_eht(u, fs);
  uf = FT_norm(uf, fs);  
  uf = uf(fcut);
  % calculate signal probability
  S = exp(-0.5*sum(abs(sf-uf).^2 ./ var) - reg);
  % add to log probability
  LP = LP + log(xi*S + (1-xi)*B);
  % offset template
  if jj~=Nf
    u3 = hu*sin(2*pi*ff3(jj)*t);
    u3 = w.*u3;
    [u3f f] = fft_eht(u3, fs);
    u3f = FT_norm(u3f, fs);
    u3f = u3f(fcut);
    S3 = exp(-0.5*sum(abs(sf-u3f).^2 ./ var) - reg);
    LP3 = LP3 + log(xi*S3 + (1-xi)*B);
  end
end

toc;
return

