function demo_v1
% function demo_v1
% eric.thrane@ligo.org
% demonstrate optimal matched filter based non-Gaussian search for sines
% this version uses matched filtering. i assume that the phase of each sine is
% well known. in the next version v2, I will use the duty cycle formalism

% noise psd
tic;
Sh = load('ZERO_DET_high_P_psd.txt');

% sample rate
fs = 2048;
dt = 1/fs;

% duration (takes too long to finish; breaks cluster...too much memory?)
%dur = 60;
% finishes quickly: (3.2 to 10.7) s
dur = 8;
fprintf('duration = %1.1e\n', dur);

% generate random Gaussian noise
n = gaussian_noise(Sh, fs, dur);

% number of sources
Ns = 100;

% generate random frequencies for signals
fmin = 10;
fmax = fs/2;
f0 = fmin + (fmax-fmin)*rand(Ns,1);

% generate random amplitudes (chosen, artificially, based on the noise curve)
f_Sh = Sh(:,1);
hrms_Sh = sqrt(Sh(:,2));
%rhomin = 0.1;
rhomin = 0.5;
rhomax = 0.5;
rhovals = rhomin + (rhomax-rhomin)*rand(Ns,1);
h0 = rhovals .* interp1(f_Sh, hrms_Sh, f0);
% loud signal sanity check
%fprintf('loud signal test\n');
%h0 = 10 * interp1(f_Sh, hrms_Sh, f0);
% pure noise sanity check
%fprintf('pure noise test\n');
%h0 = 1e-3*h0;

% random phase
%phi = 2*pi*rand(Ns,1);
fprintf('phase=0 test\n');
phi = 0;


% add sinusoids to the data
t = [1/fs : 1/fs : dur]';
h = zeros(size(t));
for ii=1:Ns
  h = h + h0(ii)*sin(2*pi*f0(ii)*t);
end

% window everything?
%w = hann(length(h));
% for now, use rectangular window. hann window yields std(snr)=0.85
w = ones(size(h));
h = w.*h;
n = w.*n;

% add signal to noise
s = h + n;

% Fourier transform
[hf f] = fft_eht(h, fs);
[sf f] = fft_eht(s, fs);

% normalise
hf = FT_norm(hf, fs);
sf = FT_norm(sf, fs);

% epectation value optimal snr (if we only knew the properties of each source)
rho_th = cal_snr(h, fs, Sh);
% actual value of optimal snr (if we only knew the properties of each source)
rho_opt = cal_snr_data(h, s, fs, Sh);
% print to screen
fprintf('if we only knew the properties of each source...\n');
fprintf('  theoretical snr = %1.2e\n', rho_th);
fprintf('     measured snr = %1.2e\n', rho_opt);

% two parameters describing the template: f0 and phi
Nphi = 12;
% do not include fmax in order to avoid unphysically large SNR
ff = f(f>=fmin & f<fmax);
% number of frequencies to search over \neq the number of FFT frequencies
Nf = length(ff);
Ntemplates = Nphi*Nf;
fprintf('Ntemplates = %1.1e\n', Ntemplates)

% construct an array of copies of the data to matched filter
sf_array = repmat(sf, 1, Nf);

% construct a matched filter template bank
t_array = repmat(t, 1, Nf);
ff_array = repmat(ff', length(t), 1);
u = sin(2*pi*ff_array.*t_array);
w_array = repmat(w, 1, Nf);
u = w_array.*u;
[uf f] = fft_eht(u, fs);
uf = FT_norm(uf, fs);

% speedy interpolation for noise
sigma = interp1(f_Sh, Sh(:,2), ff)';
sigma_array = repmat(sigma, length(hf), 1);

% calculate inner product
df = ff(2)-ff(1);
% window factor
wfac = 1;
% from inner_product.m
rho = sum( wfac*4*df*real(conj(uf).*sf_array ./ sigma_array), 1) ./ ...
  sqrt(sum( wfac*4*df*real(conj(uf).*uf ./ sigma_array), 1));

% diagnostic
fprintf('mean(snr)=%1.1e, std(snr)=%1.1e\n', mean(rho), std(rho));

% cumulative snr
rho_cum = mean(rho)*sqrt(length(rho));
fprintf('cumulative snr = %1.1e\n', rho_cum);

% compare with expected cumulative snr
rho_cum_exp = sqrt(sum(rhovals.^2));
% include dilution factor from noise bins
rho_cum_exp = (Ns/length(rho))*rho_cum_exp;
fprintf('expected cumulative snr = %1.1e\n', rho_cum_exp);

% diagnostic spectra plot
figure;
subplot(2, 1, 1);
loglog(f, abs(sf));
hold on;
loglog(f, abs(hf), 'r');
axis([fmin fmax 1e-25 1e-21]);
xlabel('f (Hz)');
ylabel('h (Hz^{-1/2})');
grid on;
pretty;
subplot(2, 1, 2);
semilogx(ff, rho);
hold on;
semilogx(f0, interp1(ff, rho, f0), 'ro');
axis([fmin fmax -5 5]);
xlabel('f (Hz)');
ylabel('\rho');
grid on;
pretty;
print('-dpng', 'img/spectum');

% carry out KS test to compare distribution of rho with Gaussian noise
[~, pval] = kstest(rho);
fprintf('KS test p-value = %1.4f%%\n', 100*pval);

toc;
return

