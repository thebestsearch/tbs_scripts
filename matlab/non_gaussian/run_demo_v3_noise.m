function run_demo_v3_noise
% function run_demo_v3_noise

LP=[]; LP3=[];
for ii=1:1000
  [LPii LP3ii] = demo_v3_noise;
  LP = [LP ; LPii];
  LP3 = [LP3 ; LP3ii];
end

save('img/run_demo_v3_noise.mat');

% calculate covariance matrix
nxi = size(LP,2);
for ii=1:nxi-1
  tmp = cov(LP(:,ii), LP3(:,ii));
  var11(ii) = tmp(1,1);
  var22(ii) = tmp(2,2);
  var12(ii) = tmp(1,2);
end

% xi vector
xi = [0 : 0.001 : 1-2*0.001];

% plot the results
figure;
semilogy(xi, var11, 'b');
hold on;
semilogy(xi, var22, 'k');
semilogy(xi, abs(var12), 'r');
grid on;
xlabel('\xi');
ylabel('covariance');
pretty;
print('-dpng', 'img/run_demo_v3_noise');

return
