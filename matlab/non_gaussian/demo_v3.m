function Lambda = demo_v2(res)
% function Lambda = demo_v2
% eric.thrane@ligo.org
% demonstrate non-Gaussian search for sine waves
% for this version:
%  * 8 seconds of data with Gaussian noise + 300 sinusoids
%  * each sinusoid has phase phi=0
%  * we assume that the strain amplitude of each signal is exactly known
%  * each signal has a signal-to-noise ratio of 0.5
% changes for v3: 
%  # allow for truly random frequencies
%  # res = the number of subdivided bins for use by run_demo_v3.m

% for use by run_demo_v3
tic;
try
  res;
catch
  res=-1;
end

% noise psd
Sh = load('../../../tbs_data/input/ZERO_DET_high_P_psd.txt');

% sample rate
fs = 2048;
dt = 1/fs;

% duration
dur = 8;
fprintf('duration = %1.1e\n', dur);

% generate random Gaussian noise
n = gaussian_noise(Sh, fs, dur);

% number of sources
Ns = 300;
fprintf('number of sources = %i\n', Ns);

% generate random frequencies for signals
fmin = 10;
fmax = fs/2;
f0 = fmin + (fmax-fmin)*rand(Ns,1);
% define fcut
[nf f] = fft_eht(n, fs);
nf = FT_norm(nf, fs);
fcut = f>=fmin & f<fmax;
ff = f(fcut);
df = ff(2)-ff(1);

% generate fixed amplitudes for now
f_Sh = Sh(:,1);
hrms_Sh = sqrt(Sh(:,2));
rhovals = 0.5;
% sanity check test
%rhovals = 1e-5;
%fprintf('TEST: pure noise\n');
h0 = rhovals .* interp1(f_Sh, hrms_Sh, f0);

% fixed phase for now
fprintf('for the time being: phase=0\n');
phi = 0;

% add sinusoids to the data
t = [1/fs : 1/fs : dur]';
h = zeros(size(t));
for ii=1:Ns
  h = h + h0(ii)*sin(2*pi*f0(ii)*t);
end

% window everything?
%w = hann(length(h));
% for now, use rectangular window. hann window yields std(snr)=0.85
w = ones(size(h));
h = w.*h;
n = w.*n;

% add signal to noise
s = h + n;

% Fourier transform
[hf f] = fft_eht(h, fs);
[sf f] = fft_eht(s, fs);

% normalise
hf = FT_norm(hf, fs);
sf = FT_norm(sf, fs);

% epectation value optimal snr (if we only knew the properties of each source)
rho_th = cal_snr(h, fs, Sh);
% actual value of optimal snr (if we only knew the properties of each source)
rho_opt = cal_snr_data(h, s, fs, Sh);
% print to screen
fprintf('if we only knew the properties of each source...\n');
fprintf('  theoretical snr = %1.2e\n', rho_th);
fprintf('     measured snr = %1.2e\n', rho_opt);

% do not include fmax in order to avoid unphysically large SNR
nf = nf(fcut);
hf = hf(fcut);
sf = sf(fcut);
Nfft = length(ff);

% speedy interpolation for noise
% factor of two ensures that std(real(sf)./sqrt(var)) = 1 for pure noise
var = 2*interp1(f_Sh, Sh(:,2), ff);

% define xi
xi = [0 : 0.001 : 1-0.001];

% initialise log probability
LP = 0;

% define regulator
reg = -0.5*sum(abs(sf).^2 ./ var);

% calculate background probability
B = exp(-0.5*sum(abs(sf).^2 ./ var) - reg);

% create a new frequency array to take into accounts two-bin signals
if res==-1
  % no observable biase
  ff2 = ff(1) : df/4 : ff(end);
else
  ff2 = ff(1) : df/res : ff(end);
end
Nf = length(ff2);

% loop over GW emission frequencies
for jj=1:Nf
  % template
  hu = rhovals*interp1(f_Sh, hrms_Sh, ff2(jj));
  u = hu*sin(2*pi*ff2(jj)*t);
  u = w.*u;
  [uf f] = fft_eht(u, fs);
  uf = FT_norm(uf, fs);  
  uf = uf(fcut);
  % calculate signal probability
  S = exp(-0.5*sum(abs(sf-uf).^2 ./ var) - reg);
  % add to log probability
  LP = LP + log(xi*S + (1-xi)*B);
end

% calculate maximum likelihood point
[~, idx] = max(LP);
xi_hat = xi(idx);

% true value: not well-defined anymore with overlapping frequency bins
% it can be book-ended with a systematic error
xi0_low = Ns/Nf;
xi0_high = Ns/Nfft;

% regularise posterior
reg2 = max(LP);
P = exp(LP-reg2);
P = P/sum(P);

% calculate the 95% CL interval
[P_sort, idx] = sort(P, 'descend');
P_cum = cumsum(P_sort);
pidx = min(find(P_cum>0.95));
xi_excluded = xi(idx(pidx:end));
ll = max(xi_excluded(xi_excluded<xi_hat));
ul = min(xi_excluded(xi_excluded>=xi_hat));
if isempty(ll)
  ll=0;
end

% detection statistic
Lambda = max(P)/P(1);

% print confidence interval
fprintf('max likelihood estimator xi = %1.1e\n', xi_hat);
fprintf('true likelihood estimator xi = %1.1e-%1.1e\n', xi0_low, xi0_high);
fprintf('95%% confidence interval = (%1.1e, %1.1e)\n', ll, ul);
fprintf('Lambda = %1.1e\n', Lambda);

if res==-1
% plot duty cycle
figure;
semilogy(xi, P, 'b');
hold on;
semilogy(xi0_low*[1 1], [1e-10 1], 'r');
semilogy(xi0_high*[1 1], [1e-10 1], 'r');
semilogy(xi_hat, interp1(xi, P, xi_hat), 'ko');
semilogy(ll*[1 1], [1e-10 1], 'g');
semilogy(ul*[1 1], [1e-10 1], 'g');
axis([0 0.1 1e-10 1]);
grid on;
xlabel('\xi');
ylabel('p(\xi)');
pretty;
print('-dpng', '../../../tbs_plots/img/demo3/xi');

% plot duty cycle loglog
figure;
loglog(xi, P, 'b');
hold on;
loglog(xi0_low*[1 1], [1e-10 1], 'r');
loglog(xi0_high*[1 1], [1e-10 1], 'r');
semilogy(xi_hat, interp1(xi, P, xi_hat), 'ko');
semilogy(ll*[1 1], [1e-10 1], 'g');
semilogy(ul*[1 1], [1e-10 1], 'g');
axis([1e-3 1 1e-10 1]);
grid on;
xlabel('\xi');
ylabel('p(\xi)');
pretty;
print('-dpng', '../../../tbs_plots/img/demo3/xi_log');

% spectra plot
figure;
loglog(ff, abs(sf));
hold on;
loglog(ff, abs(hf), 'r');
axis([fmin fmax 1e-25 1e-21]);
xlabel('f (Hz)');
ylabel('h (Hz^{-1/2})');
grid on;
pretty;
print('-dpng', '../../../tbs_plots/img/demo3/spectum');
end

toc;
return

